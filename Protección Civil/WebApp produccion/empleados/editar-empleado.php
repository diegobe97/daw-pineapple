<?php
    session_start();
    if(isset($_SESSION["privilegios"])){
        require_once("modelo-empleados.php");
        include("../_header.html");

        $entrada = getEntrada(conectar(), $_GET["userId"]);

        include("_form-empleado.html");

        include('../_user-menu.html');
        include("../_footer.html");
    }else{
        include('../error.html');
    }
?>
