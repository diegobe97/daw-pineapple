<?php
    session_start();
    require_once('modelo-empleados.php');

    eliminarEmpleado($_GET["id"]);

    $_SESSION["mensaje"] = 'Empleado eliminado correctamente.';

    header('location:ver-empleados.php');
?>
