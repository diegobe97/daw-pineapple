<?php
    session_start();
    if(isset($_SESSION["privilegios"])){
        require_once("modelo-empleados.php");
        
        if(binarySearch($_SESSION["privilegios"], 20)){
            include('../_header.html');

            $IdEmpleado = $_GET["userId"];
            $empleado = getEmpleado($IdEmpleado);

            include('_bitacora-empleado-main.html');

            include('../_user-menu.html');
            include('../_footer.html');
        }
    }else{
        include('../error.html');
    }
?>