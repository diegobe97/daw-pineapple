<?php
    require_once("../modelo.php");

    function getEmpleados($privilegios){
        $db = conectar();
        $query="SELECT u.IdUsuario as userId, u.Nombre as nombre, r.NombreRol as rol, d.NombreDepartamento as departamento , u.idEmpleado as idEmpleado
                    FROM usuario u, rol r, departamento d, asignadoa a, trabajaen t
                    WHERE u.IdUsuario = a.IdUsuario AND u.IdUsuario = t.IdUsuario AND r.IdRol = a.IdRol AND d.IdDepartamento = t.IdDepartamento
                    ORDER BY u.Nombre";
        $res=$db->query($query);

        $html = '<table class="striped">
            <thead>
                <tr>
                  <th></th>
                  <th>Nombre</th>
                  <th>Rol</th>
                  <th>Departamento</th>
                </tr>
            </thead>
            <tbody>';
        $i = 1;

        while($row = $res->fetch_array(MYSQLI_BOTH)){
            $html .= '
                    <tr>
                      <td>'.$i.'. </td>
                      <td>'.$row["nombre"].'</td>
                      <td>'.$row["rol"].'</td>
                      <td>'.$row["departamento"].'</td>';
                    if($row["rol"] != "Administrador"){
                        if(binarySearch($privilegios, 15) && binarySearch($privilegios, 16) && binarySearch($privilegios, 20)){
                            $html .= '<td>
                                  <a href="bitacora-empleado.php?userId='.$row["userId"].'" class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Bitácora"><i class="material-icons">library_books</i></a>
                                </td>
                                <td>
                                  <a href="editar-empleado.php?userId='.$row["userId"].'" class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="Editar empleado"><i class="material-icons">edit</i></a>
                                </td>
                                <td>
                                <a href="return false;" class="btn-floating red tooltipped botonBorrar" data-id="'.$row["userId"].'" data-ruta="eliminar-empleado.php" data-tipo="empleado" data-position="top" data-delay="50" data-tooltip="Eliminar empleado"><i class="material-icons">delete_forever</i></a>
                            </td></tr>';
                        }else if(binarySearch($privilegios, 15) && binarySearch($privilegios, 16)){
                            $html .= '<td>
                                  <a href="editar-empleado.php?userId='.$row["userId"].'" class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="Editar empleado">edit</i></a>
                                </td>
                                <td>
                                  <a href="return false;" class="btn-floating red tooltipped botonBorrar" data-id="'.$row["userId"].'" data-ruta="eliminar-empleado.php" data-tipo="empleado" data-position="top" data-delay="50" data-tooltip="Eliminar empleado"><i class="material-icons">delete_forever</i></a>
                            </td></tr>';
                        }else if(binarySearch($privilegios, 15) && binarySearch($privilegios, 20)){
                            $html .= '<td>
                                  <a href="bitacora-empleado.php?userId='.$row["userId"].'" class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Bitácora"><i class="material-icons">library_books</i></a>
                                </td>
                                <td>
                                  <a href="editar-empleado.php?userId='.$row["userId"].'" class="btn-floating blue"><i class="material-icons">edit</i></a>
                            </td></tr>';
                        }else if(binarySearch($privilegios, 16) && binarySearch($privilegios, 20)){
                            $html .= '<td>
                                  <a href="bitacora-empleado.php?userId='.$row["userId"].'" class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Bitácora"><i class="material-icons">library_books</i></a>
                                </td>
                                <td>
                                  <a href="return false;" class="btn-floating red tooltipped botonBorrar" data-id="'.$row["userId"].'" data-ruta="eliminar-empleado.php" data-tipo="empleado" data-position="top" data-delay="50" data-tooltip="Eliminar empleado"><i class="material-icons">delete_forever</i></a>
                            </td></tr>';
                        }else if(binarySearch($privilegios, 15)){
                            $html .= '<td>
                                <a href="editar-empleado.php?userId='.$row["userId"].'" class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="Editar empleado"><i class="material-icons">edit</i></a>
                            </td></tr>';
                        }else if(binarySearch($privilegios, 16)){
                            $html .= '<td>
                                <a href="return false;" class="btn-floating red tooltipped botonBorrar" data-id="'.$row["userId"].'" data-ruta="eliminar-empleado.php" data-tipo="empleado" data-position="top" data-delay="50" data-tooltip="Eliminar empleado"><i class="material-icons">delete_forever</i></a>
                            </td></tr>';
                        }else if(binarySearch($privilegios, 20)){
                            $html .= '<td>
                                <a href="bitacora-empleado.php?userId='.$row["userId"].'" class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Bitácora"><i class="material-icons">library_books</i></a>
                            </td></tr>';
                        }else{
                            $html .= '<td></td></tr>';
                        }
                    }else if(binarySearch($privilegios, 20)){
                        $html .= '<td>
                                  <a href="bitacora-empleado.php?userId='.$row["userId"].'" class="btn-floating green tooltipped" data-position="top" data-delay="50" data-tooltip="Bitácora"><i class="material-icons">library_books</i></a>
                                </td><td></td><td></td></tr>';
                    }
            $i++;
        }

        $html .= '</tbody></table>';

        if(binarySearch($privilegios, 14) || binarySearch($privilegios, 17)){
            $html .= '<div class="divider"></div>
                  <div class = "row">
                    <div class="col s12">
                        <a href="nuevo-empleado.php" class="right waves-effect waves-light blue btn-floating tooltipped" data-position="left" data-delay="50" data-tooltip="Crear empleado">
                          <i class="material-icons">add</i>
                        </a>
                    </div>
                </div>';
        }



        $html .= '</div>';

        mysqli_free_result($res);
        desconectar($db);
        return $html;
    }

    function guardarEmpleado($user, $idEmpleado, $nombre, $apellidoP, $apellidoM, $puesto, $rol, $departamento){
        $db = conectar();
        $query = "CALL crearUsuario('".$user."','".$idEmpleado."','".$nombre."','".$apellidoP."','".$apellidoM."','".$puesto."','".$rol."','".$departamento."')";
        if (!$db->query($query)) {
            echo "Falló CALL: (" . $db->errno . ") " . $db->error;
        }
        desconectar($db);
    }

    function getEntrada($db, $userId){
        $query = "SELECT *
                  FROM usuario u, asignadoa a, trabajaen t
                  WHERE u.IdUsuario = a.IdUsuario AND u.IdUsuario = t.IdUsuario AND u.IdUsuario ='".$userId."'";
        $res = $db->query($query);
        $row = $res->fetch_array(MYSQLI_BOTH);
        return $row;
    }

    function editarEmpleado($user, $idEmpleado, $nombre, $apellidoP, $apellidoM, $puesto, $rol, $departamento, $idUsu){
        $db = conectar();
        $query = "CALL editarUsuario('".$user."','".$idEmpleado."','".$nombre."','".$apellidoP."','".$apellidoM."','".$puesto."','".$rol."','".$departamento."','".$idUsu."')";
        if (!$db->query($query)) {
            echo "Falló CALL: (" . $db->errno . ") " . $db->error;
        }
        desconectar($db);
    }

    function eliminarEmpleado($UserId){
        eliminarRelacionesE($UserId);
        $db = conectar();
        $query = "DELETE FROM usuario WHERE IdUsuario = ?";
        if(!($statement = $db->prepare($query))){
            die("Preparación fallida: (".$db->errno.") ".$db->error);
        }
        if(!($statement->bind_param("s", $UserId))){
            die("Vinculación de parámetros fallida: (".$statement->errno.") ".$statement->error);
        }
        if(!$statement->execute()){
            die("Ejecución fallida: (".$statement->errno.") ".$statement->error);
        }
        desconectar($db);
    }

    function eliminarRelacionesE($UserId){
        $db = conectar();
        //AsignadoA
        $query = "DELETE FROM asignadoa WHERE IdUsuario = ?";
        if(!($statement = $db->prepare($query))){
            die("Preparación fallida: (".$db->errno.") ".$db->error);
        }
        if(!($statement->bind_param("s", $UserId))){
            die("Vinculación de parámetros fallida: (".$statement->errno.") ".$statement->error);
        }
        if(!$statement->execute()){
            die("Ejecución fallida: (".$statement->errno.") ".$statement->error);
        }
        //TrabajaEn
        $query2 = "DELETE FROM trabajaen WHERE IdUsuario = ?";
        if(!($statement2 = $db->prepare($query2))){
            die("Preparación fallida: (".$db->errno.") ".$db->error);
        }
        if(!($statement2->bind_param("s", $UserId))){
            die("Vinculación de parámetros fallida: (".$statement2->errno.") ".$statement2->error);
        }
        if(!$statement2->execute()){
            die("Ejecución fallida: (".$statement2->errno.") ".$statement2->error);
        }
        desconectar($db);
    }

    function getBitacora($idUsuario){
        $db = conectar();

        //Specification of the SQL query
        $query='SELECT * FROM bitacora WHERE IdUsuario LIKE "'.$idUsuario.'" ORDER BY FechaBitacora DESC';
        // Query execution; returns identifier of the result group
        $bitacora = $db->query($query);

        $result = "";

        if($bitacora){
            $i = 1;

            // cycle to explode every line of the results
            while ($fila = mysqli_fetch_array($bitacora, MYSQLI_BOTH)) {

                //Specification of the SQL query
                $query='SELECT * FROM publicacion WHERE IdPublicacion = "'.$fila["IdPublicacion"].'"';
                // Query execution; returns identifier of the result group
                $publicacion = $db->query($query);

                $titulo = mysqli_fetch_array($publicacion, MYSQLI_BOTH);

                $result .= '<div class="row">
                                <div class="col s1 m1 l1">
                                    <p>'.$i.'</p>
                                </div>
                                <div class="col s4 m4 l4">
                                    <p>'.$titulo["TituloPublicacion"].'</p>
                                </div>
                                <div class="col s4 m4 l4">
                                    <p>'.$fila["DescripcionBitacora"].'</p>
                                </div>
                                <div class="col s3 m3 l3">
                                    <p>'.getFechaConFormato($fila["FechaBitacora"]).'</p>
                                </div>
                            </div>
                            <div class="divider"></div>';
                $i++;

                mysqli_free_result($publicacion);
            }

            // it releases the associated results
            mysqli_free_result($bitacora);
        }


        desconectar($db);

        return $result;
    }

    function getEmpleado($IdUsuario){
        $db = conectar();

        //Specification of the SQL query
        $query = 'SELECT * FROM usuario WHERE IdUsuario = "'.$IdUsuario.'"';
        // Query execution; returns identifier of the result group
        $usuario = $db->query($query);

        $empleado = mysqli_fetch_array($usuario, MYSQLI_BOTH);

        desconectar($db);

        return $empleado;
    }
?>
