<?php
    session_start();
    require_once("modelo-roles.php");

    eliminarRol($_GET["id"]);

    $_SESSION["mensaje"] = "Rol eliminado correctamente.";

    header("location:ver-roles.php");
?>
