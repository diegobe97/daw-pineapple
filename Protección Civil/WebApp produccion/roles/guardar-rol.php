<?php
    session_start();
    require_once("modelo-roles.php");

    $n = countPrivilegios();

    $arrayPrivilegios = array_fill(1, $n, 0);
    $privilegiosSeleccionados = false;

    for($i = 1; $i <= $n; $i++){
        if(isset($_POST['privilegio'.$i.''])){
            $arrayPrivilegios[$i] = 1;
            $privilegiosSeleccionados = true;
        }
    }

    if($privilegiosSeleccionados){
        if(isset($_POST["IdRol"])) {
            eliminarPrivilegios($_POST["IdRol"]);
            
            editarRol($_POST["IdRol"], htmlspecialchars($_POST["nombreRol"]), htmlspecialchars($_POST["descripcionRol"]));
            guardarPrivilegios($_POST["IdRol"], $arrayPrivilegios);
            
            $_SESSION["mensaje"] = $_POST["nombreRol"].' se actualizó correctamente.';
        } else if(nombreRolUnico($_POST["nombreRol"])){
            guardarRol(htmlspecialchars($_POST["nombreRol"]), htmlspecialchars($_POST["descripcionRol"]));
            guardarPrivilegios(getIdRolByName($_POST["nombreRol"]), $arrayPrivilegios);
            
            $_SESSION["mensaje"] = $_POST["nombreRol"].' se creó correctamente.';
        } else {
            $_SESSION["mensaje"] = $_POST["nombreRol"].' ya existe.';
        }
    }else{
        echo 'Ningun privilegio fue seleccionado o se repite el nombre del rol.';
    }

    header("location:ver-roles.php");
?>
