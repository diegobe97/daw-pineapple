<?php
    session_start();
    if(isset($_SESSION["privilegios"])){
        require_once("modelo-evento.php");
        include("../_header.html");

        if(isset($_GET["idEvento"])){
            $evento = getEvento(conectar(), $_GET["idEvento"]);
        }
        
        include("_form-evento.html");

        include('../_user-menu.html');
        include('../_footer.html');
    }else{
        include('../error.html');
    }
?>