<?php
    session_start();
    require_once("modelo-evento.php");

    if(isset($_POST["idEvento"])) {
        eliminarTipoEvento($_POST["idEvento"]);

        editarEvento($_POST["idEvento"], htmlspecialchars($_POST["tituloEvento"]), htmlspecialchars($_POST["descripcionEvento"]), $_POST["fechaInicio_php"], $_POST["horaInicio"], $_POST["fechaFin_php"], $_POST["horaFin"], htmlspecialchars($_POST["urlVideo"]), htmlspecialchars($_POST["rutaAdjuntos"]), htmlspecialchars($_POST["rutaImagenes"]), $_POST["action"]);

        $arrayTipos=$_POST["idTipoEvento"];

        guardarTipoEvento($_POST["idEvento"], $arrayTipos);

        $_SESSION["mensaje"] = $_POST["tituloEvento"].' se actualizó correctamente.';
        
    } else {
        publicarEvento(htmlspecialchars($_POST["tituloEvento"]), htmlspecialchars($_POST["descripcionEvento"]), $_POST["fechaInicio_php"], $_POST["horaInicio"], $_POST["fechaFin_php"], $_POST["horaFin"], htmlspecialchars($_POST["urlVideo"]), htmlspecialchars($_POST["rutaAdjuntos"]), htmlspecialchars($_POST["rutaImagenes"]), $_POST["action"]);

        $arrayTipos=$_POST["idTipoEvento"];
        
        $idE = getIdEvento();

        guardarTipoEvento($idE, $arrayTipos);

        $_SESSION["mensaje"] = $_POST["tituloEvento"].' se creó correctamente.';
    }

    header("location:ver-eventos.php");
?>
