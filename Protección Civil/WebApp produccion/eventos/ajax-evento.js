function printErrors(field, response){
    var msg = document.getElementById('error-'+field);
    msg.innerHTML = '';
    if (response[field].hasOwnProperty('mensaje')) {
        msg.innerHTML += '<span>' + response[field]['mensaje'] + '</span>';
    }else{
        jQuery.each(response[field], function(i, element) {
            if( element['upload']==false ) {
                if( element.hasOwnProperty('nombre')) { 
                    msg.innerHTML += element['nombre'] + '<br>' 
                };
                jQuery.each(element['mensaje'], function() {
                    msg.innerHTML += '<span class="tab">' + this + '</span>';
                    msg.innerHTML += '<br>'
                }); 
            }
        });
    }  
}

function resetErrors() {
    document.getElementById('error-imagenes').innerHTML = '';
    document.getElementById('error-archivos').innerHTML = '';
    document.getElementById('error-url').innerHTML = '';
}


$('.evento-submit[type="submit"]').click(function(event) {
    event.preventDefault();
    $('#action').val(this.value)
    var form_data = new FormData($('#form-evento')[0]);
    $.ajax({
        url: 'validar-subir-evento.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function(response) {
            resetErrors();
            console.log(response);
            response = JSON.parse(response);
            console.log(response);
            if (response['errores'] === false) {
                if($('#action').val() == 'borrador'){
                    $('#publicar').off('click');
                    $("#publicar").click();
                }else if ($('#action').val() == 'publicar'){
                    $('#borrador').off('click');
                    $("#borrador").click();
                }
            } else {
                if (response.hasOwnProperty('imagenes')){
                    printErrors('imagenes', response);
                }
                if (response.hasOwnProperty('archivos')) {
                    printErrors('archivos', response);
                }
                if (response.hasOwnProperty('url')) { 
                    printErrors('url', response);
                }
            }
        },
        error: function(response) {
            $('#msg').html(response); // display error response from the PHP script
        }
    });
});

$("#imagenes").change(function() {
    var error_imagen = document.getElementById('error-imagenes');
    error_imagen.innerHTML = '';
    jQuery.each(imagenes['files'], function() {
        if (this.name != '') {
            //JPG, JPEG, PNG & GIF
            var formatos = ["jpg", "jpeg", "png", "gif", "JPG", "JPEG", "PNG", "GIF"];
            var flag = false;
            for (i in formatos) {
                substring = '.' + formatos[i];
                var res = this.name.includes(substring);
                if (res == true) {
                    flag = true;
                }
            }
            if (flag == false) {
                error_imagen.innerHTML += this.name;
                error_imagen.innerHTML += '<br>';
                error_imagen.innerHTML += '<span class="tab">La imágen debe tener uno de los siguientes formatos: jpg, jpeg, png, gif</span>';
                error_imagen.innerHTML += '<br>';
            }
        }
    })
})

$("#archivos").change(function() {
    var error_archivo = document.getElementById('error-archivos');
    error_archivo.innerHTML = '';
    var cont = 0;
    jQuery.each(archivos['files'], function() {
        if (this.name != '') {
            //PDF, JPG, JPEG, PNG & GIF
            var formatos = ["pdf", "jpg", "jpeg", "png", "gif", "PDF", "JPG", "JPEG", "PNG", "GIF"];
            var flag = false;
            for (i in formatos) {
                substring = '.' + formatos[i];
                var res = this.name.includes(substring);
                if (res == true) {
                    flag = true;
                }
            }
            if (flag == false) {
                error_archivo.innerHTML += this.name;
                error_archivo.innerHTML += '<br>';
                error_archivo.innerHTML += '<span class="tab">El archivo debe tener uno de los siguientes formatos: pdf, jpg, jpeg, png, gif</span>';
                error_archivo.innerHTML += '<br>';
            }
        }
    })
})