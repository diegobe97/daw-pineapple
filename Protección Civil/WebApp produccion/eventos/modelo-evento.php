<?php
    require_once("../modelo.php");

    function getTipoEvento($db, $idEvento){
        $db = conectar();

        //Specification of the SQL query
        $query = 'SELECT T.NombreTipoEvento
        FROM tipoevento T, perteneceaevento P
        WHERE P.IdEvento="'.$idEvento.'" AND T.IdTipoEvento = P.IdTipoEvento';

        // Query execution; returns identifier of the result group
        $registros = $db->query($query);

        $row = mysqli_fetch_array($registros, MYSQLI_BOTH);

        $tipoEvento = $row["NombreTipoEvento"];

        desconectar($db);

        return $tipoEvento;
    }
    
    function imprimeTipoEvento($idEvento){
        $db = conectar();

        //Specification of the SQL query
        $query = 'SELECT T.NombreTipoEvento
        FROM tipoevento T, perteneceaevento P
        WHERE P.IdEvento="'.$idEvento.'" AND T.IdTipoEvento = P.IdTipoEvento';

        // Query execution; returns identifier of the result group
        $registros = $db->query($query);
        
        $tiposEvento = '';
        
        $num_rows = $registros->num_rows;
        $i = 0;
        
        while ($row = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
            if($i == $num_rows - 1) {
                $tiposEvento .= $row["NombreTipoEvento"];
            }else {
                $tiposEvento .= $row["NombreTipoEvento"].', ';   
            }
            $i++;
        }
        
        return $tiposEvento;
    }

    function getEventosTable($privilegios) {
        $db = conectar();
        //Specification of the SQL query
        $query='SELECT * FROM evento ORDER BY FechaInicio DESC';
         // Query execution; returns identifier of the result group
        $registros = $db->query($query);

        $table = '<table class="striped">
            <thead>
                <tr>
                  <th>Titulo</th>
                  <th>Tipo de evento</th>
                  <th>Descripción</th>
                  <th>Fecha de Inicio</th>
                  <th>Fecha de Fin</th>
                </tr>
            </thead>
            <tbody>';
        // cycle to explode every line of the results
        while ($row = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
            $publicado = $row["EventoPublicado"];
            if($publicado == 0){
                $style='"background-color: rgb(255, 255, 163);"';
            }else{
                $style = '';
            }
            
            $fechaInicio = date('d \d\e F Y, h:ia', strtotime($row["FechaInicio"]));
            $fechaFin = date('d \d\e F Y, h:ia', strtotime($row["FechaFin"]));
            $translate = array("January" => "Enero", 
                        "February" => "Febrero", 
                        "March" => "Marzo", 
                        "April" => "Abril", 
                        "May" => "Mayo", 
                        "June" => "Junio", 
                        "July" => "Julio",
                        "August" => "Agosto", 
                        "September" => "Septiembre", 
                        "October" => "Octubre", 
                        "November" => "Noviembre", 
                        "December" => "Diciembre");
            $fechaInicio = strtr($fechaInicio, $translate);
            $fechaFin = strtr($fechaFin, $translate);
            
            $table .= '
            <tr style='.$style.'>
            <td>'.$row["TituloEvento"].'</td>
            <td>'.imprimeTipoEvento($row["IdEvento"]).'</td>
            <td class="texto-eventos">'.trunc($row["DescripcionEvento"], 400).'</td>
            <td>'.$fechaInicio.'</td>
            <td>'.$fechaFin.'</td>
            <td>';

            if(binarySearch($_SESSION["privilegios"], 5) || binarySearch($_SESSION["privilegios"], 23)){
                $table .= '<a href="editar-evento.php?idEvento='.$row["IdEvento"].'" class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="Editar evento">
                <i class="material-icons">edit</i></a>';
            }

            $table .= '</td><td>';

            if(binarySearch($_SESSION["privilegios"], 6) || binarySearch($_SESSION["privilegios"], 24)){
                $table .= '<a href="#" class="btn-floating botonBorrar red tooltipped" data-id="'.$row["IdEvento"].'" data-ruta="eliminar-evento.php" data-tipo="evento" data-position="top" data-delay="50" data-tooltip="Eliminar evento">
                <i class="material-icons">delete_forever</i></a>';
            }

            $table .= '</td></tr>';

        }
        // it releases the associated results
        mysqli_free_result($registros);

        desconectar($db);

        $table .= '</tbody></table>';

        return $table;
    }

    // Función usada en editar-evento
    function getEvento($db, $idEvento){
        $db = conectar();

        //Specification of the SQL query
        $query='SELECT * FROM evento WHERE IdEvento="'.$idEvento.'"';

         // Query execution; returns identifier of the result group
        $registros = $db->query($query);

        $row = mysqli_fetch_array($registros, MYSQLI_BOTH);

        desconectar($db);

        return $row;
    }

    function getDropdownTipoEvento($idEvento = -1) {
        $db = conectar();

        if($idEvento > 0){
            //Specification of the SQL query
            $query='SELECT * FROM perteneceaevento
            WHERE IdEvento = '.$idEvento.'';
            // Query execution; returns identifier of the result group
            $tiposDelEvento = $db->query($query);

            $n = $tiposDelEvento->num_rows;

            // Guarda los id de los tipos de evento seleccionados
            $arrayTipos = array_fill(0, $n, -1);

            $i = 0;

            while($filaTiposEvento = mysqli_fetch_array($tiposDelEvento, MYSQLI_BOTH)) {
                $arrayTipos[$i] = $filaTiposEvento["IdTipoEvento"];
                $i++;
            }
        }

        //Specification of the SQL query
        $query='SELECT * FROM tipoevento ORDER BY NombreTipoEvento';
        // Query execution; returns identifier of the result group
        $tipos = $db->query($query);

        $select = "";
        $tipoSeleccionado = false;

        $i = 0;

        // cycle to explode every line of the results
        while ($row = mysqli_fetch_array($tipos, MYSQLI_BOTH)) {
            if($idEvento > 0){
                $tipoSeleccionado = false;
                if ($i < $n && $row["IdTipoEvento"] == $arrayTipos[$i]){
                    $select .= '<option value="'.$row["IdTipoEvento"].'" selected>'.$row["NombreTipoEvento"].'</option>';
                    $tipoSeleccionado = true;
                    $i++;
                }
            }
            if(!$tipoSeleccionado){
                $select .= '<option value="'.$row["IdTipoEvento"].'">'.$row["NombreTipoEvento"].'</option>';
            }
        }

        // it releases the associated results
        mysqli_free_result($tipos);

        desconectar($db);

        return $select;
    }

    function publicarEvento($tituloEvento, $descripcionEvento, $fechaInicio, $horaInicio, $fechaFin, $horaFin, $urlVideo, $rutaAdjuntos, $rutaImagenes, $action){
        $db = conectar();
        
        if ($action == "borrador") {
            $eventoPublicado = 0;
        } else if ($action == "publicar") {
            $eventoPublicado = 1;
        }

        $fechaInicio = date('Y-m-d', strtotime("$fechaInicio"));
        $fechaFin = date('Y-m-d', strtotime("$fechaFin"));
        
        $horaInicio = date("H:i:s", strtotime("$horaInicio"));
        $horaFin = date("H:i:s", strtotime("$horaFin"));

        $fechaInicio= "$fechaInicio $horaInicio";
        $fechaFin= "$fechaFin $horaFin";

        // insert command specification
        $query='INSERT INTO evento (`TituloEvento` ,`DescripcionEvento`, `FechaInicio`, `FechaFin`, `URLVideoEvento`, `RutaAdjuntosEvento`, `RutaImagenesEvento`, `EventoPublicado`) VALUES (?,?,?,?,?,?,?,?)';
        // Preparing the statement
        if (!($statement = $db->prepare($query))) {
            die("Preparation failed: (" . $db->errno . ") " . $db->error);
        }
        // Binding statement params
        if (!$statement->bind_param("ssssssss", $tituloEvento, $descripcionEvento, $fechaInicio, $fechaFin, $urlVideo, $rutaAdjuntos, $rutaImagenes, $eventoPublicado)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
        }
        // Executing the statement
        if (!$statement->execute()) {
            die("Execution failed: (" . $statement->errno . ") " . $statement->error);
        }
        
        // Guardar el id del ultimo evento creado
        $idEvento = $db->insert_id;
        
        // Si se adjuntaron imagenes, crear la relacion
        if (isset($_SESSION['imagenes']) && $rutaImagenes != '') {
            foreach ($_SESSION['imagenes'] as $key => $id_imagen) {
                guardarImagenesEventos($db, $idEvento, $_SESSION['imagenes'][$key]['id_imagen']);
            }
            unset($_SESSION['imagenes']);
        }
        // Si se adjuntaron archivos, crear la relacion
        if (isset($_SESSION['archivos']) && $rutaAdjuntos != '') {
            foreach ($_SESSION['archivos'] as $key => $id_archivo) {
                guardarArchivosEventos($db, $idEvento, $_SESSION['archivos'][$key]['id_archivo']);
            }
            unset($_SESSION['archivos']);
        }
        
        desconectar($db);
    }

    
    function guardarImagenesEventos($db, $idEvento, $idImagenesEventos){
        // insert command specification
        $query = 'UPDATE imagenesevento SET IdEvento=? WHERE IdImagenesEventos =' . $idImagenesEventos;
        // Preparing the statement
        if (!($statement = $db->prepare($query))) {
            die("Preparation failed: (" . $db->errno . ") " . $db->error);
        }
        // Binding statement params
        if (!$statement->bind_param("s", $idEvento)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
        }
        // update execution
        if (!($statement->execute())) {
            die("Update failed: (" . $statement->errno . ") " . $statement->error);
        }
    }

    function guardarArchivosEventos($db, $idEvento, $idArchivosEventos){
        // insert command specification
        $query = 'UPDATE archivosevento SET IdEvento=? WHERE IdArchivosEventos =' . $idArchivosEventos;
        // Preparing the statement
        if (!($statement = $db->prepare($query))) {
            die("Preparation failed: (" . $db->errno . ") " . $db->error);
        }
        // Binding statement params
        if (!$statement->bind_param("s", $idEvento)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
        }
        // update execution
        if (!($statement->execute())) {
            die("Update failed: (" . $statement->errno . ") " . $statement->error);
        }
    }

    
    function editarEvento($idEvento, $tituloEvento, $descripcionEvento, $fechaInicio, $horaInicio, $fechaFin, $horaFin, $urlVideo, $rutaAdjuntos, $rutaImagenes, $action){
        $db = conectar();
        
        if ($action == "borrador") {
            $eventoPublicado = 0;
        } else if ($action == "publicar") {
            $eventoPublicado = 1;
        }

        $fechaInicio = date('Y-m-d', strtotime("$fechaInicio"));
        $fechaFin = date('Y-m-d', strtotime("$fechaFin"));

        $horaInicio = date("H:i:s", strtotime("$horaInicio"));
        $horaFin = date("H:i:s", strtotime("$horaFin"));
        
        $fechaInicio= "$fechaInicio $horaInicio";
        $fechaFin= "$fechaFin $horaFin";

        // insert command specification
        $query='UPDATE evento SET TituloEvento=?, DescripcionEvento=?, FechaInicio=?, FechaFin=?, URLVideoEvento=?, RutaAdjuntosEvento=?, RutaImagenesEvento=?, EventoPublicado=? WHERE IdEvento=?';
        // Preparing the statement
        if (!($statement = $db->prepare($query))) {
            die("Preparation failed: (" . $db->errno . ") " . $db->error);
        }
        // Binding statement params
        if (!$statement->bind_param("sssssssss", $tituloEvento, $descripcionEvento, $fechaInicio, $fechaFin, $urlVideo, $rutaAdjuntos, $rutaImagenes, $eventoPublicado, $idEvento)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
        }
        // update execution
        if (!($statement->execute())) {
            die("Update failed: (" . $statement->errno . ") " . $statement->error);
        }  
        
        // Si la ruta de imagenes esta vacia o se adjuntaron nuevas, borrar las relaciones existentes
        if (isset($_SESSION['imagenes']) || $rutaImagenes == '') {
            eliminarImagenesEvento($db, $idEvento);
            
            // Una vez borrada las relaciones, si se adjuntaron nuevas, crear la nueva relacion
            if (isset($_SESSION['imagenes'])){
                foreach ($_SESSION['imagenes'] as $key => $id_imagen) {
                    guardarImagenesEventos($db, $idEvento, $_SESSION['imagenes'][$key]['id_imagen']);
                }
                unset($_SESSION['imagenes']);
            }   
        }
        // Mismas validaciones que en imágenes
        if (isset($_SESSION['archivos']) || $rutaAdjuntos == '') {
            eliminarArchivosEvento($db, $idEvento);
            
            // Una vez borrada las relaciones, si se adjuntaron nuevas, crear la nueva relacion
            if (isset($_SESSION['archivos'])){
                foreach ($_SESSION['archivos'] as $key => $id_archivo) {
                    guardarArchivosEventos($db, $idEvento, $_SESSION['archivos'][$key]['id_archivo']);
                }
                unset($_SESSION['archivos']);
            }   
        }
        

        desconectar($db);
    }

    function eliminarImagenesEvento($db, $idEvento) {
        // Eliminar los archivos del servidor
        $query = 'SELECT * FROM imagenesevento i, evento e WHERE e.IdEvento = i.IdEvento AND i.IdEvento =' . $idEvento;
        $registros = $db->query($query);
        while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
            $imagen =   "../images/" . $fila['RutaImagenes'];
            // Para borrar la imagen del servidor
            unlink($imagen);
        }  
        // it releases the associated results
        mysqli_free_result($registros);

        $query = "DELETE FROM imagenesevento WHERE IdEvento=?";

        if (!($statement = $db->prepare($query))) {
            die("Preparación fallida: (" . $db->errno . ") " . $db->error);
        }
        if (!($statement->bind_param("s", $idEvento))) {
            die("Vinculación de parámetros fallida: (" . $statement->errno . ") " . $statement->error);
        }
        if (!$statement->execute()) {
            die("Ejecución fallida: (" . $statement->errno . ") " . $statement->error);
        }
    }

    function eliminarArchivosEvento($db, $idEvento) {
        // Eliminar los archivos del servidor
        $query = 'SELECT * FROM archivosevento a, evento e WHERE e.IdEvento = a.IdEvento AND a.IdEvento =' . $idEvento;
        $registros = $db->query($query);
        while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
            $archivo =   "../files/" . $fila['RutaArchivos'];
            // Para borrar la imagen del servidor
            unlink($archivo);
        }  
        // it releases the associated results
        mysqli_free_result($registros);

        $query = "DELETE FROM archivosevento WHERE IdEvento=?";

        if (!($statement = $db->prepare($query))) {
            die("Preparación fallida: (" . $db->errno . ") " . $db->error);
        }
        if (!($statement->bind_param("s", $idEvento))) {
            die("Vinculación de parámetros fallida: (" . $statement->errno . ") " . $statement->error);
        }
        if (!$statement->execute()) {
            die("Ejecución fallida: (" . $statement->errno . ") " . $statement->error);
        }
    }

    function eliminarEvento($idEvento){
        $db = conectar();

        // insert command specification
        $query='DELETE FROM evento WHERE IdEvento= ?';
        // Preparing the statement
        if (!($statement = $db->prepare($query))) {
            die("Preparation failed: (" . $db->errno . ") " . $db->error);
        }
        // Binding statement params
        if (!$statement->bind_param("s", $idEvento)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
        }
        // update execution
        if (!($statement->execute())) {
            die("Update failed: (" . $statement->errno . ") " . $statement->error);
        }

        desconectar($db);
    }

    function guardarTipoEvento($idEvento, $arrayTiposEvento){
        $db = conectar();

        for($i = 0; $i < count($arrayTiposEvento); $i++){
            // insert command specification
            $query='INSERT INTO perteneceaevento (`IdEvento` ,`IdTipoEvento`) VALUES (?,?)';
            // Preparing the statement
            if (!($statement = $db->prepare($query))) {
                die("Preparation failed: (" . $db->errno . ") " . $db->error);
            }
            // Binding statement params
            if (!$statement->bind_param("ss", $idEvento, $arrayTiposEvento[$i])) {
                die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
            }
            // Executing the statement
            if (!$statement->execute()) {
                die("Execution failed: (" . $statement->errno . ") " . $statement->error);
            }
        }

        desconectar($db);
    }

    function eliminarTipoEvento($idEvento){
        $db = conectar();

        // insert command specification
        $query='DELETE FROM perteneceaevento WHERE IdEvento = ?';

        // Preparing the statement
        if (!($statement = $db->prepare($query))) {
            die("Preparation failed: (" . $db->errno . ") " . $db->error);
        }
        // Binding statement params
        if (!$statement->bind_param("s", $idEvento)) {
                die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
            }
        // Executing the statement
        if (!$statement->execute()) {
            die("Execution failed: (" . $statement->errno . ") " . $statement->error);
        }

        desconectar($db);
    }

    //Función utilizada para guardar los tipos de evento en publicar-evento
    function getIdEvento($tituloEvento, $descripcionEvento, $fechaInicio, $horaInicio, $fechaFin, $horaFin){
        $db = conectar();
        
        $fechaInicio = date('Y-m-d', strtotime("$fechaInicio"));
        $fechaFin = date('Y-m-d', strtotime("$fechaFin"));
        
        $horaInicio = date("H:i:s", strtotime("$horaInicio"));
        $horaFin = date("H:i:s", strtotime("$horaFin"));

        $fechaInicio= "$fechaInicio $horaInicio";
        $fechaFin= "$fechaFin $horaFin";

        //Specification of the SQL query
        $query='SELECT * FROM evento WHERE TituloEvento = \''.$tituloEvento.'\'
        AND DescripcionEvento = \''.$descripcionEvento.'\'
        AND FechaInicio = \''.$fechaInicio.'\' AND FechaFin = \''.$fechaFin.'\'';

         // Query execution; returns identifier of the result group
        $registros = $db->query($query);

        $row = mysqli_fetch_array($registros, MYSQLI_BOTH);

        desconectar($db);

        return $row["IdEvento"];
    }

    function getSliderEvento($idEvento) {
        $evento = getEvento(conectar(), $idEvento);
        
        $slider = '';

        if ( $evento['RutaImagenesEvento'] != '' ){
            $db = conectar();
            
            //Specification of the SQL query
            $query = 'SELECT * FROM imagenesevento i, evento e WHERE e.IdEvento = i.IdEvento AND i.IdEvento =' . $idEvento;
            // Query execution; returns identifier of the result group
            $registros = $db->query($query);
            
            if ($registros->num_rows > 0) {
                $slider = '<div class="slider slider-evento">
                      <ul class="slides">';  

                // cycle to explode every line of the results
                while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                    $imagen =   "../images/" . $fila['RutaImagenes'];

                    $slider.='  <li>
                                    <img src="'.$imagen.'"> 
                                </li>';
                }

                $slider .= '</ul>
                            </div>';

                // it releases the associated results
                mysqli_free_result($registros);

                desconectar($db);

                return $slider;
            }else {
                return;
            }
            
        }else{
            return;
        }   
    }

    function getAdjuntosEvento($idEvento){
        $evento = getEvento(conectar(), $idEvento);
        $adjuntos = '';
        
        if ($evento["RutaAdjuntosEvento"] != ''){
            $db = conectar();
            
            $query = 'SELECT * FROM archivosevento WHERE IdEvento=' . $evento["IdEvento"] . ' ORDER BY IdArchivosEventos';
            $registros = $db->query($query);
            
            if ($registros->num_rows > 0) {
                $adjuntos .= '<div class="row">';
                $adjuntos .= '<h5 class="titulo-modal-eventos">Archivos adjuntos:</h5>';

                while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                    $adjuntos .='<div class="col s12 m12 l12 adjunto-eventos">
                                    <a href="../files/'.$fila["RutaArchivos"].'" target="_blank" class="valign-wrapper">
                                        <i class="material-icons center-align red-text icono-evento">picture_as_pdf</i>
                                        <span class="title">'.trunc(substr($fila["RutaArchivos"], 11),32).'</span>
                                    </a>
                                </div>';
                }

                $adjuntos .= '</div>';

                // it releases the associated results
                mysqli_free_result($registros);

                desconectar($db);

                return $adjuntos;
            }else {
                return;
            }
            
        }else{
            return;
        }
    }

    function getURLEvento($idEvento){
        $evento = getEvento(conectar(), $idEvento);
        $url = '';
        
        if ($evento["URLVideoEvento"] != ''){
            
            $url .= '<div class="row">';
            $url .= '<h5 class="titulo-modal-eventos">Link relacionado:</h5>';
            
            $url .='<div class="col s12 m12 l12">
                        <a href="'.$evento["URLVideoEvento"].'" target="_blank" class="valign-wrapper">
                        '.$evento["URLVideoEvento"].'
                        </a>
                    </div>';
            
            $url .= '</div>';
            
            return $url;
        }else{
            return;
        }
    }

    
    function getFechaInicioEvento($idEvento) {
        $db = conectar();
        $evento = getEvento($db, $idEvento);
        
        $fechaInicio = date('d \d\e F Y, h:ia', strtotime($evento["FechaInicio"]));
        
        $translate = array("January" => "Enero", 
                "February" => "Febrero", 
                "March" => "Marzo", 
                "April" => "Abril", 
                "May" => "Mayo", 
                "June" => "Junio", 
                "July" => "Julio",
                "August" => "Agosto", 
                "September" => "Septiembre", 
                "October" => "Octubre", 
                "November" => "Noviembre", 
                "December" => "Diciembre");

        $fechaInicio = strtr($fechaInicio, $translate);
        
        desconectar($db);
        
        return $fechaInicio;
        
    }

    function getFechaFinEvento($idEvento) {
        $db = conectar();
        $evento = getEvento($db, $idEvento);
        
        $fechaFin = date('d \d\e F Y, h:ia', strtotime($evento["FechaFin"]));
        
        $translate = array("January" => "Enero", 
                "February" => "Febrero", 
                "March" => "Marzo", 
                "April" => "Abril", 
                "May" => "Mayo", 
                "June" => "Junio", 
                "July" => "Julio",
                "August" => "Agosto", 
                "September" => "Septiembre", 
                "October" => "Octubre", 
                "November" => "Noviembre", 
                "December" => "Diciembre");

        $fechaFin = strtr($fechaFin, $translate);
        
        desconectar($db);
        
        return $fechaFin;
        
    }
    
    
    function getImagenesEventoEditar($evento){ 
         if ($evento["RutaImagenesEvento"] != ''){
            $db = conectar();
            $query = 'SELECT * FROM imagenesevento WHERE IdEvento=' . $evento["IdEvento"] . ' ORDER BY idImagenesEventos';
            $registros = $db->query($query);

            $imagenes = '<div class="row">';

            // cycle to explode every line of the results
            while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                // Options: MYSQLI_NUM to use only numeric indexes
                // MYSQLI_ASSOC to use only name (string) indexes
                // MYSQLI_BOTH, to use both
                $imagen =   "../images/" . $fila['RutaImagenes'];
                $id     = $fila["IdImagenesEventos"];

                $imagenes .='  <div class="col s12 m12 l12 center-align">
                                    <div class="row valign-wrapper"> 
                                        <div class="col s11 m11 l11">
                                            <img class="materialboxed" style="max-height: 300px; width:auto;" src="'.$imagen.'">
                                        </div>
                                        <div class="col s1 m1 l1">';

                if(isset($_SESSION["privilegios"])){
                    $imagenes .= '  <a href="#!" class="botonBorrar btn-floating red" data-ruta="eliminar-imagen.php"
                                    data-id="' . $fila['RutaImagenes'] . '" data-tipo="archivo o imagen">
                                            <i class="material-icons">delete_forever</i>
                                    </a>';
                }

                $imagenes .='                            
                                        </div>
                                    </div>
                                </div>';
            }

            $imagenes .= '</div>';

            // it releases the associated results
            mysqli_free_result($registros);

            desconectar($db);

            return $imagenes;
        }else{
            return;
        }   
    }

    function getArchivosAdjuntosEventoEditar($evento){
        if ($evento["RutaAdjuntosEvento"] != ''){
            $db = conectar();
            $query = 'SELECT * FROM archivosevento WHERE IdEvento=' . $evento["IdEvento"] . ' ORDER BY IdArchivosEventos';
            $registros = $db->query($query);
            $adjuntos = '<div class="row">';
            $adjuntos .= '<h5 class="titulo-adjuntos">Adjuntos</h5>';
            while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                $id = $fila['IdArchivosEventos'];
                $adjuntos .='<div class="col s3 m2 l2 center-align z-depth-1">
                                <a href="../files/'.$fila["RutaArchivos"].'" target="_blank">
                                    <div class="card z-depth-0 file">
                                        <div class="card-content center-align">
                                            <span> <i class="material-icons medium center-align red-text">picture_as_pdf</i></span>
                                            <span class="card-title center-align" style="font-size: 1rem">'.trunc(substr($fila["RutaArchivos"], 11),32).'</span>
                                        </div>
                                    </div>
                                </a>';
                $adjuntos .= '<a href="#!" class="botonBorrar btn-floating red" data-ruta="eliminar-adjunto.php" data-tooltip="Eliminar archivo adjunto"
                                    data-id="' . $fila['RutaArchivos'] . '" data-tipo="archivo adjunto">
                                        <i class="material-icons">delete_forever</i>
                                    </a>';
                $adjuntos .= '</div>';
            }
            $adjuntos .= '</div>';
            return $adjuntos;
        }else{
            return;
        }   
    }


    function eliminarImagenEvento($id){
        $db = conectar();

        $query='SELECT * FROM imagenesevento WHERE RutaImagenes = "'.$id.'"';
        $registros = $db->query($query);
        $fila = mysqli_fetch_array($registros, MYSQLI_BOTH);
        $idevento=$fila['IdEvento'];

        $query = 'UPDATE evento SET RutaImagenesEvento=? WHERE IdEvento ='.$idevento;
        if (!($statement = $db->prepare($query))) {
            die("Preparation failed: (" . $db->errno . ") " . $db->error);
        }
        // Binding statement params
        $vacio='';
        if (!$statement->bind_param("s", $vacio)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
        }
        // update execution
        if (!($statement->execute())) {
            die("Update failed: (" . $statement->errno . ") " . $statement->error);
        }

        $query = 'DELETE FROM imagenesevento WHERE RutaImagenes=?';
        if (!($statement = $db->prepare($query))) {
            die("Preparación fallida: (" . $db->errno . ") " . $db->error);
        }
        if (!($statement->bind_param("s", $id))) {
            die("Vinculación de parámetros fallida: (" . $statement->errno . ") " . $statement->error);
        }
        if (!$statement->execute()) {
            die("Ejecución fallida: (" . $statement->errno . ") " . $statement->error);
        }

        $imagen =   "../images/" . $id;
        unlink($imagen);

        desconectar($db);

        return $idevento;
    }

    function eliminarAdjuntoEvento($id){
    $db = conectar();
    
    $query='SELECT * FROM archivosevento WHERE RutaArchivos = "'.$id.'"';
    $registros = $db->query($query);
    $fila = mysqli_fetch_array($registros, MYSQLI_BOTH);
    $idevento=$fila['IdEvento'];
    
    $query = 'UPDATE evento SET RutaAdjuntosEvento=? WHERE IdEvento ='.$idevento;
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params
    $vacio='';
    if (!$statement->bind_param("s", $vacio)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
    }
    // update execution
    if (!($statement->execute())) {
        die("Update failed: (" . $statement->errno . ") " . $statement->error);
    }
    
    $query = 'DELETE FROM archivosevento WHERE RutaArchivos=?';
    if (!($statement = $db->prepare($query))) {
        die("Preparación fallida: (" . $db->errno . ") " . $db->error);
    }
    if (!($statement->bind_param("s", $id))) {
        die("Vinculación de parámetros fallida: (" . $statement->errno . ") " . $statement->error);
    }
    if (!$statement->execute()) {
        die("Ejecución fallida: (" . $statement->errno . ") " . $statement->error);
    }
    
    $archivo =   "../files/" . $id;
    unlink($archivo);
    
    desconectar($db);
    
    return $idevento;
}


?>
