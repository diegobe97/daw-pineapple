<?php
    session_start();
    if(isset($_SESSION["privilegios"])){
        require_once("modelo-evento.php");
        $idevento=eliminarImagenEvento($_GET["id"]);
        $_SESSION["mensaje"] = 'Imagen eliminada correctamente.';
        header('location:editar-evento.php?idEvento='.$idevento);
    }else{
        include('../error.html');
    }
?>