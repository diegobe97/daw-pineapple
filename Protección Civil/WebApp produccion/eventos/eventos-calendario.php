<?php
    require_once("../modelo.php");
    require_once("modelo-evento.php");

    $db = conectar();

    //Specification of the SQL query
    $query='SELECT * FROM evento';
     // Query execution; returns identifier of the result group
    $registros = $db->query($query);

    $eventos = array();

    // cycle to explode every line of the results
    while ($row = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
        $e = array();

        $e['id'] = $row["IdEvento"];
        $e['title'] = $row["TituloEvento"];
        $e['tipo'] = imprimeTipoEvento($row["IdEvento"]);
        $e['description'] = $row["DescripcionEvento"];
        $e['start'] = $row["FechaInicio"];
        $e['end'] = $row["FechaFin"];

        $publicado = $row["EventoPublicado"];
        
        // Si el evento no es un borrador, entonces se visualiza en fullCalendar
        if($publicado == 1)
            array_push($eventos, $e);
    }

    // it releases the associated results
    mysqli_free_result($registros);

    desconectar($db);

    echo json_encode($eventos);
?>