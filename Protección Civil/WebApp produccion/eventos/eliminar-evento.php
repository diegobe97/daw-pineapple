<?php
    session_start();
    require_once("modelo-evento.php");

    eliminarEvento($_GET["id"]);

    $_SESSION["mensaje"] = 'Evento eliminado correctamente.'; 

    header("location:ver-eventos.php");
?>