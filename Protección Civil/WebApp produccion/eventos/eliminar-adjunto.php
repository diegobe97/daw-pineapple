<?php
    session_start();
    if(isset($_SESSION["privilegios"])){
        require_once("modelo-evento.php");
        $idevento=eliminarAdjuntoEvento($_GET["id"]);
        $_SESSION["mensaje"] = 'Adjunto eliminado correctamente.';
        header('location:editar-evento.php?idEvento='.$idevento);
    }else{
        include('../error.html');
    }
?>