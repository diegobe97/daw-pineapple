<?php
    session_start();
    setcookie(session_name('CEPCQLogin'),session_id(),time()+12*60*60);

    require_once("modelo-sesion.php");

    $str = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token='.$_POST["idtoken"];
    $json = file_get_contents($str);
    $userInfoArray = json_decode($json,true);
    $googleEmail = $userInfoArray['email'];

    $_SESSION["email"] = $googleEmail;

    $idRol = getIdRolByUserId($_SESSION["email"]);

    $_SESSION["privilegios"] = getPrivilegiosFor($idRol);

    if($_SESSION["privilegios"] == null){
        unset($_SESSION["privilegios"]);
    }else{
        $_SESSION["idUsuario"] = getIdUsuarioByMail($_SESSION["email"]);
    }
?>
