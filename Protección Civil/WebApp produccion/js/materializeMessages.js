$.fn.extend({
    materializeMessages: function(params) {
        $(this).click(function(event) {
            var elemAttrs = {};
            $.each($(this)[0].attributes, function(index, attribute) {
                elemAttrs[attribute.name] = attribute.value;
            });
            elemAttrs['messageId'] = 'modalMessage';
            event.preventDefault();
            event.stopPropagation();
            var Default = {
                iconType: 'info',
                iconColor: 'grey',
                iconSubcolor: 'darken-2',
                rutaConId: '#',
                title: 'dont forget set the title',
                titleColor: 'grey',
                titleSubColor: 'darken-2',
                message: 'dont forget set the message',
                messageColor: 'grey',
                messageSubColor: 'lighten-2',
                bgMessage: 'white',
                bgMessageSubcolor: '',
                bgButtonsContainer: 'white',
                bgButtonsContainerSubcolor: '',
                textButtonsContainer: '',
                button1: false,
                button2: false,
                dismissible: true,
                titleVariables: {},
                messageVariables: {},
                rutaVariables: {}
            }
            var settings = $.extend(Default, params);
            if (typeof settings.titleVariables != "undefined") {
                $.each(settings.titleVariables, function(index, value) {
                    settings.title = settings.title.replace('{{' + index + '}}', elemAttrs[value]);

                });
            }
            if (typeof settings.messageVariables != "undefined") {
                $.each(settings.messageVariables, function(index, value) {
                    settings.message = settings.message.replace('{{' + index + '}}', elemAttrs[value]);

                });
            }

            if (typeof settings.rutaVariables != "undefined") {
                $.each(settings.rutaVariables, function(index, value) {
                    settings.rutaConId = settings.rutaConId.replace('{{' + index + '}}', elemAttrs[value]);
                });
            }

            wraperMessage = '<div id="modalMessage" class="modal msg-modal z-depth-5 ' + settings.bgMessage + ' ' + settings.bgMessageSubcolor + ' ">'+
                                '<div class="modal-content left-align">'+
                                    '<h4 class="' + settings.titleColor + '-text text-' + settings.titleSubColor + '">'+
                                        '<i class="left small material-icons ' + settings.iconColor + '-text text-' + settings.iconSubcolor + '">' +
                                        settings.iconType + '</i>' +
                                        settings.title +
                                    '</h4>'+
                                    '<p class="' + settings.messageColor + '-text text-' + settings.messageSubColor + '" id="messageContent">' + settings.message + '</p>'+
                                '</div>'+
                            '<div class="modal-footer ' + settings.bgButtonsContainer + ' ' + settings.bgButtonsContainerSubcolor + '" id="buttonsMessage">'+
                                '<div class="left-align" id="button1Container"></div>'+
                                '<div class="left-align" id="button2Container"></div>'+
                            '</div>';

            $('body').append(wraperMessage);
            $('#modalMessage').modal({
                complete: function() {
                    $('#modalMessage').modal('close');
                    $('#modalMessage').remove();
                }
            });

            // button 2
            if (settings.button2 === true) {
                button2 = '<a href="'+settings.rutaConId+'" class="btn-flat center-align waves-effect waves-light buttonMessage2 ' + settings.button2Content.textColor + '-text">' + settings.button2Content.text + '</a>';
                $('#button2Container').append(button2);
                if (settings.button2Content.action != undefined) {
                    if (settings.button2Content.action === 'closeMessage') {
                        $("#button2Container").on('click', 'a.buttonMessage2', function() {
                            $('#modalMessage').modal('close');
                            $('#modalMessage').remove();
                        });

                    }
                    else {
                        $("#button2Container").on('click', 'a.buttonMessage2', function() {
                            settings.button2Content.action(elemAttrs);
                        });
                    }
                }
            }
            // button 1
            if (settings.button1 === true) {
                button1 = '<a href="#!" class="btn-flat center-align waves-effect waves-light buttonMessage1 ' + settings.button1Content.textColor + '-text">' + settings.button1Content.text + '</a>';
                $('#button1Container').append(button1);
                if (settings.button1Content.action != undefined) {
                    if (settings.button1Content.action === 'closeMessage') {
                        $("#button1Container").on('click', 'a.buttonMessage1', function() {
                            $('#modalMessage').modal('close');
                            $('#modalMessage').remove();
                        });

                    }
                    else {
                        $("#button1Container").on('click', 'a.buttonMessage1', function() {
                            settings.button1Content.action(elemAttrs);
                        });
                    }
                }
            }
            $('#modalMessage').modal('open', {
                dismissible: settings.dismissible,
                ready: function() {
                    if (settings.autoclose === true) {
                        window.setTimeout(function() {
                            $('#modalMessage').modal('close');
                            $('#modalMessage').remove();
                        }, settings.secondsToClose * 1000)
                    }
                },
                complete: function() {
                    alert('Closed');
                    $('#modalMessage').remove();
                }
            });
        });
    }
});
