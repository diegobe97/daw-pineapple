document.addEventListener("DOMContentLoaded", function() {
    var elements = $("input:text");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("Este campo es obligatorio");
            }
        };
        elements[i].oninput = function(e) {
            e.target.setCustomValidity("");
        };
    }
    var elements = document.getElementsByTagName("TEXTAREA");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("Este campo es obligatorio");
            }
        };
        elements[i].oninput = function(e) {
            e.target.setCustomValidity("");
        };
    }
})

$(document).ready(function() {
    $(".button-collapse").sideNav();
    $('.collapsible').collapsible();
    $('.slider-home').slider({
        indicators: true,
        interval: 10000
    });
    $('.slider-articulo').slider({
        indicators: true,
        interval: 10000,
        height: 650
    });
    $('.slider-evento').slider({
        indicators: true,
        interval: 10000,
        height: 650
    });
    $(".dropdown-button").dropdown({
        hover: true
    });
    $('.modal').modal();

    $('.carousel.carousel-slider').carousel({fullWidth: true});

    $('.parallax').parallax();
    $('.carousel').carousel();
    $('select').material_select();

    // Para datepicker de eventos
    $('.datepicker').pickadate({
        monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        weekdaysLetter: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        today: 'Hoy',
        clear: 'Limpiar',
        close: 'OK',
        formatSubmit: 'yyyy-mm-dd',
        hiddenSuffix: '_php',
        firstDay: 1,
        min: true,
        closeOnSelect: false, // Close upon selecting a date

        // Accessibility labels
        labelMonthNext: 'Mes siguiente',
        labelMonthPrev: 'Mes anterior',
        labelMonthSelect: 'Selecciona un mes',
        labelYearSelect: 'Selecciona un año',
    });

    // Para datepicker de artículo
    $.extend($.fn.pickadate.defaults, {
        monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        showMonthsShort: true,
        weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        today: 'Hoy',
        clear: 'Limpiar',
        close: 'OK',
        formatSubmit: 'yyyy-mm-dd',
        hiddenSuffix: '_php',
        // hiddenName: true,
        firstDay: 1,
        min: true,
        closeOnSelect: false, // Close upon selecting a date

        // Accessibility labels
        labelMonthNext: 'Mes siguiente',
        labelMonthPrev: 'Mes anterior',
        labelMonthSelect: 'Selecciona un mes',
        labelYearSelect: 'Selecciona un año',
    });

    // Para timepickers de eventos
    $('.timepicker').pickatime({
        default: 'now', // Set default time: 'now', '1:30AM', '16:30'
        fromnow: 0, // set default time to * milliseconds from now (using with default = 'now')
        twelvehour: true,
        donetext: 'OK', // text for done-button
        cleartext: 'Limpiar', // text for clear-button
        canceltext: 'Cancelar', // Text for cancel-button
        autoclose: false, // automatic close timepicker
        ampmclickable: true, // make AM PM clickable
        aftershow: function() {} //Function for after opening timepicker
    });

    var trig = 1;
    $('#search-icon-inside').addClass('hide');
    $('#close-icon-inside').addClass('hide');

    //fix for chrome
    $("#search").addClass('searchbarfix');
    //animate searchbar width increase to  +150%
    $('#search').click(function(e) {
        //handle other nav elements visibility here to avoid push down
        $('.MenuHeader').addClass('hide');
        $('#search-icon-inside').removeClass('hide');
        $('#close-icon-inside').removeClass('hide');
        if (trig == 1) {
            $('#navfix2').animate({
                width: '+=200',
                'left': '-250px'
            }, 400);
            trig++;
        }

    });

    // if user leaves the form the width will go back to original state
    $("#search").focusout(function() {
        $('#navfix2').animate({
            'left': '0px',
            width: '-=200'
        }, 400);
        trig = trig - 1;
        //handle other nav elements visibility first to avoid push down
        $('.MenuHeader').removeClass('hide');
        $('#search-icon-inside').addClass('hide');
        $('#close-icon-inside').addClass('hide');
    });

    $('#calendario').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },

        fixedWeekCount: false, //Number of weeks displayed in a month view

        events: '../eventos/eventos-calendario.php',

        eventColor: '#4CAF50',

        eventClick: function(calEvent, jsEvent, view) {
            starttime = $.fullCalendar.moment(calEvent.start).format('dddd, D [de] MMMM YYYY, h:mm A');
            endtime = $.fullCalendar.moment(calEvent.end).format('dddd, D [de] MMMM YYYY, h:mm A');

            $('#modal-titulo').html(calEvent.title);
            $('#modal-inicia').html(starttime);
            $('#modal-termina').html(endtime);
            $('#modal-tipo').html(calEvent.tipo);
            $('#modal-descripcion').html(calEvent.description);
            $('#modal-urlvideo').html(calEvent.urlvideo);
            $('#modal-idevento').val(calEvent.id);

            $('#modal-calendario').modal('open');
            $('#modal-editar-evento')["0"].href = "editar-evento.php?idEvento=" + calEvent.id;
            $('#modal-eliminar-evento')["0"]["dataset"].id = calEvent.id;
            $.get( "slider-evento.php", { evento_id: calEvent.id } )
                  .done(function( data ) {
                    $('#slider-evento').html(data);
                    $('.slider-evento').slider({
                        indicators: true,
                        interval: 10000,
                        height: 650
                    });
                }
            );
            $.get( "adjuntos-evento.php", { evento_id: calEvent.id } )
                  .done(function( data ) {
                    $('#adjuntos-evento').html(data);
                }
            );
            $.get( "url-evento.php", { evento_id: calEvent.id } )
                  .done(function( data ) {
                    $('#url-evento').html(data);
                }
            );
        }

    });

    // Ejemplo:
    //<a href="!#" class="right botonBorrar" data-ruta="eliminar-articulo.php" data-id="'.$id.'" data-tipo="articulo">
    //      <i class="material-icons">delete</i>
    //</a>
    $('.botonBorrar').materializeMessages({
        dismissible: true,
        iconType: 'warning',
        iconColor: 'red',
        iconSubcolor: 'darken-2',
        rutaVariables: {
            ruta: 'data-ruta',
            id: 'data-id'
        },
        rutaConId: '{{ruta}}?id={{id}}',
        titleVariables: {
            id: 'data-id',
            tipo: 'data-tipo'
        },
        title: 'Borrar {{tipo}}',
        titleColor: 'grey',
        titleSubColor: 'darken-3',
        messageVariables: {
            id: 'data-id',
            tipo: 'data-tipo',
            accion: 'data-accion'
        },
        message: 'El {{tipo}} será borrado permanentemente de TODO el sistema y no se podrá recuperar.',
        messageColor: 'grey',
        messageSubColor: 'darken-3',
        button1: true,
        button1Content: {
            text: 'Cancelar',
            action: 'closeMessage'
        },
        button2: true,
        button2Content: {
            text: 'Sí, borrar',
        }
    });

    $("#formEmpleado").validate({
      rules: {
        rol: "required",
        departamento: "required",
        nombre: "required",
        apePaterno: "required",
        apeMaterno: "required",
        idEmpleado: {
          required: true,
          number: true
        },
        puesto: "required",
        nombreUsuario: {
          required: true,
          email: true
        }
      },
      messages: {
        rol: "Campo requerido.",
        departamento: "Campo requerido.",
        nombre: "Campo requerido.",
        apePaterno: "Campo requerido.",
        apeMaterno: "Campo requerido.",
        idEmpleado: {
          required: "Campo requerido.",
          number: "Introducir un número válido."
        },
        puesto: "Campo requerido.",
        nombreUsuario: {
          required: "Campo requerido.",
          email: "Introducir email válido."
        }
      },
      errorElement: "div",
      errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          if (placement) {
            $(placement).append(error)
          } else {
            error.insertAfter(element);
          }
        }
    });

    
    // Validaciones para formulario de Eventos
    $(".evento-select[required]").css({display:"inline", height:0, padding:0, width:0});

    $("#form-evento").validate({
      ignore: ".ignore",
      rules: {
          tituloEvento: "required",
          descripcionEvento: "required",
          fechaInicio: "required",
          horaInicio: "required",
          fechaFin: "required",
          horaFin: "required",
          urlVideo: {
              url: true
          },
      },
      messages: {
          'idTipoEvento[]': "Elige al menos un tipo de evento.",
          tituloEvento: "Campo obligatorio.",
          descripcionEvento: "Campo obligatorio.",
          fechaInicio: "Campo obligatorio.",
          horaInicio: "Campo obligatorio.",
          fechaFin: "Campo obligatorio.",
          horaFin: "Campo obligatorio.",
          urlVideo: "Introduce un URL válido."
      },
      errorElement: "div",
      errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          if (placement) {
            $(placement).append(error)
          } else {
            error.insertAfter(element);
          }
        }
    });
    
    $('#form-evento').change(function() {
        $(this).valid();
    });
    
});

function goBack() {
    window.history.back();
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    $.get('https://www.proteccioncivilqro.gob.mx/logout.php', function(data) {
        auth2.signOut().then(function() {
            console.log('User signed out.');
            window.location.href = 'https://accounts.google.com/Logout';
        });
    });

}

function onLoad() {
    gapi.load('auth2', function() {
        gapi.auth2.init();
    });
}
