<?php
    session_start();
    if(isset($_SESSION["privilegios"])){
        require_once("modelo-index.php");
        if(binarySearch($_SESSION["privilegios"], 28)){
            include("../_header.html");
            include("_form-banners.html");
            include('../_user-menu.html');
            include("../_footer.html");
        }else{
            include('../error.html');
        }
    }else{
        include('../error.html');
    }
?>