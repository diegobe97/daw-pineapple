<?php
    session_start();
    require_once("../empleados/modelo-empleados.php");
    require_once("../articulos/modelo-noticias.php");
    require_once("modelo-index.php");

    include('../_header.html');
    include('_inicio.html');

    if(isset($_SESSION["privilegios"])){
        include('../_user-menu.html');
        if (isset($_SESSION["mensaje"])) {
            $mensaje = $_SESSION["mensaje"];
            include('../_mensaje.html');
            unset($_SESSION["mensaje"]);
        }
    }
    include('../_footer.html');
?>
