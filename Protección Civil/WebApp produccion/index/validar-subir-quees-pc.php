<?php
session_start();
require_once "../modelo.php";
unset($_SESSION['imagen']);

$response['errores'] = false;

if(!(($_FILES['imagen']['error']) > 0))
{
    if($_FILES['imagen']['name'] != ""){
        // Rutas para guardar
        $target_dir     = "../images/";
        $target_file    = clean_input($target_dir . $_FILES['imagen']['name']);
        $source_file    = clean_input($_FILES['imagen']['tmp_name']);
        $filename       = clean_input($_FILES['imagen']['name']);
        
        // Propiedades de las fotos
        $size           = $_FILES['imagen']['size'];
        $type           = clean_input(pathinfo($target_file,PATHINFO_EXTENSION));
        
        $uploadOk = false;
        
        // Checar que el archivo realmente sea una imagen
        if(! @is_array(getimagesize($_FILES["imagen"]["tmp_name"]) || getimagesize($_FILES["imagen"]["tmp_name"]))){
            //echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = true;
            
            // Checar que el tamaño sea valido
            if ($size > 5000000) {
                //echo "Sorry, your file is too large.";
                $uploadOk = false;
                $errores['tamaño'] = 'Execede el tamaño máximo';
            }
            // Checar que tenga un formato valido
            if($type != "jpg" && $type != "png" && $type != "jpeg"
            && $type != "gif" ) {
                //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = false;
                $errores['formato'] = 'La imágen debe tener uno de los siguientes formatos: jpg, jpeg, png, gif';
            }
        }
        // El archivo no es una imagen
        else {
            $uploadOk = false;
            $errores['tipo'] = 'Imposible subir este archivo como imagen';
        }
        
        $response['imagen']['nombre'] = clean_input($_FILES['imagen']['name']);

        // Intentar subir la imagen
        if ($uploadOk == false) {
            $response['imagen']['upload'] = false;
            $response['imagen']['mensaje'] = $errores;
        } else {
            $target_file = time() . '-' . $_FILES["imagen"]["name"];
            if (move_uploaded_file($_FILES["imagen"]["tmp_name"], '../images/' . $target_file)) {
                $response['imagen']['upload'] = true;
                
                // Actualizar el arreglo con el nuevo id y para poder crear la relacion articulo-imagen en la BD
                $_SESSION['imagen']['nombre_real'] = $target_file;
                $_SESSION['imagen']['nombre_usuario'] = clean_input($_FILES['imagen']['name']);
            } else {
                $response['imagen']['upload'] = false;
                $response['imagen']['mensaje']['general'] = 'El archivo ' . $name . ' no puede subirse en este momento';
            }
        }
        // Validar si la foto tuvo errores
        if ($response['imagen']['upload'] === false){
           $response['errores'] = true; 
        }
    }
}

function clean_input($s){
    $s = preg_replace('/\s+/', '', $s);
    $s = strtolower($s);
    $s = htmlspecialchars($s);
    return $s;
}

echo json_encode($response);

?>