<?php
    require_once("../modelo.php");
    
    function getTextoIndex() {
        $db = conectar();

        //Specification of the SQL query
        $query='SELECT * FROM textoinicio WHERE Id=1';
        // Query execution; returns identifier of the result group
        $texto = $db->query($query);

        $result = "";

        // cycle to explode every line of the results
        while ($fila = mysqli_fetch_array($texto, MYSQLI_BOTH)) {
            $result .= $fila["Texto"];
        }

        // it releases the associated results
        mysqli_free_result($texto);

        desconectar($db);

        return $result;
    }
    
    function editarTextoIndex($textoIndex){
        $db = conectar();

        // insert command specification
        if(isset($_SESSION['imagen'])){
            // Para borrar la imagen del servidor
            $query = 'SELECT * FROM textoinicio WHERE Id1';
            $registros = $db->query($query);
            $fila = mysqli_fetch_array($registros, MYSQLI_ASSOC);
            $borrar =   "../images/" . $fila["RutaImagen"];
            unlink($borrar);


            $query='UPDATE textoinicio SET Texto=?, RutaImagen=?, RutaImagenUsuario=? WHERE Id=1';
            $RutaImagen=$_SESSION['imagen']['nombre_real'];
            $RutaImagenUsuario=$_SESSION['imagen']['nombre_usuario'];
            unset($_SESSION['imagen']);
            // Preparing the statement
            if (!($statement = $db->prepare($query))) {
                die("Preparation failed: (" . $db->errno . ") " . $db->error);
            }
            // Binding statement params
            if (!$statement->bind_param("sss", $textoIndex, $RutaImagen, $RutaImagenUsuario)) {
                die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
            }
            // update execution
            if (!$statement->execute()) {
                die("Update failed: (" . $statement->errno . ") " . $statement->error);
            }
        }else{
            $query='UPDATE textoinicio SET Texto=? WHERE Id=1';
            // Preparing the statement
            if (!($statement = $db->prepare($query))) {
                die("Preparation failed: (" . $db->errno . ") " . $db->error);
            }
            // Binding statement params
            if (!$statement->bind_param("s", $textoIndex)) {
                die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
            }
            // update execution
            if (!$statement->execute()) {
                die("Update failed: (" . $statement->errno . ") " . $statement->error);
            }
        }

        desconectar($db);
    }


    function getProximosEventos() {
        $db = conectar();
        //Specification of the SQL query
        $query='SELECT * 
        FROM evento
        WHERE EventoPublicado = 1
        ORDER BY FechaInicio DESC 
        LIMIT 3';
         // Query execution; returns identifier of the result group
        $registros = $db->query($query);

        $cards = '';
        // cycle to explode every line of the results
        while ($row = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
            
            $date = date('d F Y h:ia', strtotime($row["FechaInicio"]));
            $translate = array("January" => "Enero", 
                        "February" => "Febrero", 
                        "March" => "Marzo", 
                        "April" => "Abril", 
                        "May" => "Mayo", 
                        "June" => "Junio", 
                        "July" => "Julio",
                        "August" => "Agosto", 
                        "September" => "Septiembre", 
                        "October" => "Octubre", 
                        "November" => "Noviembre", 
                        "December" => "Diciembre");
            $date = strtr($date, $translate);
            $date = explode(" ", $date);
            

            $cards .=
            '<a href="../eventos/_evento.php?id_evento='.$row["IdEvento"].'">
                <div class="col s12 m6 l3">
                    <div class="card small hoverable event-card">
                        <div class="prox-eventos">
                            <div class="center event-date event-day">'.$date[0].'</div>
                            <div class="center event-date event-month">'.strtoupper($date[1]).'</div>
                            <div class="center event-date event-time">'.$date[3].'</div>
                        </div>
                        <div class="center card-content"> 
                            <span class="card-title truncate">'.$row["TituloEvento"].'</span>
                        </div>
                    </div>
                </div>
            </a>';
        }
        // it releases the associated results
        mysqli_free_result($registros);

        desconectar($db);

        $cards .= '';

        return $cards;
    }

    function getImagenIndexQueEsPC() {
        $db = conectar();
        $query ='SELECT * FROM textoinicio WHERE Id=1';
        $registros = $db->query($query);
        $inicio = mysqli_fetch_array($registros, MYSQLI_ASSOC);
        desconectar($db);
        return $inicio["RutaImagen"];
    }

    function getImagenIndexQueEsPCUsuario() {
        $db = conectar();
        $query ='SELECT * FROM textoinicio WHERE Id=1';
        $registros = $db->query($query);
        $inicio = mysqli_fetch_array($registros, MYSQLI_ASSOC);
        desconectar($db);
        return $inicio["RutaImagenUsuario"];
    }

   function getSliderBanner() {
        $db = conectar();

        $slider = '<div class="slider slider-home">
                    <ul class="slides">';  

        //Specification of the SQL query
        $query = 'SELECT * FROM banners';
        // Query execution; returns identifier of the result group
        $registros = $db->query($query);

        // cycle to explode every line of the results
        while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
            // Options: MYSQLI_NUM to use only numeric indexes
            // MYSQLI_ASSOC to use only name (string) indexes
            // MYSQLI_BOTH, to use both
            $imagen =   "../images/" . $fila['RutaBanner'];

            $slider.='  <li>
                            <img src="'.$imagen.'"> 
                        </li>';
        }
        
        $slider .= ' </ul>
                   </div>';

        // it releases the associated results
        mysqli_free_result($registros);

        desconectar($db);

        return $slider;
    }

    function getImagenesBanner() {
        $db = conectar();

        $imagenes = '<div class="row">'; 

        //Specification of the SQL query
        $query = 'SELECT * FROM banners';
        // Query execution; returns identifier of the result group
        $registros = $db->query($query);

        // cycle to explode every line of the results
        while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
            // Options: MYSQLI_NUM to use only numeric indexes
            // MYSQLI_ASSOC to use only name (string) indexes
            // MYSQLI_BOTH, to use both
            $imagen =   "../images/" . $fila['RutaBanner'];
            $id     = $fila["IdBanner"];

            $imagenes .='  <div class="col s12 m12 l12 center-align">
                                <div class="row valign-wrapper"> 
                                    <div class="col s11 m11 l11">
                                        <img class="materialboxed" src="'.$imagen.'">
                                    </div>
                                    <div class="col s1 m1 l1">';

            if(isset($_SESSION["privilegios"])){
                if(binarySearch($_SESSION["privilegios"], 28)){
                    $imagenes .= '  <a href="#!" class="right botonBorrar btn-floating red" data-ruta="eliminar-banner.php"
                                    data-id="' . $id . '" data-tipo="banner">
                                            <i class="material-icons">delete_forever</i>
                                    </a>';
                }
            }

            $imagenes .='                            
                                    </div>
                                </div>
                            </div>';
        }
        
        $imagenes .= '</div>';

        // it releases the associated results
        mysqli_free_result($registros);

        desconectar($db);

        return $imagenes;
    }

    function eliminarBanner($id)
    {
        $db = conectar();

        $db = conectar();
        $query = 'SELECT * FROM banners WHERE IdBanner =' . $id;
        $registros = $db->query($query);
        $fila = mysqli_fetch_array($registros, MYSQLI_ASSOC);
        $imagen =   "../images/" . $fila['RutaBanner'];
        unlink($imagen);

        mysqli_free_result($registros);
        
        $query = 'DELETE FROM banners WHERE IdBanner=?';

        if (!($statement = $db->prepare($query))) {
            die("Preparación fallida: (" . $db->errno . ") " . $db->error);
        }
        if (!($statement->bind_param("s", $id))) {
            die("Vinculación de parámetros fallida: (" . $statement->errno . ") " . $statement->error);
        }
        if (!$statement->execute()) {
            die("Ejecución fallida: (" . $statement->errno . ") " . $statement->error);
        }
  
        desconectar($db);
        return;
    }

    function guardarBanners() {
        $db = conectar();

        if (isset($_SESSION['imagenes'])) {
            foreach ($_SESSION['imagenes'] as $key => $nombre_real) {
                $RutaBanner=$_SESSION['imagenes'][$key]['nombre_real'];
                $RutaBannerUsuario=$_SESSION['imagenes'][$key]['nombre_usuario'];
                $query='INSERT INTO banners (`RutaBanner` ,`RutaBannerUsuario`) VALUES (?,?)';
                // Preparing the statement
                if (!($statement = $db->prepare($query))) {
                    die("Preparation failed: (" . $db->errno . ") " . $db->error);
                }
                // Binding statement params
                if (!$statement->bind_param("ss", $RutaBanner, $RutaBannerUsuario)) {
                    die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
                }
                // Executing the statement
                if (!$statement->execute()) {
                    die("Execution failed: (" . $statement->errno . ") " . $statement->error);
                }
            }
        unset($_SESSION['imagenes']);
        }

        desconectar($db);
        return;
    }

    function getImagenesAtendemos() {
        $db = conectar();

        //Specification of the SQL query
        $query = 'SELECT * FROM atendemos';
        // Query execution; returns identifier of the result group
        $registros = $db->query($query);

        $imagenes = '<div class="row">'; 

        // cycle to explode every line of the results
        while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
            // Options: MYSQLI_NUM to use only numeric indexes
            // MYSQLI_ASSOC to use only name (string) indexes
            // MYSQLI_BOTH, to use both
            $imagen =   "../images/" . $fila['RutaImagenAtendemos'];
            $id     = $fila["IdAtendemos"];

            $imagenes .='  <div class="col s6 m3 l3 center-align">
                                <div class="card small hoverable tramitamos atendemos">
                                    <div class="card-image">
                                        <img width="650" src="'.$imagen.'">
                                    </div>
                                    <div class="card-content">';
                                        
            if(isset($_SESSION["privilegios"])){
                if(binarySearch($_SESSION["privilegios"], 28)){
                    $imagenes .= '  <a href="#!" class="botonBorrar btn-floating red" data-ruta="eliminar-atendemos.php"
                                    data-id="' . $id . '" data-tipo="logo de atendemos">
                                            <i class="material-icons">delete_forever</i>
                                    </a>';
                }
            }

            $imagenes .='                            
                                    </div>
                                </div>
                            </div>';
        }
        
        $imagenes .= '</div>';

        // it releases the associated results
        mysqli_free_result($registros);

        desconectar($db);

        return $imagenes;
    }

    function getCarouselAtendemos(){
         $db = conectar();

        //Specification of the SQL query
        $query = 'SELECT * FROM atendemos';
        // Query execution; returns identifier of the result group
        $registros = $db->query($query);

        $carousel='<div class="carousel">';
        while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
            $imagen =   "../images/" . $fila['RutaImagenAtendemos'];
            $carousel.='<a class="carousel-item" href="#one!"><img src="'.$imagen.'"></a>';
        }
        $carousel .='</div>';

        mysqli_free_result($registros);

        desconectar($db);

        return $carousel;
    }

    function getCarouselTramitamos() {
        $db = conectar();

        //Specification of the SQL query
        $query = 'SELECT * FROM tramitamos';
        // Query execution; returns identifier of the result group
        $registros = $db->query($query);

        $carousel='<div class="carousel">';
        while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
            $imagen =   "../images/" . $fila['RutaImagenTramitamos'];
            $carousel.='<a class="carousel-item" href="#one!"><img src="'.$imagen.'"></a>';
        }
        $carousel .='</div>';

        mysqli_free_result($registros);

        desconectar($db);

        return $carousel;
    }

    function guardarAtendemos() {
        $db = conectar();

        if (isset($_SESSION['imagenes'])) {
            foreach ($_SESSION['imagenes'] as $key => $nombre_real) {
                $RutaImagenAtendemos = $_SESSION['imagenes'][$key]['nombre_real'];
                $query='INSERT INTO atendemos (`RutaImagenAtendemos`) VALUES (?)';
                // Preparing the statement
                if (!($statement = $db->prepare($query))) {
                    die("Preparation failed: (" . $db->errno . ") " . $db->error);
                }
                // Binding statement params
                if (!$statement->bind_param("s", $RutaImagenAtendemos)) {
                    die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
                }
                // Executing the statement
                if (!$statement->execute()) {
                    die("Execution failed: (" . $statement->errno . ") " . $statement->error);
                }
            }
        unset($_SESSION['imagenes']);
        }

        desconectar($db);
        return;
    }

     function guardarTramitamos() {
        $db = conectar();

        if (isset($_SESSION['imagenes'])) {
            foreach ($_SESSION['imagenes'] as $key => $nombre_real) {
                $RutaImagenTramitamos = $_SESSION['imagenes'][$key]['nombre_real'];
                $query='INSERT INTO tramitamos (`RutaImagenTramitamos`) VALUES (?)';
                // Preparing the statement
                if (!($statement = $db->prepare($query))) {
                    die("Preparation failed: (" . $db->errno . ") " . $db->error);
                }
                // Binding statement params
                if (!$statement->bind_param("s", $RutaImagenTramitamos)) {
                    die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
                }
                // Executing the statement
                if (!$statement->execute()) {
                    die("Execution failed: (" . $statement->errno . ") " . $statement->error);
                }
            }
        unset($_SESSION['imagenes']);
        }

        desconectar($db);
        return;
    }

    function eliminarAtendemos($id) {
        $db = conectar();

        $query = 'SELECT * FROM atendemos WHERE IdAtendemos =' . $id;
        $registros = $db->query($query);
        $fila = mysqli_fetch_array($registros, MYSQLI_ASSOC);
        $imagen =   "../images/" . $fila['RutaImagenAtendemos'];
        unlink($imagen);

        mysqli_free_result($registros);

        
        $query = 'DELETE FROM atendemos WHERE IdAtendemos=?';

        if (!($statement = $db->prepare($query))) {
            die("Preparación fallida: (" . $db->errno . ") " . $db->error);
        }
        if (!($statement->bind_param("s", $id))) {
            die("Vinculación de parámetros fallida: (" . $statement->errno . ") " . $statement->error);
        }
        if (!$statement->execute()) {
            die("Ejecución fallida: (" . $statement->errno . ") " . $statement->error);
        }
  
        desconectar($db);
        return;
    }

    function eliminarTramitamos($id) {
        $db = conectar();
        $query = 'SELECT * FROM tramitamos WHERE IdTramitamos =' . $id;
        $registros = $db->query($query);
        $fila = mysqli_fetch_array($registros, MYSQLI_ASSOC);
        $imagen =   "../images/" . $fila['RutaImagenTramitamos'];
        unlink($imagen);

        mysqli_free_result($registros);

        
        $query = 'DELETE FROM tramitamos WHERE IdTramitamos=?';

        if (!($statement = $db->prepare($query))) {
            die("Preparación fallida: (" . $db->errno . ") " . $db->error);
        }
        if (!($statement->bind_param("s", $id))) {
            die("Vinculación de parámetros fallida: (" . $statement->errno . ") " . $statement->error);
        }
        if (!$statement->execute()) {
            die("Ejecución fallida: (" . $statement->errno . ") " . $statement->error);
        }
  
        desconectar($db);
        return;
    }

    function getImagenesTramitamos() {
        $db = conectar();

        //Specification of the SQL query
        $query = 'SELECT * FROM tramitamos';
        // Query execution; returns identifier of the result group
        $registros = $db->query($query);

        $imagenes = '<div class="row">'; 

        // cycle to explode every line of the results
        while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
            // Options: MYSQLI_NUM to use only numeric indexes
            // MYSQLI_ASSOC to use only name (string) indexes
            // MYSQLI_BOTH, to use both
            $imagen =   "../images/" . $fila['RutaImagenTramitamos'];
            $id     = $fila["IdTramitamos"];

            $imagenes .='  <div class="col s6 m3 l3 center-align">
                                <div class="card small hoverable tramitamos atendemos">
                                    <div class="card-image">
                                        <img width="650" src="'.$imagen.'">
                                    </div>
                                    <div class="card-content">';
                                        
            if(isset($_SESSION["privilegios"])){
                if(binarySearch($_SESSION["privilegios"], 28)){
                    $imagenes .= '  <a href="#!" class="botonBorrar btn-floating red" data-ruta="eliminar-tramitamos.php"
                                    data-id="' . $id . '" data-tipo="logo de tramitamos">
                                            <i class="material-icons">delete_forever</i>
                                    </a>';
                }
            }

            $imagenes .='                            
                                    </div>
                                </div>
                            </div>';
        }
        
        $imagenes .= '</div>';

        // it releases the associated results
        mysqli_free_result($registros);

        desconectar($db);

        return $imagenes;
    }

    function getImagenInicio() {
        $db = conectar();
        $query = 'SELECT * FROM textoinicio WHERE Id=1';
        $registros = $db->query($query);
        $fila = mysqli_fetch_array($registros, MYSQLI_BOTH);
        $imagen =   "../images/" . $fila["RutaImagen"];

        $imagen = ' 
                        <div class="col s11 m11 l11">
                            <img class="materialboxed" src="'.$imagen.'">
                        </div>
                   ';
        desconectar($db);
        return $imagen;
    }


?>




