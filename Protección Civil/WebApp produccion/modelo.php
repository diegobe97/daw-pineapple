<?php
    function conectar() {
        $servername = "localhost";
        $username = "protecci_admin";
        $password = "rooter";
        $dbname = "protecci_cepcq";

        $con = mysqli_connect($servername, $username, $password, $dbname);

        if(!$con){
            die("Connection failed: ".mysqli_connect_error());
        }

        $con->set_charset('utf8');

        return $con;
    }

    function desconectar($mysql) {
        mysqli_close($mysql);
    }

    function binarySearch($array, $value){
        return in_array($value, $array);
    }

    function trunc($str, $len) {
        if (strlen($str) > $len) {
            $str = substr($str, 0, $len) . "...";
        }
        return $str;
    }

    function getFechaConFormato($fechabd){
        $fecha  =   date('d F Y', strtotime($fechabd));

        $trans = array("January" => "Enero",
                        "February" => "Febrero",
                        "March" => "Marzo",
                        "April" => "Abril",
                        "May" => "Mayo",
                        "June" => "Junio",
                        "July" => "Julio",
                        "August" => "Agosto",
                        "September" => "Septiembre",
                        "October" => "Octubre",
                        "November" => "Noviembre",
                        "December" => "Diciembre" );

        $fecha = strtr($fecha, $trans);
        return $fecha;
    }
?>
