<?php
    session_start();
    require_once("modelo-departamentos.php");

    if(isset($_POST["IdDepartamento"])) {
        editarDepartamento($_POST["IdDepartamento"], htmlspecialchars($_POST["nombreDep"]), htmlspecialchars($_POST["descripcionDep"]));
        $_SESSION["mensaje"] = htmlspecialchars($_POST["nombreDep"]).' se actualizó correctamente.';
    } else {
        guardarDepartamento(htmlspecialchars($_POST["nombreDep"]), htmlspecialchars($_POST["descripcionDep"]));
        $_SESSION["mensaje"] = htmlspecialchars($_POST["nombreDep"]).' se creó correctamente.';
    }

    header("location:ver-departamentos.php");
?>
