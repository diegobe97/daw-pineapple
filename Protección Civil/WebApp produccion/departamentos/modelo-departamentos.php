<?php
    require_once("../modelo.php");

    function getDepartamentos($privilegios) {
        $db = conectar();

        //Specification of the SQL query
        $query='SELECT * FROM departamento';
        // Query execution; returns identifier of the result group
        $roles = $db->query($query);

        $result = "";

        $i = 1;
        
        $result = '<table class="striped">
            <thead>
                <tr>
                    <th></th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                </tr>
            </thead>
            <tbody>';

        // cycle to explode every line of the results
        while ($fila = mysqli_fetch_array($roles, MYSQLI_BOTH)) {
            $result .= '
            <tr>
                <td>'.$i.'.  </td>
                <td>'.$fila["NombreDepartamento"].'</td>
                <td>'.$fila["DescripcionDepartamento"].'</td>';

                if(binarySearch($privilegios, 26) && binarySearch($privilegios, 27)){
                    $result .= '<td>
                        <a href="editar-departamento.php?IdDepartamento='.$fila["IdDepartamento"].'" class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="Editar departamento"><i class="material-icons">edit</i></a>
                    </td>
                    <td>
                        <a href="#" class="btn-floating red tooltipped botonBorrar" data-id="'.$fila["IdDepartamento"].'" data-ruta="eliminar-departamento.php" data-tipo="departamento" data-position="top" data-delay="50" data-tooltip="Eliminar departamento"><i class="material-icons">delete_forever</i></a>
                    </td>';
                }else if(binarySearch($privilegios, 26)){
                    $result .= '<td>
                        <a href="editar-departamento.php?       IdDepartamento='.$fila["IdDepartamento"].'" class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="Editar departamento"><i class="material-icons">edit</i></a>
                    </td>
                    <td></td>';
                }else if(binarySearch($privilegios, 27)){
                    $result .= '<td></td>
                    <td>
                        <a href="eliminar-departamento.php?IdDepartamento='.$fila["IdDepartamento"].'" class="btn-floating red tooltipped" data-position="top" data-delay="50" data-tooltip="Eliminar departamento"><i class="material-icons">delete_forever</i></a>
                    </td>';
                }else{
                    $result .= '<td></td><td></td>';
                }
            
            $result .= '</tr>';
            
            $i++;
        }
        
        $result .= '</tbody></table>';

        // it releases the associated results
        mysqli_free_result($roles);

        desconectar($db);

        return $result;
    }

    function getDropdownDepartamentos($IdDep = 0){
        $db = conectar();

        //Specification of the SQL query
        $query='SELECT * FROM departamento ORDER BY NombreDepartamento';
        // Query execution; returns identifier of the result group
        $dep = $db->query($query);

        $result = "";

        // cycle to explode every line of the results
        while ($fila = mysqli_fetch_array($dep, MYSQLI_BOTH)) {
            if($fila["IdDepartamento"] == $IdDep){
              $result .= '<option value="'.$fila["IdDepartamento"].'" selected>'.$fila["NombreDepartamento"].'</option>';
            }else{
              $result .= '<option value="'.$fila["IdDepartamento"].'">'.$fila["NombreDepartamento"].'</option>';
            }
        }

        // it releases the associated results
        mysqli_free_result($dep);

        desconectar($db);

        return $result;
    }

    function guardarDepartamento($nombre, $descripcion){
        $db = conectar();

        $query='INSERT INTO departamento (`NombreDepartamento`, `DescripcionDepartamento`) VALUES (?,?)';

        // Preparing the statement
        if (!($statement = $db->prepare($query))) {
            die("Preparation failed: (" . $db->errno . ") " . $db->error);
        }
        // Binding statement params
        if (!$statement->bind_param("ss", $nombre, $descripcion)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
        }
        // Executing the statement
        if (!$statement->execute()) {
            die("Execution failed: (" . $statement->errno . ") " . $statement->error);
        }

        desconectar($db);
    }

    function editarDepartamento($id, $nombre, $descripcion){
        $db = conectar();

        // insert command specification
        $query='UPDATE departamento SET NombreDepartamento=?, DescripcionDepartamento=? WHERE IdDepartamento=?';
        // Preparing the statement
        if (!($statement = $db->prepare($query))) {
            die("Preparation failed: (" . $db->errno . ") " . $db->error);
        }
        // Binding statement params
        if (!$statement->bind_param("sss", $nombre, $descripcion, $id)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
        }
        // update execution
        if ($statement->execute()) {
            //echo 'There were ' . mysqli_affected_rows($db) . ' affected rows';
        } else {
            die("Update failed: (" . $statement->errno . ") " . $statement->error);
        }

        desconectar($db);
    }

    function getByIdDepartamento($db, $IdDepartamento){
        //Specification of the SQL query
        $query='SELECT * FROM departamento WHERE IdDepartamento = "'.$IdDepartamento.'"';
         // Query execution; returns identifier of the result group
        $registros = $db->query($query);
        $departamento = mysqli_fetch_array($registros, MYSQLI_BOTH);
        return $departamento;
    }

    function eliminarDepartamento($IdDepartamento){
        $db = conectar();

        // insert command specification
        $query='DELETE FROM departamento WHERE IdDepartamento = ?';

        // Preparing the statement
        if (!($statement = $db->prepare($query))) {
            die("Preparation failed: (" . $db->errno . ") " . $db->error);
        }
        // Binding statement params
        if (!$statement->bind_param("s", $IdDepartamento)) {
                die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
            }
        // Executing the statement
        if (!$statement->execute()) {
            die("Execution failed: (" . $statement->errno . ") " . $statement->error);
        }

        desconectar($db);
    }
?>
