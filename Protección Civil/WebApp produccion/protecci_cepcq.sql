-- MySQL dump 10.13  Distrib 5.6.32-78.1, for Linux (x86_64)
--
-- Host: localhost    Database: protecci_cepcq
-- ------------------------------------------------------
-- Server version	5.6.32-78.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `archivosarticulo`
--

DROP TABLE IF EXISTS `archivosarticulo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archivosarticulo` (
  `idArchivosArticulos` int(11) NOT NULL AUTO_INCREMENT,
  `rutaArchivos` varchar(300) NOT NULL,
  `idPublicacion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idArchivosArticulos`),
  KEY `idPublicacion` (`idPublicacion`),
  CONSTRAINT `archivosarticulo_ibfk_1` FOREIGN KEY (`idPublicacion`) REFERENCES `publicacion` (`IdPublicacion`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archivosarticulo`
--

LOCK TABLES `archivosarticulo` WRITE;
/*!40000 ALTER TABLE `archivosarticulo` DISABLE KEYS */;
INSERT INTO `archivosarticulo` (`idArchivosArticulos`, `rutaArchivos`, `idPublicacion`) VALUES (37,'1512424176-REQUISISTOS ANALISIS DE RIESGOS.pdf',NULL),(45,'1512500831-Requisitos Aforo.pdf',136),(46,'1512501060-Aforo.pdf',137),(47,'1512501060-Formato Único.pdf',137),(48,'1512501060-Lista de procedimientos para la Autorización de Eventos Socio-Organizativos 2017.pdf',137),(49,'1512501060-Plan de Acción Eventos.pdf',137),(50,'1512501315-Firma de enterado de Requisitos para Alcoholes.pdf',139),(51,'1512501315-Formato Único.pdf',139),(52,'1512502074-Ley de Protección Civil del Estado de Querétaro.pdf',NULL),(53,'1512502074-Ley General de Protección Civil.pdf',NULL),(54,'1512502074-Plan Nacional de Protección Civil.pdf',142),(55,'1512502074-Reglamento de la Ley General de Protección Civil.pdf',142),(56,'1512502264-Ley de Protección Civil del Estado de Querétaro.pdf',142),(57,'1512502264-Ley General de Protección Civil.pdf',142);
/*!40000 ALTER TABLE `archivosarticulo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `archivosevento`
--

DROP TABLE IF EXISTS `archivosevento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archivosevento` (
  `IdArchivosEventos` int(11) NOT NULL AUTO_INCREMENT,
  `RutaArchivos` varchar(300) CHARACTER SET utf8 NOT NULL,
  `IdEvento` int(11) DEFAULT NULL,
  PRIMARY KEY (`IdArchivosEventos`),
  KEY `IdPublicacion` (`IdEvento`),
  CONSTRAINT `archivosevento_ibfk_1` FOREIGN KEY (`IdEvento`) REFERENCES `evento` (`IdEvento`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archivosevento`
--

LOCK TABLES `archivosevento` WRITE;
/*!40000 ALTER TABLE `archivosevento` DISABLE KEYS */;
INSERT INTO `archivosevento` (`IdArchivosEventos`, `RutaArchivos`, `IdEvento`) VALUES (1,'1511928959-24058687_1533893450033633_4137293198602541809_n.jpg',NULL),(2,'1511929238-imagen-de-prueba-320x240.jpg',NULL),(3,'1511929802-Examen DAW AD 2017 parcial 2.pdf',NULL),(4,'1511930130-Examen DAW AD 2017 parcial 2.pdf',NULL),(5,'1511930645-Examen DAW AD 2017 parcial 2.pdf',NULL),(6,'1511930685-Examen DAW AD 2017 parcial 2.pdf',NULL),(7,'1511930738-Examen DAW AD 2017 parcial 2.pdf',NULL),(8,'1511931807-Examen DAW AD 2017 parcial 2.pdf',NULL),(9,'1511932102-Examen DAW AD 2017 parcial 2.pdf',NULL),(10,'1511932151-Examen DAW AD 2017 parcial 2.pdf',NULL),(11,'1511932393-codiciosos.pdf',NULL),(12,'1511935781-recursivos.pdf',NULL),(13,'1511935972-recursivos.pdf',NULL),(14,'1511936029-recursivos.pdf',NULL),(15,'1511936031-recursivos.pdf',NULL),(19,'1512129141-grafos.pdf',22);
/*!40000 ALTER TABLE `archivosevento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asignadoa`
--

DROP TABLE IF EXISTS `asignadoa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asignadoa` (
  `IdUsuario` int(11) NOT NULL,
  `IdRol` int(8) NOT NULL,
  KEY `fk_idRol` (`IdRol`),
  KEY `fk_AsignadoAUsuario` (`IdUsuario`),
  CONSTRAINT `fk_AsignadoAUsuario` FOREIGN KEY (`IdUsuario`) REFERENCES `usuario` (`IdUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asignadoa`
--

LOCK TABLES `asignadoa` WRITE;
/*!40000 ALTER TABLE `asignadoa` DISABLE KEYS */;
INSERT INTO `asignadoa` (`IdUsuario`, `IdRol`) VALUES (5,2),(1,1),(6,2),(7,2),(8,2),(9,2),(10,2),(11,1),(13,0),(15,2),(17,1),(18,1),(19,1);
/*!40000 ALTER TABLE `asignadoa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `atendemos`
--

DROP TABLE IF EXISTS `atendemos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atendemos` (
  `IdAtendemos` int(11) NOT NULL AUTO_INCREMENT,
  `RutaImagenAtendemos` varchar(300) NOT NULL,
  PRIMARY KEY (`IdAtendemos`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atendemos`
--

LOCK TABLES `atendemos` WRITE;
/*!40000 ALTER TABLE `atendemos` DISABLE KEYS */;
INSERT INTO `atendemos` (`IdAtendemos`, `RutaImagenAtendemos`) VALUES (24,'1512148785-Sismos.png'),(25,'1512148785-Químicos.png'),(26,'1512148785-Inundaciones.png'),(27,'1512148785-Heladas.png'),(28,'1512148785-EventosMasivos.png'),(29,'1512148785-AnálisisDeRiesgos.png');
/*!40000 ALTER TABLE `atendemos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `IdBanner` int(11) NOT NULL AUTO_INCREMENT,
  `RutaBanner` varchar(300) NOT NULL,
  `RutaBannerUsuario` varchar(300) NOT NULL,
  PRIMARY KEY (`IdBanner`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` (`IdBanner`, `RutaBanner`, `RutaBannerUsuario`) VALUES (29,'1512148343-banner2.jpg','banner2.jpg');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bitacora`
--

DROP TABLE IF EXISTS `bitacora`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bitacora` (
  `IdBitacora` int(11) NOT NULL AUTO_INCREMENT,
  `IdUsuario` int(11) NOT NULL,
  `IdPublicacion` int(11) NOT NULL,
  `FechaBitacora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DescripcionBitacora` varchar(500) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`IdBitacora`),
  KEY `IdPublicacion` (`IdPublicacion`),
  KEY `IdUsuario` (`IdUsuario`),
  CONSTRAINT `IdPublicacion` FOREIGN KEY (`IdPublicacion`) REFERENCES `publicacion` (`IdPublicacion`),
  CONSTRAINT `IdUsuario` FOREIGN KEY (`IdUsuario`) REFERENCES `usuario` (`IdUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bitacora`
--

LOCK TABLES `bitacora` WRITE;
/*!40000 ALTER TABLE `bitacora` DISABLE KEYS */;
INSERT INTO `bitacora` (`IdBitacora`, `IdUsuario`, `IdPublicacion`, `FechaBitacora`, `DescripcionBitacora`) VALUES (2,17,123,'2017-12-01 17:23:23','Creó el artículo.'),(5,17,126,'2017-12-01 17:25:54','Creó el artículo.'),(9,17,126,'2017-12-02 03:02:33','Cambios: Adjuntos'),(15,17,126,'2017-12-04 21:12:21','Cambios: Adjuntos'),(16,17,126,'2017-12-04 21:14:54','Cambios: Adjuntos'),(17,1,132,'2017-12-04 21:27:09','Creó el artículo.'),(18,1,132,'2017-12-04 21:29:00','Cambios: Descripción'),(19,1,132,'2017-12-04 21:30:04','Cambios: Descripción'),(20,1,133,'2017-12-04 21:31:27','Creó el artículo.'),(22,1,135,'2017-12-04 21:42:22','Creó el artículo.'),(23,17,126,'2017-12-04 21:43:28','Cambios: Imágenes'),(24,17,126,'2017-12-04 21:44:03','Cambios: Imágenes'),(25,1,136,'2017-12-04 21:51:46','Creó el artículo.'),(26,1,137,'2017-12-04 21:54:37','Creó el artículo.'),(27,1,138,'2017-12-04 21:58:39','Creó el artículo.'),(28,1,138,'2017-12-04 22:04:00','Cambios: Descripción, Imágenes'),(29,1,138,'2017-12-04 22:07:20','Cambios: Imágenes'),(30,1,138,'2017-12-04 22:09:15','Cambios: Imágenes'),(31,1,138,'2017-12-04 22:11:42','Cambios: Imágenes'),(32,1,139,'2017-12-04 22:17:48','Creó el artículo.'),(33,1,140,'2017-12-05 18:52:55','Creó el artículo.'),(34,1,140,'2017-12-05 18:56:02','Cambios: Descripción, Imágenes'),(35,1,140,'2017-12-05 18:57:50','Cambios: Imágenes'),(36,1,132,'2017-12-05 19:03:39','Cambios: Descripción'),(37,1,126,'2017-12-05 19:05:17','Cambios: Descripción'),(38,1,136,'2017-12-05 19:07:15','Cambios: Descripción, Adjuntos'),(39,1,137,'2017-12-05 19:11:21','Cambios: Descripción, Adjuntos'),(40,1,139,'2017-12-05 19:15:35','Cambios: Descripción, Adjuntos'),(41,1,141,'2017-12-05 19:17:05','Creó el artículo.'),(42,1,142,'2017-12-05 19:30:36','Creó el artículo.'),(43,1,142,'2017-12-05 19:31:15','Cambios: Adjuntos'),(44,1,143,'2017-12-05 19:35:38','Creó el artículo.'),(45,1,143,'2017-12-05 19:37:42','Cambios: Imágenes'),(46,1,144,'2017-12-05 19:47:06','Creó el artículo.'),(47,1,143,'2017-12-05 19:50:34','Cambios: Título'),(48,1,145,'2017-12-05 19:51:56','Creó el artículo.'),(49,1,145,'2017-12-05 19:52:42','Cambios: Descripción'),(50,1,146,'2017-12-05 19:53:47','Creó el artículo.'),(51,1,146,'2017-12-05 19:55:00','Cambios: Imágenes'),(52,1,147,'2017-12-05 20:12:43','Creó el artículo.'),(53,1,148,'2017-12-05 20:14:33','Creó el artículo.'),(54,1,149,'2017-12-05 20:15:36','Creó el artículo.'),(55,1,150,'2017-12-05 20:17:07','Creó el artículo.'),(56,1,151,'2017-12-05 20:18:20','Creó el artículo.'),(57,1,152,'2017-12-05 20:19:12','Creó el artículo.'),(58,1,153,'2017-12-05 20:20:18','Creó el artículo.');
/*!40000 ALTER TABLE `bitacora` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `IdCategoria` int(11) NOT NULL AUTO_INCREMENT,
  `NombreCategoria` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`IdCategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` (`IdCategoria`, `NombreCategoria`) VALUES (1,'Nosotros'),(2,'Fenómenos'),(3,'Cultura de la Prevención'),(4,'Programas Especiales'),(5,'Trámites'),(6,'Servicios'),(7,'Documentos'),(8,'Noticias');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `correspondea`
--

DROP TABLE IF EXISTS `correspondea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `correspondea` (
  `IdPublicacion` int(11) NOT NULL,
  `IdSubcategoria` int(11) NOT NULL,
  PRIMARY KEY (`IdPublicacion`,`IdSubcategoria`),
  KEY `IdPublicacion` (`IdPublicacion`,`IdSubcategoria`),
  KEY `IdCategoria` (`IdSubcategoria`),
  KEY `IdPublicacion_2` (`IdPublicacion`),
  CONSTRAINT `correspondea_ibfk_1` FOREIGN KEY (`IdSubcategoria`) REFERENCES `subcategoria` (`IdSubcategoria`),
  CONSTRAINT `correspondea_ibfk_2` FOREIGN KEY (`IdPublicacion`) REFERENCES `publicacion` (`IdPublicacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `correspondea`
--

LOCK TABLES `correspondea` WRITE;
/*!40000 ALTER TABLE `correspondea` DISABLE KEYS */;
INSERT INTO `correspondea` (`IdPublicacion`, `IdSubcategoria`) VALUES (147,5),(148,6),(143,7),(144,8),(145,9),(146,10),(123,11),(136,12),(137,13),(139,14),(140,15),(141,16),(135,17),(138,18),(142,19),(126,23),(149,24),(150,25),(151,26),(152,27),(153,28),(132,29),(133,30),(126,31);
/*!40000 ALTER TABLE `correspondea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuentacon`
--

DROP TABLE IF EXISTS `cuentacon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuentacon` (
  `IdRol` int(8) NOT NULL,
  `IdPrivilegio` int(8) NOT NULL,
  KEY `IdPrivilegio` (`IdPrivilegio`),
  KEY `IdRol` (`IdRol`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuentacon`
--

LOCK TABLES `cuentacon` WRITE;
/*!40000 ALTER TABLE `cuentacon` DISABLE KEYS */;
INSERT INTO `cuentacon` (`IdRol`, `IdPrivilegio`) VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10),(1,11),(1,12),(1,13),(1,14),(1,15),(1,16),(1,17),(1,18),(1,19),(1,20),(1,21),(1,25),(1,26),(1,27),(1,28),(1,29),(3,22),(3,23),(3,24),(2,3),(2,4),(2,5),(2,6),(2,18),(2,19),(2,17),(2,21),(2,1),(2,2),(56,1),(56,2),(56,3),(56,8),(56,9),(56,10);
/*!40000 ALTER TABLE `cuentacon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departamento`
--

DROP TABLE IF EXISTS `departamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departamento` (
  `IdDepartamento` int(8) NOT NULL AUTO_INCREMENT,
  `NombreDepartamento` varchar(50) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `DescripcionDepartamento` varchar(250) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`IdDepartamento`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departamento`
--

LOCK TABLES `departamento` WRITE;
/*!40000 ALTER TABLE `departamento` DISABLE KEYS */;
INSERT INTO `departamento` (`IdDepartamento`, `NombreDepartamento`, `DescripcionDepartamento`) VALUES (1,'Dirección de Gestión de Riesgos','Se encargan del control de posibles eventos futuros.'),(2,'Dirección de Atención de Emergencias','Se encargan de brindar atención inmediata a situaciones de emergencia y accidentes.'),(3,'Dirección de Promoción y Difusión','Se encarga de divulgar las actividades y eventos del CEPCQ.'),(4,'Dirección de Capacitación ','Realizan actividades que fomentan la cultura de la protección'),(78,'Área de Vinculación ','Encargados de la vinculación entre protección civil y los ciudadanos queretanos.');
/*!40000 ALTER TABLE `departamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evento`
--

DROP TABLE IF EXISTS `evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento` (
  `IdEvento` int(11) NOT NULL AUTO_INCREMENT,
  `TituloEvento` varchar(200) CHARACTER SET utf8 NOT NULL,
  `DescripcionEvento` text CHARACTER SET utf8 NOT NULL,
  `FechaPublicacionEvento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FechaInicio` datetime NOT NULL,
  `FechaFin` datetime NOT NULL,
  `URLVideoEvento` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `RutaAdjuntosEvento` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `RutaImagenesEvento` varchar(300) CHARACTER SET utf8 DEFAULT 'arcos.jpg',
  `EventoPublicado` int(1) NOT NULL,
  PRIMARY KEY (`IdEvento`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evento`
--

LOCK TABLES `evento` WRITE;
/*!40000 ALTER TABLE `evento` DISABLE KEYS */;
INSERT INTO `evento` (`IdEvento`, `TituloEvento`, `DescripcionEvento`, `FechaPublicacionEvento`, `FechaInicio`, `FechaFin`, `URLVideoEvento`, `RutaAdjuntosEvento`, `RutaImagenesEvento`, `EventoPublicado`) VALUES (9,'Prueba evento capacitación','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu scelerisque ipsum. Nullam leo velit, imperdiet at vehicula ut, placerat quis lectus. Cras nec ex interdum, tempus erat in, aliquet arcu. Cras vitae fermentum nisl, in bibendum arcu. Pellentesque quis nisl mollis, mattis enim ut, laoreet enim. Nulla enim nunc, laoreet eu elementum vel, malesuada et ligula. Sed vitae felis in nunc ultricies tempor vel vel ex. Proin hendrerit vestibulum maximus. Integer congue velit facilisis, tempor ante quis, congue orci.\r\n\r\nOrci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras posuere ligula lectus, ut cursus mi venenatis nec. Nulla tortor mi, vestibulum in aliquet sed, semper quis lacus. Nulla interdum justo in fermentum faucibus. Donec euismod nisl ac venenatis dignissim. Nullam rhoncus eleifend nisi, sed posuere lectus aliquam non. Nam accumsan volutpat nisi, ac consectetur enim commodo non. Donec id lectus accumsan, pulvinar arcu eu, ornare eros. In suscipit nisl sit amet ligula convallis ullamcorper. Quisque sed interdum tellus. Proin luctus sem neque, et pulvinar neque malesuada sed.\r\n\r\nMorbi interdum eu lectus in tempor. Proin quis augue urna. Sed fringilla eget augue vitae aliquam. Pellentesque convallis tempus luctus. Integer vitae justo risus. Morbi lobortis sem ligula, vel imperdiet nibh blandit placerat. Morbi elementum enim orci, sagittis euismod risus scelerisque eu. Etiam sit amet neque vehicula, sollicitudin nisl at, rutrum nibh. Nunc odio tellus, hendrerit in risus eget, dignissim varius nulla. Sed id efficitur metus. Vivamus dolor magna, scelerisque in erat id, mattis dictum nunc. Vivamus hendrerit felis eu dolor convallis mollis.','2017-11-23 18:55:06','2017-11-23 12:54:00','2017-11-24 13:54:00','','','',1),(11,'Prueba evento 1','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu scelerisque ipsum. Nullam leo velit, imperdiet at vehicula ut, placerat quis lectus. Cras nec ex interdum, tempus erat in, aliquet arcu. Cras vitae fermentum nisl, in bibendum arcu. Pellentesque quis nisl mollis, mattis enim ut, laoreet enim. Nulla enim nunc, laoreet eu elementum vel, malesuada et ligula. Sed vitae felis in nunc ultricies tempor vel vel ex. Proin hendrerit vestibulum maximus. Integer congue velit facilisis, tempor ante quis, congue orci.\r\n\r\nOrci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras posuere ligula lectus, ut cursus mi venenatis nec. Nulla tortor mi, vestibulum in aliquet sed, semper quis lacus. Nulla interdum justo in fermentum faucibus. Donec euismod nisl ac venenatis dignissim. Nullam rhoncus eleifend nisi, sed posuere lectus aliquam non. Nam accumsan volutpat nisi, ac consectetur enim commodo non. Donec id lectus accumsan, pulvinar arcu eu, ornare eros. In suscipit nisl sit amet ligula convallis ullamcorper. Quisque sed interdum tellus. Proin luctus sem neque, et pulvinar neque malesuada sed.\r\n\r\nMorbi interdum eu lectus in tempor. Proin quis augue urna. Sed fringilla eget augue vitae aliquam. Pellentesque convallis tempus luctus. Integer vitae justo risus. Morbi lobortis sem ligula, vel imperdiet nibh blandit placerat. Morbi elementum enim orci, sagittis euismod risus scelerisque eu. Etiam sit amet neque vehicula, sollicitudin nisl at, rutrum nibh. Nunc odio tellus, hendrerit in risus eget, dignissim varius nulla. Sed id efficitur metus. Vivamus dolor magna, scelerisque in erat id, mattis dictum nunc. Vivamus hendrerit felis eu dolor convallis mollis.\r\n\r\nAenean posuere vulputate massa id commodo. Curabitur molestie quam et nunc egestas egestas. Duis fermentum tellus ut quam lobortis semper. Pellentesque pharetra gravida lectus ac pellentesque. Morbi rhoncus, velit id varius malesuada, leo ligula vehicula metus, et luctus quam lectus sed neque. Cras tempor aliquet sem in iaculis. Donec et nibh vel est auctor elementum nec ac tellus. Vivamus placerat scelerisque leo sit amet maximus. In auctor in nulla rutrum ullamcorper. Vestibulum gravida augue sed massa feugiat egestas. Sed in mauris venenatis, venenatis erat nec, porttitor arcu. Nullam fringilla pretium cursus. Proin ut mi non felis aliquet rutrum sed vitae lorem. Maecenas ut lorem convallis, semper enim quis, gravida mauris. Suspendisse sed pharetra ex. Proin quis placerat sap','2017-11-23 18:59:02','2017-11-23 12:58:00','2017-11-23 13:58:00','','','',1),(12,'Plática prueba','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu scelerisque ipsum. Nullam leo velit, imperdiet at vehicula ut, placerat quis lectus. Cras nec ex interdum, tempus erat in, aliquet arcu. Cras vitae fermentum nisl, in bibendum arcu. Pellentesque quis nisl mollis, mattis enim ut, laoreet enim. Nulla enim nunc, laoreet eu elementum vel, malesuada et ligula. Sed vitae felis in nunc ultricies tempor vel vel ex. Proin hendrerit vestibulum maximus. Integer congue velit facilisis, tempor ante quis, congue orci.\r\n\r\nOrci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras posuere ligula lectus, ut cursus mi venenatis nec. Nulla tortor mi, vestibulum in aliquet sed, semper quis lacus. Nulla interdum justo in fermentum faucibus. Donec euismod nisl ac venenatis dignissim. Nullam rhoncus eleifend nisi, sed posuere lectus aliquam non. Nam accumsan volutpat nisi, ac consectetur enim commodo non. Donec id lectus accumsan, pulvinar arcu eu, ornare eros. In suscipit nisl sit amet ligula convallis ullamcorper. Quisque sed interdum tellus. Proin luctus sem neque, et pulvinar neque malesuada sed.\r\n\r\nMorbi interdum eu lectus in tempor. Proin quis augue urna. Sed fringilla eget augue vitae aliquam. Pellentesque convallis tempus luctus. Integer vitae justo risus. Morbi lobortis sem ligula, vel imperdiet nibh blandit placerat. Morbi elementum enim orci, sagittis euismod risus scelerisque eu. Etiam sit amet neque vehicula, sollicitudin nisl at, rutrum nibh. Nunc odio tellus, hendrerit in risus eget, dignissim varius nulla. Sed id efficitur metus. Vivamus dolor magna, scelerisque in erat id, mattis dictum nunc. Vivamus hendrerit felis eu dolor convallis mollis.\r\n\r\nAenean posuere vulputate massa id commodo. Curabitur molestie quam et nunc egestas egestas. Duis fermentum tellus ut quam lobortis semper. Pellentesque pharetra gravida lectus ac pellentesque. Morbi rhoncus, velit id varius malesuada, leo ligula vehicula metus, et luctus quam lectus sed neque. Cras tempor aliquet sem in iaculis. Donec et nibh vel est auctor elementum nec ac tellus. Vivamus placerat scelerisque leo sit amet maximus. In auctor in nulla rutrum ullamcorper. Vestibulum gravida augue sed massa feugiat egestas. Sed in mauris venenatis, venenatis erat nec, porttitor arcu. Nullam fringilla pretium cursus. Proin ut mi non felis aliquet rutrum sed vitae lorem. Maecenas ut lorem convallis, semper enim quis, gravida mauris. Suspendisse sed pharetra ex. Proin quis placerat sap','2017-11-23 18:59:51','2017-11-23 15:59:00','2017-11-28 17:59:00','','','',1),(14,'Prueba de evento','lalsdasdjalkdjalkdjalkd','2017-12-02 17:46:18','2017-12-03 22:30:00','2017-12-04 22:20:00','','','1511220369-plaza-de-armas.jpg',1),(22,'Prueba de evento','Esta es una prueba','2017-12-01 19:24:19','2017-12-01 05:51:00','2017-12-02 06:51:00','https://www.youtube.com/','grafos.pdf','',0),(23,'Prueba evento 2','Esto es una prueba','2017-12-01 19:23:07','2017-12-01 13:40:00','2017-12-02 08:57:00','','','',1),(25,'Borrador evento','Prueba de borrador','2017-12-04 20:03:55','2017-12-04 18:02:00','2017-12-05 07:02:00','','','',0);
/*!40000 ALTER TABLE `evento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imagenesarticulo`
--

DROP TABLE IF EXISTS `imagenesarticulo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imagenesarticulo` (
  `idImagenesArticulos` int(11) NOT NULL AUTO_INCREMENT,
  `rutaImagenes` varchar(300) NOT NULL,
  `idPublicacion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idImagenesArticulos`),
  KEY `idPublicacion` (`idPublicacion`),
  CONSTRAINT `imagenesarticulo_ibfk_1` FOREIGN KEY (`idPublicacion`) REFERENCES `publicacion` (`IdPublicacion`)
) ENGINE=InnoDB AUTO_INCREMENT=206 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imagenesarticulo`
--

LOCK TABLES `imagenesarticulo` WRITE;
/*!40000 ALTER TABLE `imagenesarticulo` DISABLE KEYS */;
INSERT INTO `imagenesarticulo` (`idImagenesArticulos`, `rutaImagenes`, `idPublicacion`) VALUES (162,'1512149002-Programa Especial de Protección Civil para la Temporada de Frío y Heladas.JPG',123),(173,'1512422825-IMG-20170110 (18).jpg',132),(174,'1512423080-tempora de lluvias.jpg',133),(175,'1512423291-Respuesta y vinculación por sismos.jpg',NULL),(176,'1512423355-Respuesta y vinculación por sismos.jpg',NULL),(177,'1512423741-capacitación.jpg',135),(179,'1512423841-Respuesta y vinculación por sismos.jpg',126),(180,'1512424176-análisis de riesgo.jpg',NULL),(181,'1512424296-aforos.jpg',136),(182,'1512424453-eventos masivos.jpg',137),(187,'1512425477-análisis de riesgo.jpg',138),(188,'1512425855-9b52e8ce-9a9f-43fb-83eb-cd3172d6b9a2.jpg',139),(191,'1512500263-Registro de terceros acreditadosnueva.png',140),(192,'1512502074-Marco Juridico.jpg',NULL),(193,'1512502236-Marco Juridico.jpg',142),(195,'1512502643-123 026.jpg',143),(196,'1512502643-niños.jpg',143),(197,'1512502889-c0a51570-639a-44e5-910c-c32709c7ae58.jpg',NULL),(198,'1512502971-c0a51570-639a-44e5-910c-c32709c7ae58.jpg',NULL),(199,'1512503162-cultura jovenesn.jpg',NULL),(200,'1512503186-c0a51570-639a-44e5-910c-c32709c7ae58.jpg',144),(201,'1512503186-cultura jovenesn.jpg',144),(202,'1512503506-adultos.JPG',145),(204,'1512503691-culrura de p. familia.jpg',146),(205,'1512503691-cultura de prevención en la familia.jpg',146);
/*!40000 ALTER TABLE `imagenesarticulo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imagenesevento`
--

DROP TABLE IF EXISTS `imagenesevento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imagenesevento` (
  `IdImagenesEventos` int(11) NOT NULL AUTO_INCREMENT,
  `RutaImagenes` varchar(300) CHARACTER SET utf8 NOT NULL,
  `IdEvento` int(11) DEFAULT NULL,
  PRIMARY KEY (`IdImagenesEventos`),
  KEY `IdPublicacion` (`IdEvento`),
  CONSTRAINT `imagenesevento_ibfk_1` FOREIGN KEY (`IdEvento`) REFERENCES `evento` (`IdEvento`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imagenesevento`
--

LOCK TABLES `imagenesevento` WRITE;
/*!40000 ALTER TABLE `imagenesevento` DISABLE KEYS */;
INSERT INTO `imagenesevento` (`IdImagenesEventos`, `RutaImagenes`, `IdEvento`) VALUES (23,'1512158899-1511220369-plaza-de-armas.jpg',14);
/*!40000 ALTER TABLE `imagenesevento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perteneceaevento`
--

DROP TABLE IF EXISTS `perteneceaevento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perteneceaevento` (
  `IdEvento` int(11) NOT NULL,
  `IdTipoEvento` int(11) NOT NULL,
  KEY `IdEvento` (`IdEvento`),
  KEY `IdTipoEvento` (`IdTipoEvento`),
  CONSTRAINT `perteneceaevento_ibfk_1` FOREIGN KEY (`IdEvento`) REFERENCES `evento` (`IdEvento`) ON DELETE CASCADE,
  CONSTRAINT `perteneceaevento_ibfk_2` FOREIGN KEY (`IdTipoEvento`) REFERENCES `tipoevento` (`IdTipoEvento`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perteneceaevento`
--

LOCK TABLES `perteneceaevento` WRITE;
/*!40000 ALTER TABLE `perteneceaevento` DISABLE KEYS */;
INSERT INTO `perteneceaevento` (`IdEvento`, `IdTipoEvento`) VALUES (9,1),(11,1),(11,4),(11,5),(12,5),(12,2),(22,1),(22,5),(23,1),(23,4),(23,5),(14,1),(25,4),(25,5);
/*!40000 ALTER TABLE `perteneceaevento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perteneceapublicacion`
--

DROP TABLE IF EXISTS `perteneceapublicacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perteneceapublicacion` (
  `IdTipoPublicacion` int(11) NOT NULL,
  `IdPublicacion` int(11) NOT NULL,
  PRIMARY KEY (`IdTipoPublicacion`,`IdPublicacion`),
  KEY `IdTipoPublicacion` (`IdTipoPublicacion`),
  KEY `IdPublicacion` (`IdPublicacion`),
  KEY `IdTipoPublicacion_2` (`IdTipoPublicacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perteneceapublicacion`
--

LOCK TABLES `perteneceapublicacion` WRITE;
/*!40000 ALTER TABLE `perteneceapublicacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `perteneceapublicacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `privilegio`
--

DROP TABLE IF EXISTS `privilegio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `privilegio` (
  `IdPrivilegio` int(8) NOT NULL AUTO_INCREMENT,
  `NombrePrivilegio` varchar(50) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `DescripcionPrivilegio` varchar(250) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`IdPrivilegio`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `privilegio`
--

LOCK TABLES `privilegio` WRITE;
/*!40000 ALTER TABLE `privilegio` DISABLE KEYS */;
INSERT INTO `privilegio` (`IdPrivilegio`, `NombrePrivilegio`, `DescripcionPrivilegio`) VALUES (1,'Registrar Articulo','Registro de artículos nuevos.'),(2,'Modificar Articulo','Modificación de artículos existentes.'),(3,'Eliminar Articulo','Eliminación de artículos existentes.'),(4,'Registrar Evento','Registro de eventos nuevos.'),(5,'Modificar Evento','Modificación de eventos existentes.'),(6,'Eliminar Evento','Eliminación de eventos existentes.'),(7,'Ver Usuarios','Visualición de usuarios existente en el sistema.'),(8,'Registrar Rol','Registro de rol con privilegios para usuarios.'),(9,'Modificar Rol','Modificación de nombre y privilegios de roles.'),(10,'Eliminar Rol','Eliminación de roles.'),(11,'Asignar Rol','Asignación de rol a un usuario.'),(12,'Modificar Privilegios','Modificación de privilegios de roles.'),(13,'Otorgar Privilegios','Otorgar privilegios a un rol.'),(14,'Registrar Empleado','Registro de usuarios con el rol de empleado en el sistema.'),(15,'Modificar Empleado','Modificar empleados en el sistema.\r\n'),(16,'Eliminar Empleado','Eliminar empleados del sistema.'),(17,'Registrar Ayudante','Registrar de usuarios con el rol de ayudante en el sistema.'),(18,'Modificar Ayudante','Modificar ayudantes de empleados en el sistema.'),(19,'Eliminar Ayudante','Eliminar ayudantes de empleados del sistema.'),(20,'Consultar Bitacora Empleado','Consultar Bitácora de modificaciones que han realizado los empleados.'),(21,'Consultar Bitacora Articulo','Consultar Bitácora de modificaciones que han sido realizadas a un artículo.'),(22,'Crear borrador.','Crear artículos nuevos como borrador.'),(23,'Cambiar publicación a borrador.','Cambiar un artículo publicado a borrador.'),(25,'Registrar Departamento','Registro de departamentos nuevos.'),(26,'Modificar Departamento','Modificación de departamentos existentes.'),(27,'Eliminar Departamento','Eliminación de departamentos existentes.'),(28,'Modificar Página Principal','Modificar textos e imagenes de la pagina principal'),(29,'Editar descripción subcategorias','Este privilegio permite al usuario editar la descripción de las subcategorias.');
/*!40000 ALTER TABLE `privilegio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publicacion`
--

DROP TABLE IF EXISTS `publicacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publicacion` (
  `IdPublicacion` int(11) NOT NULL AUTO_INCREMENT,
  `FechaPublicacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `TituloPublicacion` varchar(200) COLLATE utf8_bin NOT NULL,
  `DescripcionPublicacion` text COLLATE utf8_bin NOT NULL,
  `RutaImagenesPublicacion` varchar(300) COLLATE utf8_bin NOT NULL DEFAULT 'arcos.jpg',
  `RutaAdjuntosPublicacion` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `URLVideoPublicacion` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `FijoPublicacion` tinyint(1) NOT NULL,
  `FechaFinFijoPublicacion` datetime DEFAULT NULL,
  `PublicarDespues` tinyint(1) NOT NULL,
  `FechaPublicacionProgramada` datetime DEFAULT NULL,
  `Borrador` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`IdPublicacion`)
) ENGINE=InnoDB AUTO_INCREMENT=154 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publicacion`
--

LOCK TABLES `publicacion` WRITE;
/*!40000 ALTER TABLE `publicacion` DISABLE KEYS */;
INSERT INTO `publicacion` (`IdPublicacion`, `FechaPublicacion`, `TituloPublicacion`, `DescripcionPublicacion`, `RutaImagenesPublicacion`, `RutaAdjuntosPublicacion`, `URLVideoPublicacion`, `FijoPublicacion`, `FechaFinFijoPublicacion`, `PublicarDespues`, `FechaPublicacionProgramada`, `Borrador`) VALUES (123,'2017-12-01 17:23:23','Programa Especial de Protección Civil para la Temporada de Frío y Heladas','En este programa se plasman las acciones relativas a la prevención y actuación en caso de fríos extremos en el Estado de Querétaro; asimismo se promueve la coordinación con las entidades de gobierno que interactúan para evitar riesgos graves a la población.\r\n\r\nLa Secretaría de Salud del Gobierno del Estado de Querétaro, es la entidad que asume preponderancia en este plan con medidas preventivas, tales como las campañas de vacunación, de difusión de “qué hacer en caso de“ y otras acciones, con la finalidad de evitar sobre todo disminuir los índices de contagios masivos de las enfermedades propias de la temporada. \r\n\r\nOtro de los actores muy importantes en este Programa es el Sistema Estatal DIF, el cual con su campaña “Cobijemos con una Sonrisa 2017” ayuda a promover el acopio y distribución de miles de cobijas entre la población más vulnerable del Estado.','Programa Especial de Protección Civil para la Temporada de Frío y Heladas.JPG','','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',0),(126,'2017-12-01 17:25:54','Plan de Preparación, Respuesta y Vinculación por Sismo.','Como parte de las acciones de preparación antes, durante y después de un sismo se ha creado el “Plan Sismo”  el cual consta de actividades específicas de “qué hacer” en caso de un evento geológico de este tipo.\r\n\r\nEste Plan está alineado al Plan Nacional de Sismo, e involucra a todas las dependencias de todos los sectores (Público, Privado, Social y Académico) con la finalidad de promover acciones tendientes a minimizar el riesgo de daños provocados por sismos en Querétaro. Asimismo contiene la participación de las fuerzas del Estado en la atención de fenómenos de éste tipo en otras entidades del país.','Respuesta y vinculación por sismos.jpg','','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',0),(132,'2017-12-04 21:27:09','Programa Especial de Protección Civil para Incendios Forestales','Este programa consta de una serie de acciones para desarrollar por parte de las dependencias relacionadas al tipo de riesgo, las cuales están coordinadas por protección civil y lideradas por la Comisión Nacional Forestal (CONAFOR), Entre las acciones más recurrentes que se tienen es la implementación de brigadas forestales, las cuales están formadas por personal de las instituciones que tienen bajo el ámbito de su responsabilidad la atención de la población en situaciones de riesgo.\r\n\r\nLos alcances del programa se relacionan a las actividades previas a la generación de un incendio forestal, una vez que éste se encuentra evolucionando y cuando es extinguido. Esto con la idea de hacer que la población afectada pueda volver a la normalidad.\r\n\r\nEn el programa se mencionan actividades, procedimiento y relaciones funcionales entre las dependencias; tendientes a la optimización de los recursos humanos y materiales y la pronta actuación en caso de un Incendio de origen natural o provocado por el hombre. \r\n\r\nEl fin último del Programa es evitar o minimizar los riesgos a la población.\r\n','IMG-20170110 (18).jpg','','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',0),(133,'2017-12-04 21:31:27','Programa Especial de Protección Civil para la Temporada de Lluvias','\r\nEste programa está formado por las diferentes actividades que se deben desarrollar como respuesta y previo a los eventos desafortunados que se presentan ante la presencia de lluvias torrenciales e inundaciones en alguna o algunas de las localidades del Estado de Querétaro.\r\n\r\nAsimismo contiene las acciones de recuperación que se deben implementar una vez que éstas han sucedido, y es necesario regresar a un estado similar al que tenía la población antes de estas lluvias o inundaciones.\r\n\r\nEl insumo principal de éste programa es la coordinación entre las diferentes dependencias que como parte de sus atribuciones y responsabilidades tienen la obligación de brindar el apoyo a la población vulnerable a estos fenómenos.\r\n','tempora de lluvias.jpg','','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',0),(135,'2017-12-04 21:42:22','Capacitación','\r\nLa capacitación en materia de Protección Civil es el proceso educativo a corto plazo para aprender y no olvidar habilidades para salvaguardar tu vida y la de quienes te rodean.\r\n\r\nTener conocimientos en materia de autoprotección puede marcar la diferencia entre salvar una vida o perderla. Es importante que los ciudadanos seamos conscientes de la necesidad de que nuestro hogar, escuela, centro de trabajo o negocio y cualquier lugar al que asistimos cuente con las medidas de Protección Civil.\r\n\r\nLos cursos básicos que ofrecemos en materia de Protección Civil son:\r\n\r\n•	Primeros Auxilios\r\n•	Prevención y combate de incendios\r\n•	Diseños de escenarios y simulacros de evacuación \r\n\r\n¿Qué otros cursos te podemos brindar?\r\n\r\n•	Cursos básicos de Protección Civil\r\n•	Cursos especializados en atención de emergencias\r\n•	Cursos de gestión de riesgos \r\n•	Psicología de emergencias\r\n','capacitación.jpg','','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',0),(136,'2017-12-04 21:51:46','Aforos','Un aforo es la cantidad de personas que pueden ingresar, permanecer y ser evacuadas de manera segura de un establecimiento público o privado durante el desarrollo de una contingencia.\r\n','aforos.jpg','Requisitos Aforo.pdf','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',0),(137,'2017-12-04 21:54:37','Eventos Masivos','La actividad con aglomeraciones de personas pueden representar un factor de riesgo si no cuenta con las medidas de prevención y seguridad necesarias, si pretende realizar un evento en un inmueble a cargo de Gobierno del Estado  es importante que alguno de nuestros analistas le orienten sobre  los procedimientos y requisitos necesarios para obtener el visto bueno de nuestra dependencia.\r\n','eventos masivos.jpg','Aforo.pdf, Formato Único.pdf, Lista de procedimientos para la Autorización de Eventos Socio-Organizativos 2017.pdf, Plan de Acción Eventos.pdf','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',0),(138,'2017-12-04 21:58:39','Análisis de Riesgo','Es el diagnostico de posibles riesgos a lo que está expuesto un inmueble, establecimiento o zona. Nos permite generar acciones que reduzcan daños a la población, sus bienes y entorno.\r\n','análisis de riesgo.jpg','','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',0),(139,'2017-12-04 22:17:48','Licencia de alcoholes','Si se encuentra realizando un trámite ante la Dirección de Gobierno del Estado, para la obtener su licencia de ventas de bebidas alcohólicas en envase abierto, deberá acudir con nosotros y obtener una opinión técnica de medidas de seguridad, para lo cual deberá integrar una serie de documentos y aprobar una revisión física del establecimiento, con esto pretendemos promover que los  propietarios, trabajadores y clientes estén en un lugar seguro.\r\n','9b52e8ce-9a9f-43fb-83eb-cd3172d6b9a2.jpg','Firma de enterado de Requisitos para Alcoholes.pdf, Formato Único.pdf','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',0),(140,'2017-12-05 18:52:55','Registro de terceros acreditados:','Un capacitador enseña lo necesario en materia de prevención y autocuidado. Un consultor, por su parte, nos orienta o elabora esquemas de prevención planes de acción prevención, auxilio y recuperación emitiendo la carta de corresponsabilidad.\r\n\r\nLas personas o dependencias públicas pueden registrarse como capacitador o consultor en materia de Protección Civil, mientras tengan la trayectoria y actualizaciones necesarias para impartir cursos o realizar programas internos de Protección Civil. Así como también es de suma importancia que estén registrados porque te aseguras de que es alguien que sabe y que te enseñará cosas que te ayudarán a cuidarte mejor a ti y a los tuyos.\r\n','Registro de terceros acreditadosnueva.png','','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',0),(141,'2017-12-05 19:17:05','Registro de grupos voluntarios:','\r\nSon las personas que pueden contratar, por ejemplo, cuando organizas un evento masivo, para que cuiden a ti y a tus asistentes. Son expertos en Protección Civil y en medidas de emergencia. Están agrupados en empresas o en organizaciones sin fines de lucro, y deben estar registrados en CEPCQ.\r\n¿Para qué sirve el registro de grupos voluntarios?\r\nEl registro sirve para que puedan ser contratados por empresarios organizadores de eventos masivos y sólo son válidos los que se encuentren registrados.\r\n','','','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',1),(142,'2017-12-05 19:30:36','Marco Jurídico','Son las Leyes y Reglamentos vigentes que fundamentan el actuar de la Coordinación Estatal de Protección  Civil del Estado de Querétaro para el cumplimiento de sus deberes.\r\n','Marco Juridico.jpg','Ley de Protección Civil del Estado de Querétaro.pdf, Ley General de Protección Civil.pdf','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',0),(143,'2017-12-05 19:35:38','Niños','La CEPCQ fomenta y promueve la cultura de prevención y autocuidado a los niños, mediante métodos didácticos para fomentar la cultura de Protección Civil y niños resilientes ante una  emergencia.','123 026.jpg, niños.jpg','','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',0),(144,'2017-12-05 19:47:06','Jóvenes','La CEPCQ promueve la cultura de prevención y autocuidado en los jóvenes mediante actividades didácticas y de sensibilización, así como en sus redes sociales.','c0a51570-639a-44e5-910c-c32709c7ae58.jpg, cultura jovenesn.jpg','','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',0),(145,'2017-12-05 19:51:56','Adultos mayores','La CEPCQ fomenta y promueve la cultura de prevención a los adultos mayores apoyada de actividades de sensibilización.','adultos.JPG','','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',0),(146,'2017-12-05 19:53:47','Familiar','La CEPCQ promueve la cultura de prevención a las familias a través del Plan Familiar, en donde los integrantes deben identificar los fenómenos a los que están expuestos y  así desarrolla acciones de autoprotección, para cada uno de los integrantes, así como estudiando riesgos internos y externos para saber qué hacer en una emergencia.','culrura de p. familia.jpg, cultura de prevención en la familia.jpg','','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',0),(147,'2017-12-05 20:12:43','Fenómeno Antrópico','Se refiere  a los procesos, efectos o materiales resultados por la actividad humana a diferencia de otros que tienen causas naturales.','','','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',0),(148,'2017-12-05 20:14:33','Fenómeno Astronómico','Eventos, procesos o propiedades a los que están sometidos los objetos del espacio exterior incluidos estrellas, planetas, cometas y meteoros. Algunos de estos fenómenos interactúan con la tierra, ocasionándole situaciones que generan perturbaciones que pueden ser destructivas, tanto en la atmósfera como en la superficie terrestre.','','','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',0),(149,'2017-12-05 20:15:36','Fenómeno Geológico','Fenómenos  producto de la actividad de la corteza terrestre que se encuentra en constante transformación.','','','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',0),(150,'2017-12-05 20:17:07','Fenómeno Hidrometeorológico','Agente perturbador que se genera por la acción de los agentes atmosféricos tales como: humedad, vientos,precipitación y temperatura, provocando ciclones tropicales, lluvias extremas, inundación pluviales, fluviales costeras y lacustres, granizo y polvo electricidad.','','','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',0),(151,'2017-12-05 20:18:20','Fenómeno Sanitario-Ecológico','Agente perturbador que se genera por la acción patógena de agentes biológicos que afectan a la población, a los animales y a las cosechas, causando su muerte o la alteración de su salud, son producto en parte del crecimiento poblacional e industrial.','','','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',0),(152,'2017-12-05 20:19:12','Fenómeno Químico-Tecnológico','Agente perturbador que se genera por la acción violenta de diferentes sustancias derivadas de su interacción molecular o nuclear.','','','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',0),(153,'2017-12-05 20:20:18','Fenómeno Socio-Organizativo','Se genera con motivo de errores humanos o por acciones premeditadas, que se dan en el marco de grandes concentraciones o movimientos masivos de población.\r\n\r\n(Centro Nacional de Prevención de Desastres)\r\n','','','',0,'1969-12-31 18:00:00',0,'1969-12-31 18:00:00',0);
/*!40000 ALTER TABLE `publicacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rol` (
  `IdRol` int(8) NOT NULL AUTO_INCREMENT,
  `NombreRol` varchar(50) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `DescripcionRol` varchar(250) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`IdRol`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` (`IdRol`, `NombreRol`, `DescripcionRol`) VALUES (1,'Administrador','Administrador del sitio. Puede hacer uso de todos los privilegios.'),(2,'Empleado','Empleados de Protección Civil que pueden aportar al sitio, cuentan con ciertos privilegios.'),(3,'Ayudante','Ayudantes de empleados de Protección Civil, necesitan aprobación de su superior para que se realice una publicación suya.');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subcategoria`
--

DROP TABLE IF EXISTS `subcategoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subcategoria` (
  `IdSubcategoria` int(11) NOT NULL AUTO_INCREMENT,
  `IdCategoria` int(11) NOT NULL,
  `NombreSubcategoria` varchar(50) COLLATE utf8_bin NOT NULL,
  `Descripcion` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `RutaImagen` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `RutaImagenUsuario` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `CorreoSubcategoria` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `cc` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`IdSubcategoria`,`IdCategoria`),
  KEY `IdCategoria` (`IdCategoria`),
  CONSTRAINT `subcategoria_ibfk_1` FOREIGN KEY (`IdCategoria`) REFERENCES `categoria` (`IdCategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subcategoria`
--

LOCK TABLES `subcategoria` WRITE;
/*!40000 ALTER TABLE `subcategoria` DISABLE KEYS */;
INSERT INTO `subcategoria` (`IdSubcategoria`, `IdCategoria`, `NombreSubcategoria`, `Descripcion`, `RutaImagen`, `RutaImagenUsuario`, `CorreoSubcategoria`, `cc`) VALUES (1,1,'Conócenos',NULL,NULL,NULL,NULL,NULL),(2,1,'Misión','Planear y coordinar la política interna del Estado, mediante la realización oportuna de acciones que coadyuven a fortalecer la libertad y seguridad de la población, con objeto de preservar y mantener la gobernabilidad, el orden y la paz social en la entidad, impulsar el régimen democrático, generar una cultura de participación con responsabilidad y promover el desarrollo integral de la sociedad.','','','',''),(3,1,'Ubicación',NULL,NULL,NULL,NULL,NULL),(4,2,'Naturales','Descripción fenómenos naturales.',NULL,NULL,'',''),(5,2,'Antrópicos','Descripción fenómenos antrópicos.',NULL,NULL,'',''),(6,2,'Astronómicos','Descripción fenómenos astronómicos.',NULL,NULL,'',''),(7,3,'Niños',NULL,NULL,NULL,NULL,NULL),(8,3,'Jóvenes',NULL,NULL,NULL,NULL,NULL),(9,3,'Adultos mayores',NULL,NULL,NULL,NULL,NULL),(10,3,'Familiar',NULL,NULL,NULL,NULL,NULL),(11,4,'Temporada de frío y heladas',NULL,NULL,NULL,NULL,NULL),(12,5,'Aforos','',NULL,NULL,'vhernandezm@queretaro.gob.mx','jmendozam@queretaro.gob.mx,lnicteha@queretaro.gob.mx'),(13,5,'Eventos masivos',NULL,NULL,NULL,'cmendoza@queretaro.gob.mx','farroy@queretaro.gob.mx,lnicteha@queretaro.gob.mx'),(14,5,'Licencia de alcoholes',NULL,NULL,NULL,'cmendoza@queretaro.gob.mx','farroy@queretaro.gob.mx,lnicteha@queretaro.gob.mx'),(15,5,'Registro de terceros acreditados',NULL,NULL,NULL,'mdenis@@queretaro.gob.mx','lnicteha@queretaro.gob.mx'),(16,5,'Registro de grupos voluntarios',NULL,NULL,NULL,'hgarciah@queretaro.gob.mx','lnicteha@queretaro.gob.mx'),(17,6,'Capacitación',NULL,NULL,NULL,'mlanderost@queretaro.gob.mx','aoropeza@queretaro.gob.mx,lnicteha@queretaro.gob.mx'),(18,6,'Análisis de riesgos',NULL,NULL,NULL,'sgarcias@queretaro.gob.mx ','jmendozam@queretaro.gob.mx,lnicteha@queretaro.gob.mx'),(19,7,'Marco jurídico',NULL,NULL,NULL,NULL,NULL),(20,7,'Guías',NULL,NULL,NULL,NULL,NULL),(21,7,'Manuales',NULL,NULL,NULL,NULL,NULL),(22,7,'Directorio',NULL,NULL,NULL,NULL,NULL),(23,8,'Noticias',NULL,NULL,NULL,NULL,NULL),(24,2,'Geológicos','Descripción fenómenos geológicos.',NULL,NULL,'',''),(25,2,'Hidrometeorológicos','Descripción fenómenos hidrometeorológicos.',NULL,NULL,'',''),(26,2,'Sanitarios - Ecologicos','Descripción fenómenos Sanitarios - Ecologicos.',NULL,NULL,'',''),(27,2,'Químicos - Tecnológicos','Descripción fenómenos Químicos - Tecnológicos.',NULL,NULL,'',''),(28,2,'Socio - Organizativos','Descripción fenómenos Socio - Organizativos.',NULL,NULL,'',''),(29,4,'Incendios forestales',NULL,NULL,NULL,NULL,NULL),(30,4,'Temporada de lluvias',NULL,NULL,NULL,NULL,NULL),(31,4,'Respuesta y vinculación por sismos',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `subcategoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `textoinicio`
--

DROP TABLE IF EXISTS `textoinicio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `textoinicio` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Texto` varchar(4000) NOT NULL,
  `RutaImagen` varchar(300) NOT NULL,
  `RutaImagenUsuario` varchar(300) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `textoinicio`
--

LOCK TABLES `textoinicio` WRITE;
/*!40000 ALTER TABLE `textoinicio` DISABLE KEYS */;
INSERT INTO `textoinicio` (`Id`, `Texto`, `RutaImagen`, `RutaImagenUsuario`) VALUES (1,'La protección civil es un sistema que se haya instalado en cada país y que tiene la misión de proporcionarle protección y asistencia para los ciudadanos que residen en él, y a quienes se hayan de paso, en caso de sucederse cualquier tipo de desastre natural o accidente. También estará a cargo de la protección de los bienes y del medio ambiente. Poniéndolo en términos más sencillos, se encargará de la gestión de los servicios de emergencias que hay en una nación. Formalmente, la protección civil nació a instancias del Convenio de Ginebra, el 12 de agosto del año 1949, siendo su principal misión la protección de las víctimas de los enfrentamientos armados internacionales.\r\n\r\nLa Protección Civil es el Conjunto de disposiciones, medidas y acciones destinadas a la prevención, auxilio y recuperación de la población ante la eventualidad de un desastre.\r\n\r\nEs importante que la sociedad, los grupos voluntarios, las autoridades y todos los que conforman el Sistema Municipal de Protección Civil se sumen a la cultura del autocuidado. Recordemos que Protección Civil Somos Todos.','1512162031-queesproteccioncivil.jpg','queesproteccioncivil.jpg');
/*!40000 ALTER TABLE `textoinicio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipodepublicacion`
--

DROP TABLE IF EXISTS `tipodepublicacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipodepublicacion` (
  `IdTipoPublicacion` int(11) NOT NULL,
  `NombreTipoPublicacion` varchar(50) COLLATE utf8_bin NOT NULL,
  `DescripcionTipoPublicacion` varchar(250) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`IdTipoPublicacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipodepublicacion`
--

LOCK TABLES `tipodepublicacion` WRITE;
/*!40000 ALTER TABLE `tipodepublicacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipodepublicacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoevento`
--

DROP TABLE IF EXISTS `tipoevento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoevento` (
  `IdTipoEvento` int(11) NOT NULL,
  `NombreTipoEvento` varchar(50) CHARACTER SET utf8 NOT NULL,
  `DescripcionTipoEvento` varchar(250) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`IdTipoEvento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoevento`
--

LOCK TABLES `tipoevento` WRITE;
/*!40000 ALTER TABLE `tipoevento` DISABLE KEYS */;
INSERT INTO `tipoevento` (`IdTipoEvento`, `NombreTipoEvento`, `DescripcionTipoEvento`) VALUES (1,'Capacitación','Evento en el que se dará una capacitación relacionada a Protección Civil.'),(2,'Plática','Evento en el que un ponente importante dentro del área de Protección Civil dará una charla informativa.'),(3,'Taller','Evento en el que los asistentes podrán formar parte de un taller que les ayudará a estar preparados ante cualquier situación.'),(4,'Curso','Evento que consiste en una serie de actividades para preparar a los asistentes sobre algún tema en específico.'),(5,'Junta','Evento en el que se convoca a los miembros de Protección Civil para hablar sobre temas importantes.');
/*!40000 ALTER TABLE `tipoevento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trabajaen`
--

DROP TABLE IF EXISTS `trabajaen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trabajaen` (
  `IdUsuario` int(11) NOT NULL,
  `IdDepartamento` int(8) NOT NULL,
  `Fecha` date DEFAULT NULL,
  KEY `fk_TrabajaEnUsuario` (`IdUsuario`),
  CONSTRAINT `fk_TrabajaEnUsuario` FOREIGN KEY (`IdUsuario`) REFERENCES `usuario` (`IdUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trabajaen`
--

LOCK TABLES `trabajaen` WRITE;
/*!40000 ALTER TABLE `trabajaen` DISABLE KEYS */;
INSERT INTO `trabajaen` (`IdUsuario`, `IdDepartamento`, `Fecha`) VALUES (5,3,NULL),(1,3,NULL),(6,4,NULL),(7,1,NULL),(8,78,NULL),(9,1,NULL),(10,2,NULL),(11,1,NULL),(13,0,NULL),(15,0,NULL),(17,2,NULL),(18,4,NULL),(19,78,NULL);
/*!40000 ALTER TABLE `trabajaen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tramitamos`
--

DROP TABLE IF EXISTS `tramitamos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tramitamos` (
  `IdTramitamos` int(11) NOT NULL AUTO_INCREMENT,
  `RutaImagenTramitamos` varchar(300) NOT NULL,
  PRIMARY KEY (`IdTramitamos`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tramitamos`
--

LOCK TABLES `tramitamos` WRITE;
/*!40000 ALTER TABLE `tramitamos` DISABLE KEYS */;
INSERT INTO `tramitamos` (`IdTramitamos`, `RutaImagenTramitamos`) VALUES (11,'1512148799-Sismos.png'),(12,'1512148799-Químicos.png'),(13,'1512148799-Inundaciones.png'),(14,'1512148799-Heladas.png'),(15,'1512148799-EventosMasivos.png'),(16,'1512148799-AnálisisDeRiesgos.png');
/*!40000 ALTER TABLE `tramitamos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `IdUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `NombreUsuario` varchar(50) NOT NULL,
  `IdEmpleado` int(8) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  `ApellidoP` varchar(50) NOT NULL,
  `ApellidoM` varchar(50) DEFAULT NULL,
  `Puesto` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`IdUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`IdUsuario`, `NombreUsuario`, `IdEmpleado`, `Nombre`, `ApellidoP`, `ApellidoM`, `Puesto`) VALUES (1,'lnicteha@queretaro.gob.mx',57926,'Lucía Nicté-Há','Koh','Lira','Directora de Promoción y Difusión '),(5,'mdenis@queretaro.gob.mx',19020,'María de Lourdes','Denis','Estrada','Promotor de Programas de Protección Civil '),(6,'mlanderost@queretaro.gob.mx',189265,'Lucero','Landeros','Tejeida','Administradora del Centro de Capacitación'),(7,'vhernandezm@queretaro.gob.mx',55696,'Veronica','Hernández','Moran','Auxiliar de Información'),(8,'hgarciah@queretaro.gob.mx',59816,'Humberto','García','Herrera','Jefe de Área'),(9,'lnavarrete@queretarogob.mx',154831,'Luis Alfredo','Navarrete','Ramos','Instructor en Atención de Emergencias '),(10,'cmendoza@queretaro.gob.mx',78644,'Carlos','Mendoza','Martínez','Jefe del Área de Atención de Riesgos y Eventos '),(11,'diegobtzeqr@gmail.com',6542,'Diego','Betanzos','Esquer','Servicio Social'),(13,'karyrs1506@gmail.com',1349,'Karina','Reyes','2','Servicio Social'),(15,'diego_be_8@hotmail.com',9875,'asd','b','c','Jefazo'),(17,'andresraygadas@gmail.com',9584,'Andres','Raygadas','Dominguez','Servicio Social'),(18,'A01700666@itesm.mx',9875,'Karina','Reyes','Santiago','Servicio Social'),(19,'b_mg96@hotmail.com',9874,'Bruno','Maglioni','Granada','Servicio Social');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'protecci_cepcq'
--

--
-- Dumping routines for database 'protecci_cepcq'
--
/*!50003 DROP PROCEDURE IF EXISTS `crearUsuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`protecci`@`localhost` PROCEDURE `crearUsuario`(IN `usu` VARCHAR(255), IN `idE` INT(8), IN `nom` VARCHAR(50), IN `apeP` VARCHAR(50), IN `apeM` VARCHAR(50), IN `pu` VARCHAR(50), IN `idR` INT(8), IN `idDep` INT(8))
    NO SQL
BEGIN
	DECLARE NombreUsuario VARCHAR(50) DEFAULT 'nombre usuario';
	DECLARE IdEmpleado INT(8) DEFAULT 0;
	DECLARE Nombre VARCHAR(50) DEFAULT 'nombre';
	DECLARE ApellidoP VARCHAR(50) DEFAULT 'apellido paterno';
	DECLARE ApellidoM VARCHAR(50) DEFAULT 'apellido materno';
	DECLARE Puesto VARCHAR(50) DEFAULT 'puesto';
	DECLARE IdRol INT(8) DEFAULT 2;
	DECLARE IdDepartamento INT(8) DEFAULT 78;
    DECLARE IdUsuario INT(11) DEFAULT 0;
	SET NombreUsuario = usu;
	SET nombre = nom;
	SET IdEmpleado = idE;
	SET ApellidoP = apeP;
	SET ApellidoM = apeM;
	SET Puesto = pu;
	SET IdRol = idR;
	SET IdDepartamento = idDep;
    SET IdUsuario = (SELECT `AUTO_INCREMENT`
					FROM  INFORMATION_SCHEMA.TABLES
					WHERE TABLE_SCHEMA = 'protecci_cepcq'
					AND   TABLE_NAME   = 'usuario');
    START TRANSACTION;
	INSERT INTO usuario (NombreUsuario, IdEmpleado, Nombre, ApellidoP, ApellidoM, Puesto) VALUES(NombreUsuario, IdEmpleado, Nombre, ApellidoP, ApellidoM, Puesto);
	INSERT INTO asignadoa (IdUsuario, IdRol) VALUES(IdUsuario, IdRol);
	INSERT INTO trabajaen (IdUsuario, IdDepartamento) VALUES(IdUsuario, IdDepartamento);
	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `editarUsuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`protecci`@`localhost` PROCEDURE `editarUsuario`(IN `usu` VARCHAR(255), IN `idE` INT(8), IN `nom` VARCHAR(50), IN `apeP` VARCHAR(50), IN `apeM` VARCHAR(50), IN `pu` VARCHAR(50), IN `idR` INT(8), IN `idDep` INT(8), IN `idUsu` INT(11))
    NO SQL
BEGIN
	DECLARE nomU VARCHAR(255) DEFAULT 'nombre usuario';
	DECLARE idEm INT(8) DEFAULT 0;
	DECLARE nomb VARCHAR(50) DEFAULT 'nombre';
	DECLARE apellP VARCHAR(50) DEFAULT 'apellido paterno';
	DECLARE apellM VARCHAR(50) DEFAULT 'apellido materno';
	DECLARE pues VARCHAR(50) DEFAULT 'puesto';
	DECLARE idRl INT(8) DEFAULT 0;
	DECLARE idDepar INT(8) DEFAULT 0;
    DECLARE idU INT(11) DEFAULT 0;
    SET idU = idUsu;
	SET nomU = usu;
	SET nomb = nom;
	SET idEm = idE;
	SET apellP = apeP;
	SET apellM = apeM;
	SET pues = pu;
	SET idRl = idR;
	SET idDepar = idDep;
	START TRANSACTION;
    UPDATE usuario SET NombreUsuario = nomU, IdEmpleado = idEm, Nombre = nomb, ApellidoP = apellP, ApellidoM = apellM, Puesto = pues WHERE IdUsuario = idU;
	UPDATE asignadoa SET IdRol = idRl WHERE IdUsuario = idU;
	UPDATE trabajaen SET IdDepartamento = idDepar WHERE IdUsuario = idU;
	COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08  0:07:53
