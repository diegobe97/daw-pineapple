<?php

if (isset($_GET["id"])) { 
	$id = $_GET["id"];
	session_start();
	require_once "modelo-articulos.php";
	revisarPublicarDespues();
    revisarFijos();
	$publicacion = getPublicacion(conectar(), $id);

	if (isset($publicacion)){
		if ($publicacion["Borrador"] == 1){
			if (isset($_SESSION["privilegios"])) {
				include("../_header.html");
				include("_articulo.html");
		    	include("../_user-menu.html");
		    	include("../_footer.html");
		    }else{
				include("../error.html");
			}
		}
		else {
			include("../_header.html");
			include("_articulo.html");
		    if (isset($_SESSION["privilegios"])) {
		    	include("../_user-menu.html");
		    }
		    include("../_footer.html");
	    }
	}else{
		include("../error.html");
	}
	
} else {
	include("../error.html");
}

?>