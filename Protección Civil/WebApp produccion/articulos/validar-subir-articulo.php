<?php
session_start();
require_once "../modelo.php";
unset($_SESSION['imagenes']);

// Valores por default
// URLVideoPublicacion
$response['errores'] = false;

// Validar imagenes si se adjuntan
if(!(array_sum($_FILES['imagenes']['error']) > 0))
{
    foreach($_FILES['imagenes']['name'] as $key=>$name){
        // Rutas para guardar
        $target_dir     = "../images/";
        $target_file    = clean_input($target_dir . $_FILES['imagenes']['name'][$key]);
        $source_file    = clean_input($_FILES['imagenes']['tmp_name'][$key]);
        $filename       = clean_input($_FILES['imagenes']['name'][$key]);
        
        // Propiedades de las fotos
        $size           = $_FILES['imagenes']['size'][$key];
        $type           = clean_input(pathinfo($target_file,PATHINFO_EXTENSION));
        
        $uploadOk = false;
        
        // Checar que el archivo realmente sea una imagen
        if(! @is_array(getimagesize($_FILES["imagenes"]["tmp_name"][$key]) || getimagesize($_FILES["imagenes"]["tmp_name"][$key]))){
            //echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = true;
            
            // Checar que el tamaño sea valido
            if ($size > 5000000) {
                //echo "Sorry, your file is too large.";
                $uploadOk = false;
                $errores['tamaño'] = 'Execede el tamaño máximo';
            }
            // Checar que tenga un formato valido
            if($type != "jpg" && $type != "png" && $type != "jpeg"
            && $type != "gif" ) {
                //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = false;
                $errores['formato'] = 'La imágen debe tener uno de los siguientes formatos: jpg, jpeg, png, gif';
            }
        }
        // El archivo no es una imagen
        else {
            $uploadOk = false;
            $errores['tipo'] = 'Imposible subir este archivo como imagen';
        }
        
        $response['imagenes'][$key]['nombre'] = $name;

        // Intentar subir la imagen
        if ($uploadOk == false) {
            $response['imagenes'][$key]['upload'] = false;
            $response['imagenes'][$key]['mensaje'] = $errores;
        } else {
            $target_file = time() . '-' . $_FILES["imagenes"]["name"][$key];
            if (move_uploaded_file($_FILES["imagenes"]["tmp_name"][$key], '../images/' . $target_file)) {
                $response['imagenes'][$key]['upload'] = true;
                // Subir la imagen a la base de datos
                $db = conectar();
                $query = 'INSERT INTO imagenesarticulo (`rutaImagenes`) VALUES (?)';

                // Preparing the statement
                if (!($statement = $db->prepare($query))) {
                    $response['imagenes'][$key]['upload'] = false;
                    $response['imagenes'][$key]['mensaje']['general'] = 'El archivo ' . $name . ' no puede subirse en este momento';
                    $response['errores'] = true; 
                    break;
                }
                // Binding statement params
                if (!$statement->bind_param("s", $target_file)) {
                    $response['imagenes'][$key]['upload'] = false;
                    $response['imagenes'][$key]['mensaje']['general'] = 'El archivo ' . $name . ' no puede subirse en este momento';
                    $response['errores'] = true; 
                    break;
                }
                // Executing the statement
                if (!$statement->execute()) {
                    $response['imagenes'][$key]['upload'] = false;
                    $response['imagenes'][$key]['mensaje']['general'] = 'El archivo ' . $name . ' no puede subirse en este momento';
                    $response['errores'] = true; 
                    break;
                }

                $IdImagenesArticulos = $db->insert_id;
                desconectar($db);

                // Actualizar el arreglo con el nuevo id y para poder crear la relacion articulo-imagen en la BD
                $_SESSION['imagenes'][$key]['id_imagen'] = $IdImagenesArticulos;
            } else {
                $response['imagenes'][$key]['upload'] = false;
                $response['imagenes'][$key]['mensaje']['general'] = 'El archivo ' . $name . ' no puede subirse en este momento';
            }
        }
        // Validar si la foto tuvo errores
        if ($response['imagenes'][$key]['upload'] === false){
           $response['errores'] = true; 
        }
    }
}

// Validar archivos si se adjuntan
if(!(array_sum($_FILES['archivos']['error']) > 0))
{
    foreach($_FILES['archivos']['name'] as $key=>$name){
        // Rutas para guardar
        $target_dir     = "../files/";
        $target_file    = clean_input($target_dir . $_FILES['archivos']['name'][$key]);
        $source_file    = clean_input($_FILES['archivos']['tmp_name'][$key]);
        $filename       = clean_input($_FILES['archivos']['name'][$key]);
        
        // Propiedades de los archivos
        $size           = $_FILES['archivos']['size'][$key];
        $type           = clean_input(pathinfo($target_file,PATHINFO_EXTENSION));
        
        $uploadOk = true;
        
        // Checar que el tamaño sea valido
       if ($size > 5000000) {
            //echo "Sorry, your file is too large.";
            $uploadOk = false;
            $errores['tamaño'] = 'Execede el tamaño máximo';
        }
        // Checar que tenga un formato valido
        if($type != "pdf" && $type != "jpg" && $type != "png" && $type != "jpeg"
        && $type != "gif" ) {
            //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = false;
            $errores['formato'] = 'El archivo debe tener uno de los siguientes formatos: pdf, jpg, jpeg, png, gif';
        }    
        
        $response['archivos'][$key]['nombre'] = $name;
        
        // Intentar subir el archivo
        if ($uploadOk == false) {
            $response['archivos'][$key]['upload'] = false;
            $response['archivos'][$key]['mensaje'] = $errores;
        } else {
            $target_file = time() . '-' . $_FILES["archivos"]["name"][$key];
            if (move_uploaded_file($_FILES["archivos"]["tmp_name"][$key], '../files/' . $target_file)) {
                $response['archivos'][$key]['upload'] = true;
                // Subir la imagen a la base de datos
                $db = conectar();
                $query = 'INSERT INTO archivosarticulo (`rutaArchivos`) VALUES (?)';

                // Preparing the statement
                if (!($statement = $db->prepare($query))) {
                    $response['archivos'][$key]['upload'] = false;
                    $response['archivos'][$key]['mensaje']['general'] = 'El archivo ' . $name . ' no puede subirse en este momento';
                    $response['errores'] = true; 
                    break;
                }
                // Binding statement params
                if (!$statement->bind_param("s", $target_file)) {
                    $response['archivos'][$key]['upload'] = false;
                    $response['archivos'][$key]['mensaje']['general'] = 'El archivo ' . $name . ' no puede subirse en este momento';
                    $response['errores'] = true; 
                    break;
                }
                // Executing the statement
                if (!$statement->execute()) {
                    $response['archivos'][$key]['upload'] = false;
                    $response['archivos'][$key]['mensaje']['general'] = 'El archivo ' . $name . ' no puede subirse en este momento';
                    $response['errores'] = true; 
                    break;
                }

                
                $IdArchivosArticulos = $db->insert_id;
                desconectar($db);

                // Actualizar el arreglo con el nuevo id y para poder crear la relacion articulo-archivo en la BD
                $_SESSION['archivos'][$key]['id_archivo'] = $IdArchivosArticulos;
            } else {
                $response['archivos'][$key]['upload'] = false;
                $response['archivos'][$key]['mensaje']['general'] = 'El archivo ' . $name . ' no puede subirse en este momento.';
            }
        }
        // Validar si la foto tuvo errores
        if ($response['archivos'][$key]['upload'] === false){
           $response['errores'] = true; 
        }
    }
}

// Validar URL si se adjunta
if(isset($_POST["URLVideoPublicacion"]) && $_POST["URLVideoPublicacion"] != ''){
    $url = $_POST["URLVideoPublicacion"];
    // Remove all illegal characters from a url
    $url = filter_var($url, FILTER_SANITIZE_URL);

    // Validate url
    if (!(filter_var($url, FILTER_VALIDATE_URL))) {
        $response['url']['mensaje'] = 'Ingresa un URL valido';
        $response['errores'] = true; 
    }
}

// Validar fijar artículo 
if(isset($_POST["fijo"])){
    $fijo = $_POST["fijo"];
    if ($fijo == "1"){
        if(isset($_POST["fecha-fin-fijo"])){
            if ($_POST["fecha-fin-fijo"] == ''){
                $response['fecha-fin-fijo']['mensaje'] = 'Ingresa una fecha o da click en "no fijar artículo"';
                $response['errores'] = true; 
            }
        }
        if(isset($_POST["hora-fin-fijo"])){
            if ($_POST["hora-fin-fijo"] == ''){
                $response['hora-fin-fijo']['mensaje'] = 'Ingresa una hora o da click en "no fijar artículo"';
                $response['errores'] = true;
            }
        }
    }
}

// Validar publicar despues
if(isset($_POST["publicar-despues"])){
    $publicar_despues = $_POST["publicar-despues"];
    if ($publicar_despues == "1"){
        if(isset($_POST["fecha-publicar"])){
            if ($_POST["fecha-publicar"] == ''){
            $response['fecha-publicar']['mensaje'] = 'Ingresa una fecha o da click en "publicar ahora"';
            $response['errores'] = true; 
            }
        }
        
        if(isset($_POST["hora-publicar"])){
            if ($_POST["hora-publicar"] == ''){
            $response['hora-publicar']['mensaje'] = 'Ingresa una hora o da click en "publicar ahora"';
            $response['errores'] = true; 
            }
        }
    }
}
$response["POST"]=$_POST;
// Respuesta final
echo json_encode($response);
       

function clean_input($s){
    $s = preg_replace('/\s+/', '', $s);
    $s = strtolower($s);
    $s = htmlspecialchars($s);
    return $s;
}

?>