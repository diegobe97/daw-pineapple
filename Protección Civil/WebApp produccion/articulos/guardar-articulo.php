<?php
session_start();
require_once "modelo-articulos.php";

if (isset($_POST["IdPublicacion"])) {

    $Actual  = getPublicacion(conectar(), $_POST["IdPublicacion"]);

    $Descripcion = "Cambios: ";

    if(isset($_POST["TituloPublicacion"]))
    {
        if($Actual["TituloPublicacion"] != $_POST["TituloPublicacion"]){
            $Descripcion .= "Título, ";
        }
    }

    if(isset($_POST["DescripcionPublicacion"])){
        if($Actual["DescripcionPublicacion"] != $_POST["DescripcionPublicacion"]){
            $Descripcion .= "Descripción, ";
        }
    }

    if(isset($_POST["URLVideoPublicacion"])){
        if($Actual["URLVideoPublicacion"] != $_POST["URLVideoPublicacion"]){
            $Descripcion .= "URL Video, ";
        }
    }

    if(isset($_POST["RutaAdjuntosPublicacion"])){
        if($Actual["RutaAdjuntosPublicacion"] != $_POST["RutaAdjuntosPublicacion"]){
            $Descripcion .= "Adjuntos, ";
        }
    }

    if(isset($_POST["RutaImagenesPublicacion"])){
        if($Actual["RutaImagenesPublicacion"] != $_POST["RutaImagenesPublicacion"]){
            $Descripcion .= "Imágenes, ";
        }
    }

    if(isset($_POST["FijoPublicacion"])){
        if($Actual["FijoPublicacion"] != $_POST["FijoPublicacion"]){
            $Descripcion .= "Fijar, ";
        }
    }

    if(isset($_POST["PublicacionPublicada"])){
        if($Actual["PublicacionPublicada"] != $_POST["PublicacionPublicada"]){
            $Descripcion .= "Publicar, ";
        }
    }

    editarArticulo(htmlspecialchars($_POST["IdPublicacion"]), htmlspecialchars($_POST["TituloPublicacion"]), htmlspecialchars($_POST["DescripcionPublicacion"]), htmlspecialchars($_POST["RutaImagenesPublicacion"]), htmlspecialchars($_POST["RutaAdjuntosPublicacion"]), htmlspecialchars($_POST["URLVideoPublicacion"]), htmlspecialchars($_POST["fijo"]), htmlspecialchars($_POST["fecha-fin-fijo_php"]), htmlspecialchars($_POST["hora-fin-fijo"]), htmlspecialchars($_POST["publicar-despues"]), htmlspecialchars($_POST["fecha-publicar_php"]), htmlspecialchars($_POST["hora-publicar"]), $_POST["Subcategorias"], $_POST["action"]);

    if($Descripcion != "Cambios: "){
        $Descripcion = substr($Descripcion, 0, -2);

        bitacoraArticulo($_POST["IdPublicacion"], $_SESSION["idUsuario"], $Descripcion);
    }

    $_SESSION["mensaje"] = '"' . $_POST["TituloPublicacion"] . '" se actualizó correctamente.';
} else {
    $last_id = guardarArticulo(htmlspecialchars($_POST["TituloPublicacion"]), htmlspecialchars($_POST["DescripcionPublicacion"]), htmlspecialchars($_POST["RutaImagenesPublicacion"]), htmlspecialchars($_POST["RutaAdjuntosPublicacion"]), htmlspecialchars($_POST["URLVideoPublicacion"]), htmlspecialchars($_POST["fijo"]), htmlspecialchars($_POST["fecha-fin-fijo_php"]), htmlspecialchars($_POST["hora-fin-fijo"]), htmlspecialchars($_POST["publicar-despues"]), htmlspecialchars($_POST["fecha-publicar_php"]), htmlspecialchars($_POST["hora-publicar"]), $_POST["Subcategorias"], $_POST["action"]);

    $Descripcion = "Creó el artículo.";

    bitacoraArticulo($last_id, $_SESSION["idUsuario"], $Descripcion);

    $_SESSION["mensaje"] = '"' . $_POST["TituloPublicacion"] . '" se registró correctamente.';
}

header('location:ver-articulos.php');
?>