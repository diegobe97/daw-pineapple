<?php
    session_start();
    if(isset($_SESSION["privilegios"])){
        require_once("modelo-articulos.php");
        $idarticulo=eliminarAdjunto($_GET["id"]);
        $_SESSION["mensaje"] = 'Adjunto eliminado correctamente.';
        header('location:editar-articulo.php?id='.$idarticulo);
    }else{
        include('../error.html');
    }
?>