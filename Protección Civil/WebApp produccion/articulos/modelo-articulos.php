<?php
require_once "../modelo.php";
require_once "../modelo-subir-imagen.php";
date_default_timezone_set('America/Mexico_City');

function getPublicacion($db, $registroId)
{
    //Specification of the SQL query
    $query = 'SELECT * FROM publicacion p, correspondea c, subcategoria s, categoria cat WHERE p.IdPublicacion=' . $registroId . '
        AND p.IdPublicacion=c.IdPublicacion AND s.IdSubcategoria=c.IdSubcategoria AND cat.IdCategoria=s.IdCategoria';
    // Query execution; returns identifier of the result group
    $registros   = $db->query($query);
    $publicacion = mysqli_fetch_array($registros, MYSQLI_ASSOC);
    return $publicacion;
}

function getArticulos($col, $IdSubcategoria = -1, $columna=-1)
{
    if(getNombreSubcategoria($IdSubcategoria) == 'Astronómicos'){
        return;
    }
    revisarPublicarDespues();
    revisarFijos();
    $db = conectar(); 
    if ($IdSubcategoria > -1) {
        if (isset($_SESSION["privilegios"])) {
            // Cargar artículos por columa seleccionada
            if ($columna == 'Publicados'){
                $query = 'SELECT * FROM publicacion p, correspondea c WHERE p.IdPublicacion = c.IdPublicacion AND p.Borrador = 0 AND p.PublicarDespues = 0 AND c.IdSubcategoria =' . $IdSubcategoria . ' ORDER BY p.FijoPublicacion DESC, p.FechaPublicacion DESC';
            }else{
                $query = 'SELECT * FROM publicacion p, correspondea c WHERE p.IdPublicacion = c.IdPublicacion AND '. $columna .' = 1 AND c.IdSubcategoria =' . $IdSubcategoria . ' ORDER BY p.FijoPublicacion DESC, p.FechaPublicacion DESC';
            }
        } else {
            // No puede ver los borradores ni los articulos que están programados para publicarse despues
            $query = 'SELECT * FROM publicacion p, correspondea c WHERE p.IdPublicacion = c.IdPublicacion AND p.Borrador=0 AND p.PublicarDespues=0 AND c.IdSubcategoria =' . $IdSubcategoria . ' ORDER BY p.FijoPublicacion DESC, p.FechaPublicacion DESC';
        }
    } else {
        if ($columna == 'Publicados'){
            $query = 'SELECT * FROM publicacion WHERE Borrador = 0 AND PublicarDespues = 0 ORDER BY FechaPublicacion DESC';
        }else{
            $query = 'SELECT * FROM publicacion WHERE '. $columna .' = 1 ORDER BY FechaPublicacion DESC';
        }  
    }

    // Query execution; returns identifier of the result group
    $registros = mysqli_query($db,$query);    

    $col  = (int) ($col);
    $cont = 0;
    
    $articulos = "";

    if ( mysqli_num_rows($registros) == 0){
        $articulos .= "";
        $subcategoria=getSubcategoria($IdSubcategoria);
        if (!isset($_SESSION["privilegios"])) {
            if ($subcategoria["Descripcion"] == ''){
                $articulos .= "<p class='center-align'>Estamos trabajando en esta sección para brindarte un mejor servicio.</p>";
            }
        }
    }

    if (isset($_SESSION["privilegios"])) {
        if ($columna != -1){
            if ($columna == 'Borrador'){
                $articulos .= '<h5 class="subtitulo-articulos">Borradores de artículos</h5>';    
            }
            else if ($columna == 'PublicarDespues'){
                $articulos .= '<h5 class="subtitulo-articulos">Artículos programados para publicar después</h5>';
            }
            else if ($columna == 'Publicados'){
                $articulos .= '<h5 class="subtitulo-articulos">Artículos publicados</h5>';
            }
        }
    }

    if ( mysqli_num_rows($registros) == 0){
        $articulos .= "";
        if (isset($_SESSION["privilegios"])) {
            $articulos .= "<p>Por el momento no hay publicaciones en este apartado.</p>";
        }
        return $articulos;
    }

    $articulos .= '<div class="row">';

    // cycle to explode every line of the results
    while ($publicacion = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
        $fecha  = date('F d, Y', strtotime($publicacion["FechaPublicacion"]));
        $imagen = "../images/default.png";
        $fecha  = date('F d, Y', strtotime($publicacion["FechaPublicacion"]));
        $titulo = trunc($publicacion["TituloPublicacion"], 12);
        $cuerpo = trunc($publicacion["DescripcionPublicacion"], 270);
        $id     = $publicacion["IdPublicacion"];
        $fijo   = $publicacion["FijoPublicacion"];

        // Traduce la fecha
        $trans = array("January" => "Enero",
                "February" => "Febrero",
                "March" => "Marzo",
                "April" => "Abril",
                "May" => "Mayo",
                "June" => "Junio",
                "July" => "Julio",
                "August" => "Agosto",
                "September" => "Septiembre",
                "October" => "Octubre",
                "November" => "Noviembre",
                "December" => "Diciembre");
        $fecha = strtr($fecha, $trans);

        // Cargar la primera imagen del artículo
        if(isset($publicacion['RutaImagenesPublicacion']) && $publicacion['RutaImagenesPublicacion'] != ''){
            $query          = 'SELECT * FROM imagenesarticulo WHERE idPublicacion=' . $id . ' ORDER BY idImagenesArticulos ASC LIMIT 1';
            $imagenes       = $db->query($query);
            $imagenarticulo = mysqli_fetch_array($imagenes, MYSQLI_BOTH);
            $imagen         = "../images/" . $imagenarticulo["rutaImagenes"];
        }

        $articulos .= '<div class="col s12 m6 l' . 12 / ($col % 12) . '">';
        if($publicacion["Borrador"] == 1){
          $articulos .=            '<div class="card small hoverable">
                                      <a href="_articulo.php?id='.$id.'">';

        if ($imagen == '../images/default.png'){
            $articulos .= "<div class='card-image default'>";
        }else{
            $articulos .= "<div class='card-image'>";
        }  

        $articulos .=                  '
                                            <img class="responsive-img" src="' . $imagen . '" style="opacity:.3;">
                                            <span class="card-title black-text" style="text-shadow:.5px .5px #000000;">(Borrador)</span>
                                          </div>
                                      </a>
                                      <div class="card-content">
                                          <span class="card-title grey-text text-darken-4">
                                              <a href="_articulo.php?id='.$id.'">';
                                              if ($fijo == 1){
                                                $articulos .= '<i class="material-icons tiny tooltipped" data-position="top" data-delay="50" data-tooltip="Artículo fijo" style="margin-right: 5px;">gps_fixed</i>';
                                              }
        $articulos .=                       $titulo . '
                                              </a>
                                              <i class="material-icons activator right">more_vert</i>
                                          </span>
                                          <span class="card-title grey-text text-darken-4"><small>' . $fecha . '</small>';
        }else{
            $articulos .=            '<div class="card small hoverable">
                                      <a href="_articulo.php?id='.$id.'">';

           if ($imagen == '../images/default.png'){
                $articulos .= "<div class='card-image default'>";
            }else{
                $articulos .= "<div class='card-image'>";
            }  

            $articulos .=                      '<img class="responsive-img" src="' . $imagen . '">
                                          </div>
                                      </a>
                                      <div class="card-content">
                                          <span class="card-title grey-text text-darken-4">
                                            <a href="_articulo.php?id='.$id.'">';
                                            if ($fijo == 1){
                                                $articulos .= '<i class="material-icons tiny tooltipped" data-position="top" data-delay="50" data-tooltip="Artículo fijo" style="margin-right: 5px;">gps_fixed</i>';
                                            }
            $articulos .=                       $titulo . '
                                              </a>
                                              <i class="material-icons activator right">more_vert</i>
                                          </span>

                                          <span class="card-title grey-text text-darken-4"><small>' . $fecha . '</small>';
        }


        if(isset($_SESSION["privilegios"])){
            if(binarySearch($_SESSION["privilegios"], 3)){
                $articulos .= '<a href="#!" class="right botonBorrar btn-floating red" data-ruta="eliminar-articulo.php" data-tooltip="Eliminar articulo"
                                data-id="' . $id . '" data-tipo="articulo">
                                    <i class="material-icons">delete_forever</i>
                                </a>';
            }
        }

        if (isset($_SESSION["privilegios"])) {
            if (binarySearch($_SESSION["privilegios"], 2)) {
                $articulos .= '<a href="editar-articulo.php?id=' . $id . '" data-tooltip="Editar articulo" class="right  btn-floating blue"><i class="material-icons">edit</i></a>';
            }
        }

        $articulos .= '             </span>
                                    </div>
                                    <div class="card-reveal">
                                      <span class="card-title grey-text text-darken-4">' . $titulo . '<i class="material-icons right">close</i></span>
                                      <p>' . $cuerpo . '</p>
                                    </div>
                                </div>';
        $articulos .= '                 </div>';
        $cont++;
    }

    $articulos .= '</div>';

    mysqli_free_result($registros);
    desconectar($db);
    return $articulos;
}

function getCategoria($id)
{
    $db = conectar();

    $query = 'SELECT * FROM categoria WHERE IdCategoria=' . $id;

    $registros = $db->query($query);
    $categoria = mysqli_fetch_array($registros, MYSQLI_ASSOC);

    mysqli_free_result($registros);

    desconectar($db);
    return $categoria['NombreCategoria'];
}

function getNombreSubcategoria($id)
{
    $db = conectar();

    $query = 'SELECT NombreSubcategoria FROM subcategoria WHERE IdSubcategoria=' . $id;

    $registros    = $db->query($query);
    $subcategoria = mysqli_fetch_array($registros, MYSQLI_ASSOC);

    mysqli_free_result($registros);

    desconectar($db);
    return $subcategoria['NombreSubcategoria'];
}

function getSubcategoria($id)
{
    $db = conectar();

    $query = 'SELECT * FROM subcategoria s, categoria c WHERE s.IdSubcategoria =' . $id . ' AND s.IdCategoria = c.IdCategoria';

    $registros=mysqli_query($db,$query);

    if ( mysqli_num_rows($registros) > 0  ) {
        $subcategoria = mysqli_fetch_array($registros, MYSQLI_ASSOC);
    }else{
        $subcategoria = "error";
    }

    mysqli_free_result($registros);

    desconectar($db);
    return $subcategoria;
}

function getIdCategoria($IdSubcategoria)
{
    $db    = conectar();
    $query = 'SELECT * FROM subcategoria WHERE IdSubcategoria=' . $IdSubcategoria;
    $registros    = $db->query($query);
    $subcategoria = mysqli_fetch_array($registros, MYSQLI_ASSOC);
    mysqli_free_result($registros);
    desconectar($db);
    return $subcategoria['IdCategoria'];
}

function getIdSubcategoria($NombreSubcategoria)
{
    $db    = conectar();
    $query = 'SELECT * FROM subcategoria WHERE NombreSubcategoria="' . $NombreSubcategoria . '"';
    $registros    = $db->query($query);
    $subcategoria = mysqli_fetch_array($registros, MYSQLI_ASSOC);
    mysqli_free_result($registros);

    desconectar($db);
    return $subcategoria['IdSubcategoria'];
}

function getCategorias()
{
    $db = conectar();
    $query = 'SELECT * FROM categoria ORDER BY IdCategoria';
    $registros = $db->query($query);
    desconectar($db);
    return $registros;
}

function getSubcategorias($id = -1, $selected = -1)
{
    $db = conectar();
    $query = 'SELECT * FROM subcategoria WHERE IdCategoria=' . $id . ' ORDER BY IdSubcategoria';
    $registros = $db->query($query);
    desconectar($db);
    return $registros;
}

function getCategoriasSubcategorias($IdPublicacion=-1){
    $categorias = getCategorias();

    
    if ($IdPublicacion>0){
        $db = conectar();
        //Specification of the SQL query
        $query="SELECT * FROM correspondea WHERE IdPublicacion =" . $IdPublicacion;
        // Query execution; returns identifier of the result group
        $subcategoriasCorrespondeA = $db->query($query);
        $arrayCorrespondeA = [];
        while($fila = mysqli_fetch_array($subcategoriasCorrespondeA, MYSQLI_BOTH)) {
                array_push($arrayCorrespondeA, (int)$fila["IdSubcategoria"]);
        }
        desconectar($db);
    }
    

    $menu = '<div class="row">';
    $j = 0;
    
    while ($categoria = mysqli_fetch_array($categorias, MYSQLI_BOTH)) {
        $subcategorias = getSubcategorias($categoria["IdCategoria"]);
        if($j % 10 == 0){
            $menu .= '<div class="col s12 m6 l3">';
        }

        $menu .= '  <p>
                      <input type="checkbox" name="'.$categoria["NombreCategoria"].'" id="'.$categoria["NombreCategoria"].'" disabled="disabled" />
                      <label class="black-text" for="'.$categoria["NombreCategoria"].'">'.$categoria["NombreCategoria"].'</label>
                    </p>';
        if( $j % 10 == 9){
            $menu .= '</div>';
        }

        $j++;

        while ($subcategoria = mysqli_fetch_array($subcategorias, MYSQLI_BOTH)) {
            // 
            if($j % 10 == 0){
                $menu .= '<div class="col s12 m6 l3">';
            }
            $id = (int)$subcategoria["IdSubcategoria"];
            
            if (isset($arrayCorrespondeA) && in_array($id, $arrayCorrespondeA) ) {
                $menu .= '  <p>
                                <input type="checkbox" class="checkboxsubcategoria" name="Subcategorias[]" id="subcategoria'.$subcategoria["IdSubcategoria"].'" checked="checked" value="'.$subcategoria["IdSubcategoria"].'"/>
                                <label class="black-text" for="subcategoria'.$subcategoria["IdSubcategoria"].'" class="truncate">'.$subcategoria["NombreSubcategoria"].'</label>
                            </p>';
            }
            
           else{
                $menu .= '  <p>
                                <input type="checkbox" class="checkboxsubcategoria" name="Subcategorias[]" id="subcategoria'.$subcategoria["IdSubcategoria"].'" value="'.$subcategoria["IdSubcategoria"].'"/>
                                <label class="black-text" for="subcategoria'.$subcategoria["IdSubcategoria"].'" class="truncate">'.$subcategoria["NombreSubcategoria"].'</label>
                            </p>';
           }

            //Si ya hay 9 <p>, crea una nueva columna
            if( $j % 10 == 9){
                $menu .= '</div>';
            }
            $j++;
        }
    }
    $menu .= '</div>';
    $menu .= '</div>';

    return $menu;
}

function guardarArticulo($TituloPublicacion, $DescripcionPublicacion, $RutaImagenesPublicacion, $RutaAdjuntosPublicacion,
    $URLVideoPublicacion, $FijoPublicacion, $FechaFinFijoPublicacion, $HoraFinFijoPublicacion,
    $PublicarDespues, $FechaPublicacionProgramada, $HoraPublicacionProgramada, $ArraySubcategorias, $Accion) {
    if ($Accion == "borrador") {
        $Borrador = 1;
    } else if ($Accion == "publicar") {
        $Borrador = 0;
    }

    $db                         = conectar();
    $FechaFinFijoPublicacion    = date('Y-m-d', strtotime("$FechaFinFijoPublicacion"));
    $FechaPublicacionProgramada = date('Y-m-d', strtotime("$FechaPublicacionProgramada"));
    $HoraFinFijoPublicacion     = date("H:i:s", strtotime("$HoraFinFijoPublicacion"));
    $HoraPublicacionProgramada  = date("H:i:s", strtotime("$HoraPublicacionProgramada"));

    $FechaFinFijoPublicacion    = "$FechaFinFijoPublicacion $HoraFinFijoPublicacion";
    $FechaPublicacionProgramada = "$FechaPublicacionProgramada $HoraPublicacionProgramada";

    $query = 'INSERT INTO publicacion (`TituloPublicacion`,`DescripcionPublicacion`, `RutaImagenesPublicacion`, `RutaAdjuntosPublicacion`, `URLVideoPublicacion`, `FijoPublicacion`, `FechaFinFijoPublicacion`, `PublicarDespues`,
        `FechaPublicacionProgramada`, `Borrador`) VALUES (?,?,?,?,?,?,?,?,?,?)';

    // Preparing the statement
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params
    if (!$statement->bind_param("ssssssssss", $TituloPublicacion, $DescripcionPublicacion, $RutaImagenesPublicacion, $RutaAdjuntosPublicacion,
        $URLVideoPublicacion, $FijoPublicacion, $FechaFinFijoPublicacion, $PublicarDespues, $FechaPublicacionProgramada, $Borrador)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
    }
    // Executing the statement
    if (!$statement->execute()) {
        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
    }

    // Guardar el id del ultimo articulo creado
    $IdPublicacion = $db->insert_id;

    // Insertar a qué subcategoría pertenece la publicación
    guardarCorrespondeA($db, $IdPublicacion, $ArraySubcategorias);

    // Si se adjuntaron imagenes, crear la relacion
    if (isset($_SESSION['imagenes']) && $RutaImagenesPublicacion != '') {
        foreach ($_SESSION['imagenes'] as $key => $id_imagen) {
            guardarImagenesArticulos($db, $IdPublicacion, $_SESSION['imagenes'][$key]['id_imagen']);
        }
        unset($_SESSION['imagenes']);
    }
    // Si se adjuntaron archivos, crear la relacion
    if (isset($_SESSION['archivos']) && $RutaAdjuntosPublicacion != '') {
        foreach ($_SESSION['archivos'] as $key => $id_archivo) {
            guardarArchivosArticulos($db, $IdPublicacion, $_SESSION['archivos'][$key]['id_archivo']);
        }
        unset($_SESSION['archivos']);
    }
    
    $query = 'SELECT * FROM publicacion ORDER BY IdPublicacion DESC LIMIT 1';
    
    // Query execution; returns identifier of the result group
    $registros = $db->query($query);
        
    $nuevaPublicacion = mysqli_fetch_array($registros, MYSQLI_BOTH);

    desconectar($db);
    
    return $nuevaPublicacion["IdPublicacion"];
}

function guardarImagenesArticulos($db, $IdPublicacion, $IdImagenesArticulos)
{
    // insert command specification
    $query = 'UPDATE imagenesarticulo SET idPublicacion=? WHERE IdImagenesArticulos =' . $IdImagenesArticulos;
    // Preparing the statement
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params
    if (!$statement->bind_param("s", $IdPublicacion)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
    }
    // update execution
    if (!($statement->execute())) {
        die("Update failed: (" . $statement->errno . ") " . $statement->error);
    }
}

function guardarArchivosArticulos($db, $IdPublicacion, $IdArchivosArticulos)
{
    // insert command specification
    $query = 'UPDATE archivosarticulo SET idPublicacion=? WHERE IdArchivosArticulos =' . $IdArchivosArticulos;
    // Preparing the statement
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params
    if (!$statement->bind_param("s", $IdPublicacion)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
    }
    // update execution
    if (!($statement->execute())) {
        die("Update failed: (" . $statement->errno . ") " . $statement->error);
    }
}

function editarArticulo($IdPublicacion, $TituloPublicacion, $DescripcionPublicacion, $RutaImagenesPublicacion, $RutaAdjuntosPublicacion,
    $URLVideoPublicacion, $FijoPublicacion, $FechaFinFijoPublicacion, $HoraFinFijoPublicacion,
    $PublicarDespues, $FechaPublicacionProgramada, $HoraPublicacionProgramada, $ArraySubcategorias, $Accion) {

    if ($Accion == "borrador") {
        $Borrador = 1;
    } else if ($Accion == "publicar") {
        $Borrador = 0;
    }

    $db                         = conectar();
    $FechaFinFijoPublicacion    = date('Y-m-d', strtotime("$FechaFinFijoPublicacion"));
    $FechaPublicacionProgramada = date('Y-m-d', strtotime("$FechaPublicacionProgramada"));
    $HoraFinFijoPublicacion     = date("H:i:s", strtotime("$HoraFinFijoPublicacion"));
    $HoraPublicacionProgramada  = date("H:i:s", strtotime("$HoraPublicacionProgramada"));

    $FechaFinFijoPublicacion    = "$FechaFinFijoPublicacion $HoraFinFijoPublicacion";
    $FechaPublicacionProgramada = "$FechaPublicacionProgramada $HoraPublicacionProgramada";

    // insert command specification
    $query = 'UPDATE publicacion SET TituloPublicacion=?, DescripcionPublicacion=?, RutaImagenesPublicacion=?, RutaAdjuntosPublicacion=?,
            URLVideoPublicacion=?, FijoPublicacion=?,  FechaFinFijoPublicacion=?, PublicarDespues=?, FechaPublicacionProgramada=?, Borrador=? WHERE IdPublicacion =' . $IdPublicacion;
    // Preparing the statement
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params
    if (!$statement->bind_param("ssssssssss", $TituloPublicacion, $DescripcionPublicacion, $RutaImagenesPublicacion, $RutaAdjuntosPublicacion,
        $URLVideoPublicacion, $FijoPublicacion, $FechaFinFijoPublicacion, $PublicarDespues, $FechaPublicacionProgramada, $Borrador)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
    }
    // update execution
    if (!($statement->execute())) {
        die("Update failed: (" . $statement->errno . ") " . $statement->error);
    }

    // Si la ruta de imagenes esta vacia o se adjuntaron nuevas, borrar las relaciones existentes
    if (isset($_SESSION['imagenes']) || $RutaImagenesPublicacion == '') {
        eliminarImagenesArticulo($db, $IdPublicacion);
            // Una vez borrada las relaciones, si se adjuntaron nuevas, crear la nueva relacion
        if (isset($_SESSION['imagenes'])){
            foreach ($_SESSION['imagenes'] as $key => $id_imagen) {
                guardarImagenesArticulos($db, $IdPublicacion, $_SESSION['imagenes'][$key]['id_imagen']);
            }
            unset($_SESSION['imagenes']);
        }   
    }
    // Mismas validaciones que en imágenes
    if (isset($_SESSION['archivos']) || $RutaAdjuntosPublicacion == '') {
        eliminarArchivosArticulo($db, $IdPublicacion);
            // Una vez borrada las relaciones, si se adjuntaron nuevas, crear la nueva relacion
        if (isset($_SESSION['archivos'])){
            foreach ($_SESSION['archivos'] as $key => $id_archivo) {
                guardarArchivosArticulos($db, $IdPublicacion, $_SESSION['archivos'][$key]['id_archivo']);
            }
            unset($_SESSION['archivos']);
        }   
    }

    // Insertar a qué subcategoría pertenece la publicación
    eliminarCorrespondeA($db, $IdPublicacion);
    guardarCorrespondeA($db, $IdPublicacion, $ArraySubcategorias);
    
    desconectar($db);
}

function eliminarImagenesArticulo($db, $id) {
    // Eliminar los archivos del servidor
    $query = 'SELECT * FROM imagenesarticulo i, publicacion p WHERE p.IdPublicacion = i.IdPublicacion AND i.IdPublicacion =' . $id;
    $registros = $db->query($query);
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
        $imagen =   "../images/" . $fila['rutaImagenes'];
        // Para borrar la imagen del servidor
        unlink($imagen);
    }  
    // it releases the associated results
    mysqli_free_result($registros);

    $query = "DELETE FROM imagenesarticulo WHERE IdPublicacion=?";

    if (!($statement = $db->prepare($query))) {
        die("Preparación fallida: (" . $db->errno . ") " . $db->error);
    }
    if (!($statement->bind_param("s", $id))) {
        die("Vinculación de parámetros fallida: (" . $statement->errno . ") " . $statement->error);
    }
    if (!$statement->execute()) {
        die("Ejecución fallida: (" . $statement->errno . ") " . $statement->error);
    }
}

function eliminarArchivosArticulo($db, $id) {
    // Eliminar los archivos del servidor
    $query = 'SELECT * FROM archivosarticulo a, publicacion p WHERE p.IdPublicacion = a.IdPublicacion AND a.IdPublicacion =' . $id;
    $registros = $db->query($query);
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
        $archivo =   "../files/" . $fila['rutaArchivos'];
        // Para borrar la imagen del servidor
        unlink($archivo);
    }  
    // it releases the associated results
    mysqli_free_result($registros);

    $query = "DELETE FROM archivosarticulo WHERE IdPublicacion=?";

    if (!($statement = $db->prepare($query))) {
        die("Preparación fallida: (" . $db->errno . ") " . $db->error);
    }
    if (!($statement->bind_param("s", $id))) {
        die("Vinculación de parámetros fallida: (" . $statement->errno . ") " . $statement->error);
    }
    if (!$statement->execute()) {
        die("Ejecución fallida: (" . $statement->errno . ") " . $statement->error);
    }
}

function eliminarArticulo($id)
{
    $db = conectar();
    eliminarCorrespondeA($db, $id);
    eliminarImagenesArticulo($db, $id);
    eliminarArchivosArticulo($db, $id);
    
    $query = "DELETE FROM bitacora WHERE IdPublicacion=?";

    if (!($statement = $db->prepare($query))) {
        die("Preparación fallida: (" . $db->errno . ") " . $db->error);
    }
    if (!($statement->bind_param("s", $id))) {
        die("Vinculación de parámetros fallida: (" . $statement->errno . ") " . $statement->error);
    }
    if (!$statement->execute()) {
        die("Ejecución fallida: (" . $statement->errno . ") " . $statement->error);
    }

    $query = "DELETE FROM publicacion WHERE IdPublicacion=?";

    if (!($statement = $db->prepare($query))) {
        die("Preparación fallida: (" . $db->errno . ") " . $db->error);
    }
    if (!($statement->bind_param("s", $id))) {
        die("Vinculación de parámetros fallida: (" . $statement->errno . ") " . $statement->error);
    }
    if (!$statement->execute()) {
        die("Ejecución fallida: (" . $statement->errno . ") " . $statement->error);
    }
    
    desconectar($db);
}

function eliminarImagen($id){
    $db = conectar();
    
    $query='SELECT * FROM imagenesarticulo WHERE rutaImagenes = "'.$id.'"';
    $registros = $db->query($query);
    $fila = mysqli_fetch_array($registros, MYSQLI_BOTH);
    $idarticulo=$fila['idPublicacion'];
    
    $query = 'UPDATE publicacion SET RutaImagenesPublicacion=? WHERE IdPublicacion ='.$idarticulo;
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params
    $vacio='';
    if (!$statement->bind_param("s", $vacio)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
    }
    // update execution
    if (!($statement->execute())) {
        die("Update failed: (" . $statement->errno . ") " . $statement->error);
    }
    
    $query = 'DELETE FROM imagenesarticulo WHERE rutaImagenes=?';
    if (!($statement = $db->prepare($query))) {
        die("Preparación fallida: (" . $db->errno . ") " . $db->error);
    }
    if (!($statement->bind_param("s", $id))) {
        die("Vinculación de parámetros fallida: (" . $statement->errno . ") " . $statement->error);
    }
    if (!$statement->execute()) {
        die("Ejecución fallida: (" . $statement->errno . ") " . $statement->error);
    }
    
    $imagen =   "../images/" . $id;
    unlink($imagen);
    
    desconectar($db);
    
    return $idarticulo;
}

function eliminarCorrespondeA($db, $id)
{
    $query = "DELETE FROM correspondea WHERE IdPublicacion = ?";
    if (!($statement = $db->prepare($query))) {
        die("Preparación fallida: (" . $db->errno . ") " . $db->error);
    }
    if (!($statement->bind_param("s", $id))) {
        die("Vinculación de parámetros fallida: (" . $statement->errno . ") " . $statement->error);
    }
    if (!$statement->execute()) {
        die("Ejecución fallida: (" . $statement->errno . ") " . $statement->error);
    }
}

function guardarCorrespondeA($db, $IdPublicacion, $ArraySubcategorias){
    for($i = 0; $i < count($ArraySubcategorias); $i++){
        // insert command specification
        $query='INSERT INTO correspondea (`IdPublicacion` ,`IdSubcategoria`) VALUES (?,?)';
        // Preparing the statement
        if (!($statement = $db->prepare($query))) {
            die("Preparation failed: (" . $db->errno . ") " . $db->error);
        }
        // Binding statement params
        if (!$statement->bind_param("ss", $IdPublicacion, $ArraySubcategorias[$i])) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
        }
        // Executing the statement
        if (!$statement->execute()) {
            die("Execution failed: (" . $statement->errno . ") " . $statement->error);
        }
    }
}

function bitacoraArticulo($IdPublicacion, $IdUsuario, $Descripcion)
{
    $db = conectar();

    $query = 'INSERT INTO bitacora (`IdUsuario`, `IdPublicacion`, `DescripcionBitacora`) VALUES (?,?,?)';

    // Preparing the statement
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params
    if (!$statement->bind_param("sss", $IdUsuario, $IdPublicacion, $Descripcion)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
    }
    // Executing the statement
    if (!$statement->execute()) {
        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
    }

    desconectar($db);
}

function getBitacoraPublicacion($IdPublicacion){
    $db = conectar();

    //Specification of the SQL query
    $query='SELECT * FROM bitacora WHERE IdPublicacion = "'.$IdPublicacion.'" ORDER BY FechaBitacora DESC';
    // Query execution; returns identifier of the result group
    $bitacora = $db->query($query);

    $result = "";

    if($bitacora){
        $i = 1;
        
        // cycle to explode every line of the results
        while ($fila = mysqli_fetch_array($bitacora, MYSQLI_BOTH)) {

            //Specification of the SQL query
            $query='SELECT * FROM usuario WHERE IdUsuario = "'.$fila["IdUsuario"].'"';
            // Query execution; returns identifier of the result group
            $usuario = $db->query($query);

            $empleado = mysqli_fetch_array($usuario, MYSQLI_BOTH);

            $result .= '<div class="row">
                                <div class="col s1 m1 l1">
                                    <p>'.$i.'</p>
                                </div>
                                <div class="col s3 m3 l3">
                                    <p>'.$empleado["Nombre"].' '.$empleado["ApellidoP"].'</p>
                                </div>
                                <div class="col s5 m5 l5">
                                    <p>'.$fila["DescripcionBitacora"].'</p>
                                </div>
                                <div class="col s3 m3 l3">
                                    <p>'.getFechaConFormato($fila["FechaBitacora"]).'</p>
                                </div>
                            </div>
                            <div class="divider"></div>';
            $i++;

            mysqli_free_result($usuario);
        }

        // it releases the associated results
        mysqli_free_result($bitacora);
    }
    
    desconectar($db);

    return $result;
}

function getFechaFinFijo($id = -1, $checked = 0)
{
    if ($id == -1 || $checked == 0) {
        $datepicker = '<input type="text" class="datepicker fecha-fin-fijo" name="fecha-fin-fijo" id="fecha-fin-fijo" disabled>
            <label for="fecha-fin-fijo"><i class="material-icons left">date_range</i>Para cambiar, activa el botón "sí fijar artículo"</label>';
    } elseif ($id != -1 && $checked == 1) {
        $db = conectar();
        //Specification of the SQL query
        $query = 'SELECT * FROM publicacion WHERE IdPublicacion=' . $id;
        // Query execution; returns identifier of the result group
        $registros   = $db->query($query);
        $publicacion = mysqli_fetch_array($registros, MYSQLI_ASSOC);
        // it releases the associated results
        $fecha      = date("Y/m/d", strtotime($publicacion["FechaFinFijoPublicacion"]));
        $datepicker = '<input type="text" class="datepicker fecha-fin-fijo" name="fecha-fin-fijo" id="fecha-fin-fijo"
            data-value="' . $fecha . '"><label class="black-text" for="fecha-fin-fijo">Fecha fin del artículo fijo<i class="material-icons left">date_range</i></label>';
        mysqli_free_result($registros);
    }
    return $datepicker;
}

function getHoraFinFijo($id = -1, $checked = 0)
{
    if ($id == -1 || $checked == 0) {
        $timepicker = '<input type="text" class="timepicker hora-fin-fijo" name="hora-fin-fijo" id="hora-fin-fijo" disabled>
                        <label for="hora-fin-fijo"><i class="material-icons left">access_time</i>Para cambiar, activa el botón "sí fijar articulo"</label>';
    } elseif ($id != -1 && $checked == 1) {
        $db = conectar();
        //Specification of the SQL query
        $query = 'SELECT * FROM publicacion WHERE IdPublicacion=' . $id;
        // Query execution; returns identifier of the result group
        $registros   = $db->query($query);
        $publicacion = mysqli_fetch_array($registros, MYSQLI_ASSOC);
        // it releases the associated results
        $hora       = date('h:iA', strtotime($publicacion["FechaFinFijoPublicacion"]));
        $timepicker = '<input type="text" class="timepicker hora-fin-fijo" name="hora-fin-fijo" id="hora-fin-fijo"
            value="' . $hora . '"><label class="black-text" for="hora-fin-fijo"><i class="material-icons left">access_time</i>Hora fin del artículo fijo</label>';
        mysqli_free_result($registros);
    }
    return $timepicker;
}

function getFechaPublicar($id = -1, $checked = 0)
{
    if ($id == -1 || $checked == 0) {
        $datepicker = '<input type="text" class="datepicker fecha-publicar" name="fecha-publicar" id="fecha-publicar" disabled>
            <label for="fecha-publicar"><i class="material-icons left">date_range</i>Para cambiar, activa el botón "publicar después"</label>';
    } elseif ($id != -1 && $checked == 1) {
        $db = conectar();
        //Specification of the SQL query
        $query = 'SELECT * FROM publicacion WHERE IdPublicacion=' . $id;
        // Query execution; returns identifier of the result group
        $registros   = $db->query($query);
        $publicacion = mysqli_fetch_array($registros, MYSQLI_ASSOC);
        // it releases the associated results
        $fecha      = date("Y/m/d", strtotime($publicacion["FechaPublicacionProgramada"]));
        $datepicker = '<input type="text" class="datepicker fecha-publicar" name="fecha-publicar" id="fecha-publicar"
            data-value="' . $fecha . '"><label class="black-text" for="fecha-publicar"><i class="material-icons left">date_range</i>Fecha en que se publicará el artículo</label>';
        mysqli_free_result($registros);
    }
    return $datepicker;
}

function getHoraPublicar($id = -1, $checked = 0)
{
    if ($id == -1 || $checked == 0) {
        $timepicker = '<input type="text" class="timepicker hora-publicar" name="hora-publicar" id="hora-publicar" disabled>
            <label for="hora-publicar"><i class="material-icons left">access_time</i>Para cambiar, activa el botón "publicar después"</label>';
    } elseif ($id != -1 && $checked == 1) {
        $db = conectar();
        //Specification of the SQL query
        $query = 'SELECT * FROM publicacion WHERE IdPublicacion=' . $id;
        // Query execution; returns identifier of the result group
        $registros   = $db->query($query);
        $publicacion = mysqli_fetch_array($registros, MYSQLI_ASSOC);
        // it releases the associated results
        $hora       = date('h:iA', strtotime($publicacion["FechaPublicacionProgramada"]));
        $timepicker = '<input type="text" class="timepicker hora-publicar" name="hora-publicar" id="hora-publicar"
            value="' . $hora . '"><label class="black-text" for="hora-publicar"><i class="material-icons left">access_time</i>Hora en que se publicará el artículo</label>';
        mysqli_free_result($registros);
    }
    return $timepicker;
}


function getSlider($IdPublicacion) {
    $db = conectar();

    $slider = '<div class="slider slider-articulo">
                <ul class="slides">';  

    $publicacion = getPublicacion($db, $IdPublicacion);

    if ($publicacion['URLVideoPublicacion'] != ''){
        $slider.='  <li>
                        <div class="video-container">
                            <iframe height="650" src="//www.youtube.com/embed/Q8TXgCzxEnw?rel=0" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </li>';
    }            


    if ( $publicacion['RutaImagenesPublicacion'] != '' ){
        //Specification of the SQL query
        $query = 'SELECT * FROM imagenesarticulo i, publicacion p WHERE p.IdPublicacion = i.IdPublicacion AND i.IdPublicacion =' . $IdPublicacion;
        // Query execution; returns identifier of the result group
        $registros = $db->query($query);

        // cycle to explode every line of the results
        while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
            // Options: MYSQLI_NUM to use only numeric indexes
            // MYSQLI_ASSOC to use only name (string) indexes
            // MYSQLI_BOTH, to use both
            $imagen =   "../images/" . $fila['rutaImagenes'];

            $slider.='  <li class="center-align">
                            <img src="'.$imagen.'"> 
                        </li>';
        }
    }   
    
    $slider .= ' </ul>
               </div>';

    // it releases the associated results
    mysqli_free_result($registros);

    desconectar($db);

    return $slider;
}

function getFecha($IdPublicacion){
    $db = conectar();
    $publicacion = getPublicacion($db, $IdPublicacion);
    $fecha  =   date('F d, Y', strtotime($publicacion["FechaPublicacion"]));
      
    $trans = array("January" => "Enero", 
                    "February" => "Febrero", 
                    "March" => "Marzo", 
                    "April" => "Abril", 
                    "May" => "Mayo", 
                    "June" => "Junio", 
                    "July" => "Julio",
                    "August" => "Agosto", 
                    "September" => "Septiembre", 
                    "October" => "Octubre", 
                    "November" => "Noviembre", 
                    "December" => "Diciembre" );

    $fecha = strtr($fecha, $trans);
    desconectar($db);
    return $fecha;
}

function getArticulosBusqueda($col, $busqueda)
{
    $db = conectar();
    
    // Si no tiene los privilegios, no mostrar borradore
     if(isset($_SESSION["privilegios"])) {
        $query = 'SELECT count(*) As total FROM publicacion WHERE LOWER(DescripcionPublicacion) LIKE LOWER(\'%'.$busqueda.'%\') OR LOWER(TituloPublicacion) LIKE LOWER(\'%'.$busqueda.'%\') ORDER BY FechaPublicacion DESC';
     }else{
        $query = 'SELECT count(*) As total FROM publicacion WHERE Borrador = 0 AND (LOWER(DescripcionPublicacion) LIKE LOWER(\'%'.$busqueda.'%\') OR LOWER(TituloPublicacion) LIKE LOWER(\'%'.$busqueda.'%\')) ORDER BY FechaPublicacion DESC';
     }
    

    // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    
    $n = mysqli_fetch_array($registros, MYSQLI_BOTH);
    
    $articulos = '<div class="row">';
    
    if($n["total"]){
        if(isset($_SESSION["privilegios"])) {
            $query = 'SELECT * FROM publicacion WHERE LOWER(DescripcionPublicacion) LIKE LOWER(\'%'.$busqueda.'%\') OR LOWER(TituloPublicacion) LIKE LOWER(\'%'.$busqueda.'%\') ORDER BY FechaPublicacion DESC';
        }else{
            $query = 'SELECT * FROM publicacion WHERE Borrador = 0 AND (LOWER(DescripcionPublicacion) LIKE LOWER(\'%'.$busqueda.'%\') OR LOWER(TituloPublicacion) LIKE LOWER(\'%'.$busqueda.'%\')) ORDER BY FechaPublicacion DESC';
        }
        

        // Query execution; returns identifier of the result group
        $registros = $db->query($query);

        $articulos = '<div class="row">';
        
        $col  = (int) ($col);
        $cont = 0;

        // cycle to explode every line of the results
        while ($publicacion = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
                // Options: MYSQLI_NUM to use only numeric indexes
            // MYSQLI_ASSOC to use only name (string) indexes
            // MYSQLI_BOTH, to use both
            $fecha  = date('F d, Y', strtotime($publicacion["FechaPublicacion"]));
            $imagen = "../images/default.png";
            $fecha  = date('F d, Y', strtotime($publicacion["FechaPublicacion"]));
            $titulo = trunc($publicacion["TituloPublicacion"], 15);
            $cuerpo = trunc($publicacion["DescripcionPublicacion"], 280);
            $id     = $publicacion["IdPublicacion"];

            // Traduce la fecha
            $trans = array("January" => "Enero",
                    "February" => "Febrero",
                    "March" => "Marzo",
                    "April" => "Abril",
                    "May" => "Mayo",
                    "June" => "Junio",
                    "July" => "Julio",
                    "August" => "Agosto",
                    "September" => "Septiembre",
                    "October" => "Octubre",
                    "November" => "Noviembre",
                    "December" => "Diciembre");
            $fecha = strtr($fecha, $trans);

            // Cargar la primera imagen del artículo
            if(isset($publicacion['RutaImagenesPublicacion']) && $publicacion['RutaImagenesPublicacion'] != ''){
                $query          = 'SELECT * FROM imagenesarticulo WHERE idPublicacion=' . $id . ' ORDER BY idImagenesArticulos ASC LIMIT 1';
                $imagenes       = $db->query($query);
                $imagenarticulo = mysqli_fetch_array($imagenes, MYSQLI_BOTH);
                $imagen         = "../images/" . $imagenarticulo["rutaImagenes"];
            }

            $articulos .= '<div class="col s12 m6 l' . 12 / ($col % 12) . '">';
            if($publicacion["Borrador"] == 1){
              $articulos .=            '<div class="card small hoverable">
                                          <a href="_articulo.php?id='.$id.'">
                                              <div class="card-image">
                                                <img class="responsive-img" src="' . $imagen . '" style="opacity:.3;">
                                                <span class="card-title black-text" style="text-shadow:.5px .5px #000000;">(Borrador)</span>
                                              </div>
                                          </a>
                                          <div class="card-content">
                                              <span class="card-title grey-text text-darken-4">
                                                  <a href="_articulo.php?id='.$id.'">' . $titulo . '</a>
                                                  <i class="material-icons activator right">more_vert</i>
                                              </span>
                                              <span class="card-title grey-text text-darken-4"><small>' . $fecha . '</small>';
            }else{
              $articulos .=            '<div class="card small hoverable">
                                          <a href="_articulo.php?id='.$id.'">
                                              <div class="card-image">
                                                <img class="responsive-img" src="' . $imagen . '">
                                              </div>
                                          </a>
                                          <div class="card-content">
                                              <span class="card-title grey-text text-darken-4">
                                                  <a href="_articulo.php?id='.$id.'">' . $titulo . '</a>
                                                  <i class="material-icons activator right">more_vert</i>
                                              </span>

                                              <span class="card-title grey-text text-darken-4"><small>' . $fecha . '</small>';
            }


            if(isset($_SESSION["privilegios"])){
                if(binarySearch($_SESSION["privilegios"], 3)){
                    $articulos .= '<a href="#!" class="right botonBorrar btn-floating red" data-ruta="eliminar-articulo.php" data-tooltip="Eliminar articulo"
                                    data-id="' . $id . '" data-tipo="articulo">
                                        <i class="material-icons">delete_forever</i>
                                    </a>';
                }
            }

            if (isset($_SESSION["privilegios"])) {
                if (binarySearch($_SESSION["privilegios"], 2)) {
                    $articulos .= '<a href="editar-articulo.php?id=' . $id . '" data-tooltip="Editar articulo" class="right  btn-floating blue"><i class="material-icons">edit</i></a>';
                }
            }

            $articulos .= '             </span>
                                        </div>
                                        <div class="card-reveal">
                                          <span class="card-title grey-text text-darken-4">' . $titulo . '<i class="material-icons right">close</i></span>
                                          <p>' . $cuerpo . '</p>
                                        </div>
                                    </div>';
            $articulos .= '                 </div>';
            $cont++;
        }

        // it releases the associated results
        mysqli_free_result($registros);
    }else{
        $articulos .= '<h5 class="center-align">No se encontraron resultados para la búsqueda "'.$busqueda.'"</h5>';
    }
    
    $articulos .= '</div>';
    
    desconectar($db);
    return $articulos;
}

function getArchivosAdjuntos($publicacion){
    if ($publicacion["RutaAdjuntosPublicacion"] != ''){
        $db = conectar();
        $query = 'SELECT * FROM archivosarticulo WHERE IdPublicacion=' . $publicacion["IdPublicacion"] . ' ORDER BY IdArchivosArticulos';
        $registros = $db->query($query);
        $adjuntos = '<div class="row">';
        $adjuntos .= '<h5 class="titulo-adjuntos">Adjuntos</h5>';
        while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
            $adjuntos .='<div class="col s3 m2 l2">
                            <a href="../files/'.$fila["rutaArchivos"].'" target="_blank">
                                <div class="card z-depth-0 file">
                                    <div class="card-content center-align">
                                        <span> <i class="material-icons medium center-align red-text">picture_as_pdf</i></span>
                                        <span class="card-title center-align" style="font-size: 1rem">'.trunc(substr($fila["rutaArchivos"], 11),32).'</span>
                                    </div>
                                </div>
                            </a>
                        </div>';
        }
        $adjuntos .= '</div>';
        return $adjuntos;
    }else{
        return;
    }
}

function getArchivosAdjuntosEditar($publicacion){
    if ($publicacion["RutaAdjuntosPublicacion"] != ''){
        $db = conectar();
        $query = 'SELECT * FROM archivosarticulo WHERE IdPublicacion=' . $publicacion["IdPublicacion"] . ' ORDER BY IdArchivosArticulos';
        $registros = $db->query($query);
        $adjuntos = '<div class="row">';
        $adjuntos .= '<h5 class="titulo-adjuntos">Adjuntos</h5>';
        while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
            $id = $fila['idArchivosArticulos'];
            $adjuntos .='<div class="col s3 m2 l2 center-align z-depth-1">
                            <a href="../files/'.$fila["rutaArchivos"].'" target="_blank">
                                <div class="card z-depth-0 file">
                                    <div class="card-content center-align">
                                        <span> <i class="material-icons medium center-align red-text">picture_as_pdf</i></span>
                                        <span class="card-title center-align" style="font-size: 1rem">'.trunc(substr($fila["rutaArchivos"], 11),32).'</span>
                                    </div>
                                </div>
                            </a>';
            $adjuntos .= '<a href="#!" class="botonBorrar btn-floating red" data-ruta="eliminar-adjunto.php" data-tooltip="Eliminar archivo adjunto"
                                data-id="' . $fila['rutaArchivos'] . '" data-tipo="archivo adjunto">
                                    <i class="material-icons">delete_forever</i>
                                </a>';
            $adjuntos .= '</div>';
        }
        $adjuntos .= '</div>';
        return $adjuntos;
    }else{
        return;
    }   
}

function getImagenesEditar($publicacion){
    if ($publicacion["RutaImagenesPublicacion"] != ''){
        $db = conectar();
        $query = 'SELECT * FROM imagenesarticulo WHERE IdPublicacion=' . $publicacion["IdPublicacion"] . ' ORDER BY idImagenesArticulos';
        $registros = $db->query($query);
        
        $imagenes = '<div class="row">';

        // cycle to explode every line of the results
        while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
            // Options: MYSQLI_NUM to use only numeric indexes
            // MYSQLI_ASSOC to use only name (string) indexes
            // MYSQLI_BOTH, to use both
            $imagen =   "../images/" . $fila['rutaImagenes'];
            $id     = $fila["idImagenesArticulos"];

            $imagenes .='  <div class="col s12 m12 l12 center-align">
                                <div class="row valign-wrapper"> 
                                    <div class="col s11 m11 l11">
                                        <img class="materialboxed" style="max-height: 300px; width:auto;" src="'.$imagen.'">
                                    </div>
                                    <div class="col s1 m1 l1">';

            if(isset($_SESSION["privilegios"])){
                $imagenes .= '  <a href="#!" class="right botonBorrar btn-floating red" data-ruta="eliminar-imagen.php"
                                data-id="' . $fila['rutaImagenes'] . '" data-tipo="banner">
                                        <i class="material-icons">delete_forever</i>
                                </a>';
            }

            $imagenes .='                            
                                    </div>
                                </div>
                            </div>';
        }
        
        $imagenes .= '</div>';

        // it releases the associated results
        mysqli_free_result($registros);

        desconectar($db);

        return $imagenes;
    }else{
        return;
    }   
}

function eliminarAdjunto($id){
    $db = conectar();
    
    $query='SELECT * FROM archivosarticulo WHERE rutaArchivos = "'.$id.'"';
    $registros = $db->query($query);
    $fila = mysqli_fetch_array($registros, MYSQLI_BOTH);
    $idarticulo=$fila['idPublicacion'];
    
    $query = 'UPDATE publicacion SET RutaAdjuntosPublicacion=? WHERE IdPublicacion ='.$idarticulo;
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params
    $vacio='';
    if (!$statement->bind_param("s", $vacio)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
    }
    // update execution
    if (!($statement->execute())) {
        die("Update failed: (" . $statement->errno . ") " . $statement->error);
    }
    
    $query = 'DELETE FROM archivosarticulo WHERE rutaArchivos=?';
    if (!($statement = $db->prepare($query))) {
        die("Preparación fallida: (" . $db->errno . ") " . $db->error);
    }
    if (!($statement->bind_param("s", $id))) {
        die("Vinculación de parámetros fallida: (" . $statement->errno . ") " . $statement->error);
    }
    if (!$statement->execute()) {
        die("Ejecución fallida: (" . $statement->errno . ") " . $statement->error);
    }
    
    $archivo =   "../files/" . $id;
    unlink($archivo);
    
    desconectar($db);
    
    return $idarticulo;
}

function revisarPublicarDespues() {
    $db = conectar();
    // Publica los artículos si la fecha si ya paso la fecha que tenian programados para publicarse
    $query='SELECT * FROM publicacion WHERE Borrador = 0 AND PublicarDespues = 1 ORDER BY FechaPublicacion';
    $registros=mysqli_query($db,$query);
    // Checar si existen artículos para publicar despues
    if ( mysqli_num_rows($registros) > 0  ) { 
        while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
            // Revisar si la fecha de publicar despues ya paso
             if( time() - strtotime($fila["FechaPublicacionProgramada"]) > 0 ) {
                // Si la fecha ya paso, cambiar la bandera
                $query = 'UPDATE publicacion SET PublicarDespues=? WHERE IdPublicacion =' . $fila["IdPublicacion"];
                // Preparing the statement
                if (!($statement = $db->prepare($query))) {
                    die("Preparation failed: (" . $db->errno . ") " . $db->error);
                }
                // Binding statement params
                $flag = 0;
                if (!$statement->bind_param("s", $flag)) {
                    die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
                }
                // update execution
                if (!($statement->execute())) {
                    die("Update failed: (" . $statement->errno . ") " . $statement->error);
                }
            }     
        }
    }
    desconectar($db);
}

function revisarFijos() {
    $db = conectar();
    // Quita los artículos fijos que ya paso la fecha hasta la que tenian que permanecer fijos
    $query='SELECT * FROM publicacion WHERE Borrador = 0 AND FijoPublicacion = 1 AND PublicarDespues = 0 ORDER BY FechaPublicacion';
    $registros=mysqli_query($db,$query);
    // Checar si existen artículos fijos
    if ( mysqli_num_rows($registros) > 0  ) { 
        while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
            // Revisar si la fecha de fin fijo ya paso
             if( time() - strtotime($fila["FechaFinFijoPublicacion"]) > 0 ) {
                // Si la fecha ya paso, cambiar la bandera
                $query = 'UPDATE publicacion SET FijoPublicacion=? WHERE IdPublicacion =' . $fila["IdPublicacion"];
                // Preparing the statement
                if (!($statement = $db->prepare($query))) {
                    die("Preparation failed: (" . $db->errno . ") " . $db->error);
                }
                // Binding statement params
                $flag = 0;
                if (!$statement->bind_param("s", $flag)) {
                    die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
                }
                // update execution
                if (!($statement->execute())) {
                    die("Update failed: (" . $statement->errno . ") " . $statement->error);
                }
            }
        }
    }
    desconectar($db);
}

function getDropdownListarArticulo($IdSubcategoria=-1, $tipo='generales'){
    if($IdSubcategoria == -1){
        $select = "<option value='{\"subcategoria\":\"".$IdSubcategoria."\",\"valor\":\"Publicados\",\"tipo\":\"".$tipo."\"}' selected>Listar todos los artículos publicados</option>";
        $select .= "<option value='{\"subcategoria\":\"".$IdSubcategoria."\",\"valor\":\"PublicarDespues\", \"tipo\":\"".$tipo."\"}'>Listar todos los artículos programados para publicar después</option>";
        $select .= "<option value='{\"subcategoria\":\"".$IdSubcategoria."\",\"valor\":\"Borrador\", \"tipo\":\"".$tipo."\"}'>Listar todos los borradores de artículos</option>";
    }else{
        $select = "<option value='{\"subcategoria\":\"".$IdSubcategoria."\",\"valor\":\"Publicados\",\"tipo\":\"".$tipo."\"}' selected>Listar artículos publicados en ".getNombreSubcategoria($IdSubcategoria)."</option>";
        $select .= "<option value='{\"subcategoria\":\"".$IdSubcategoria."\",\"valor\":\"PublicarDespues\", \"tipo\":\"".$tipo."\"}'>Listar artículos programados para publicar después en ".getNombreSubcategoria($IdSubcategoria)."</option>";
        $select .= "<option value='{\"subcategoria\":\"".$IdSubcategoria."\",\"valor\":\"Borrador\", \"tipo\":\"".$tipo."\"}'>Listar borradores de artículos en ".getNombreSubcategoria($IdSubcategoria)."</option>";
    }
    
    return $select;
}

function getDropdownCategoria($selected = -1)
{
    $db = conectar();

    //Specification of the SQL query
    $query = 'SELECT * FROM categoria ORDER BY IdCategoria';
    // Query execution; returns identifier of the result group
    $registros = $db->query($query);

    // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
        if ($selected == $fila["IdCategoria"]) {
            $select .= '<option value="' . $fila["IdCategoria"] . '" selected>' . $fila["NombreCategoria"] . '</option>';
        } else {
            $select .= '<option value="' . $fila["IdCategoria"] . '">' . $fila["NombreCategoria"] . '</option>';
        }

    }

    // it releases the associated results
    mysqli_free_result($registros);

    desconectar($db);

    return $select;
}

function getDropdownSubcategoria($id = -1, $selected = -1)
{

    if ($id == -1) {
        $select = '<option value="" disabled="" selected="">Subcategoría (Escoge una categoría primero)*</option>';
    } else {
        $db = conectar();
        //Specification of the SQL query
        $query = 'SELECT * FROM subcategoria WHERE IdCategoria=' . $id . ' ORDER BY IdSubcategoria';
        // Query execution; returns identifier of the result group
        $registros = $db->query($query);

        $select = '<option value="" disabled="" selected>Subcategoría de ' . getCategoria($id) . ' (Requerido)*</option>';
        // cycle to explode every line of the results
        while ($fila = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
            if ($selected == $fila["IdSubcategoria"]) {
                $select .= '<option value="' . $fila["IdSubcategoria"] . '" selected>' . $fila["NombreSubcategoria"] . '</option>';
            } else {
                $select .= '<option value="' . $fila["IdSubcategoria"] . '">' . $fila["NombreSubcategoria"] . '</option>';
            }
        }

        // it releases the associated results
        mysqli_free_result($registros);

        desconectar($db);
    }
    return $select;
}

function getDescripcionSubcategoria($id){
    $db = conectar();
    //Specification of the SQL query
    $query='SELECT * FROM subcategoria WHERE IdSubcategoria = "'.$id.'"';
    // Query execution; returns identifier of the result group
    $registros = $db->query($query);
    $departamento = mysqli_fetch_array($registros, MYSQLI_BOTH);

    return $departamento["Descripcion"];
}

function getFenomenos($IdSubcategoria, $columna=-1){
    revisarPublicarDespues();
    revisarFijos();
    $db = conectar(); 
    
    $query = 'SELECT * FROM publicacion p, correspondea c WHERE p.IdPublicacion = c.IdPublicacion AND p.Borrador = 0 AND p.PublicarDespues = 0 AND c.IdSubcategoria =' . $IdSubcategoria . ' ORDER BY p.FijoPublicacion DESC, p.FechaPublicacion DESC';
    
    if ($IdSubcategoria > -1) {
        if (isset($_SESSION["privilegios"])) {
            // Cargar artículos por columa seleccionada
            if ($columna == 'Publicados'){
                $query = 'SELECT * FROM publicacion p, correspondea c WHERE p.IdPublicacion = c.IdPublicacion AND p.Borrador = 0 AND p.PublicarDespues = 0 AND c.IdSubcategoria =' . $IdSubcategoria . ' ORDER BY p.FijoPublicacion DESC, p.FechaPublicacion DESC';
            }else{
                $query = 'SELECT * FROM publicacion p, correspondea c WHERE p.IdPublicacion = c.IdPublicacion AND '. $columna .' = 1 AND c.IdSubcategoria =' . $IdSubcategoria . ' ORDER BY p.FijoPublicacion DESC, p.FechaPublicacion DESC';
            }
        } else {
            // No puede ver los borradores ni los articulos que están programados para publicarse despues
            $query = 'SELECT * FROM publicacion p, correspondea c WHERE p.IdPublicacion = c.IdPublicacion AND p.Borrador=0 AND p.PublicarDespues=0 AND c.IdSubcategoria =' . $IdSubcategoria . ' ORDER BY p.FijoPublicacion DESC, p.FechaPublicacion DESC';
        }
    } else {
        if ($columna == 'Publicados'){
            $query = 'SELECT * FROM publicacion WHERE Borrador = 0 AND PublicarDespues = 0 ORDER BY FechaPublicacion DESC';
        }else{
            $query = 'SELECT * FROM publicacion WHERE '. $columna .' = 1 ORDER BY FechaPublicacion DESC';
        }  
    }

    // Query execution; returns identifier of the result group
    $registros = mysqli_query($db,$query);  
    

    $cont = 0;
    
    $articulos = "";

     if ( mysqli_num_rows($registros) == 0){
        $articulos .= "";
        $subcategoria=getSubcategoria($IdSubcategoria);
        if (!isset($_SESSION["privilegios"])) {
            if ($subcategoria["Descripcion"] == ''){
                $articulos .= "<p class='center-align'>Estamos trabajando en esta sección para brindarte un mejor servicio.</p>";
            }
        }
    }

    if ( mysqli_num_rows($registros) == 0){
        $articulos .= "";
        if (isset($_SESSION["privilegios"]) && $articulos != '') {
            $articulos .= "<p>Por el momento no hay publicaciones en este apartado.</p>";
        }
        return $articulos;
    }

    
    // cycle to explode every line of the results
    while ($publicacion = mysqli_fetch_array($registros, MYSQLI_BOTH)){
        $fecha  = date('F d, Y', strtotime($publicacion["FechaPublicacion"]));
        $imagen = "../images/default.png";
        $fecha  = date('F d, Y', strtotime($publicacion["FechaPublicacion"]));
        $titulo = trunc($publicacion["TituloPublicacion"], 12);
        $cuerpo = trunc($publicacion["DescripcionPublicacion"], 400);
        $id     = $publicacion["IdPublicacion"];
        $fijo   = $publicacion["FijoPublicacion"];

        // Traduce la fecha
        $trans = array("January" => "Enero",
                "February" => "Febrero",
                "March" => "Marzo",
                "April" => "Abril",
                "May" => "Mayo",
                "June" => "Junio",
                "July" => "Julio",
                "August" => "Agosto",
                "September" => "Septiembre",
                "October" => "Octubre",
                "November" => "Noviembre",
                "December" => "Diciembre");
        $fecha = strtr($fecha, $trans);

        // Cargar la primera imagen del artículo
        if(isset($publicacion['RutaImagenesPublicacion']) && $publicacion['RutaImagenesPublicacion'] != ''){
            $query          = 'SELECT * FROM imagenesarticulo WHERE idPublicacion=' . $id . ' ORDER BY idImagenesArticulos ASC LIMIT 1';
            $imagenes       = $db->query($query);
            $imagenarticulo = mysqli_fetch_array($imagenes, MYSQLI_BOTH);
            $imagen         = "../images/" . $imagenarticulo["rutaImagenes"];
        }
        
        $articulos .= '<div class="row">';
        $articulos .= '<div class="section">';
        $articulos .= '<div class="col s12 m12 l12">';
        
        if($publicacion["Borrador"] == 1){
          $articulos .=            '<div class="card horizontal fenomenos">
                                    <div class="row">';
                                      
        $articulos .= '<div class="col s12 m6 l5">';
        if ($imagen == '../images/default.png'){
            $articulos .= "<div class='card-image valign-wrapper default'>";
        }else{
            $articulos .= "<div class='card-image valign-wrapper'>";
        }  
            
        

        $articulos .=                  '    
                                            <img class="responsive-img" src="' . $imagen . '" style="opacity:.3;">
                                            <span class="card-title black-text" style="text-shadow:.5px .5px #000000;">(Borrador)</span>
                                            </a>
                                          </div>
                                      </div><div class="col s12 m6 l7">  
                                      <div class="card-stacked">
                                        <div class="card-content">
                                          <span class="card-title grey-text text-darken-4">
                                              <a href="_articulo.php?id='.$id.'">';
                                              if ($fijo == 1){
                                                $articulos .= '<i class="material-icons tiny tooltipped" data-position="top" data-delay="50" data-tooltip="Artículo fijo" style="margin-right: 5px;">gps_fixed</i>';
                                              }
        $articulos .=                       $titulo . '
                                              </a>
                                          </span>
                                          <span class="card-title grey-text text-darken-4">
                                            <p class="cuerpo">'.$cuerpo.'</p>
                                          </span>
                                          <span class="card-title grey-text text-darken-4"><small>' . $fecha . '</small>';
        }else{
            $articulos .=            '<div class="card horizontal fenomenos">
                                        <div class="row">';

           $articulos .= '<div class="col s12 m6 l5">';                                
           if ($imagen == '../images/default.png'){
                $articulos .= "<div class='card-image valign-wrapper default'>";
            }else{
                $articulos .= "<div class='card-image valign-wrapper'>";
            }  
            $articulos .= '<a href="_articulo.php?id='.$id.'">';

            $articulos .=                      '<img class="responsive-img" src="' . $imagen . '">
                                            </a>
                                        </div>
                                      </div><div class="col s12 m6 l7">
                                      <div class="card-stacked">
                                        <div class="card-content">
                                          <span class="card-title grey-text text-darken-4">
                                            <a href="_articulo.php?id='.$id.'">';
                                            if ($fijo == 1){
                                                $articulos .= '<i class="material-icons tiny tooltipped" data-position="top" data-delay="50" data-tooltip="Artículo fijo" style="margin-right: 5px;">gps_fixed</i>';
                                            }
            $articulos .=                       $titulo . '
                                              </a>
                                          </span>
                                          <span class="card-title grey-text text-darken-4">
                                          <div>
                                            <p class="cuerpo">'.$cuerpo.'</p>
                                            </div>
                                          </span>

                                          <span class="card-title grey-text text-darken-4"><small>' . $fecha . '</small>';
        }


        if(isset($_SESSION["privilegios"])){
            if(binarySearch($_SESSION["privilegios"], 3)){
                $articulos .= '<a href="#!" class="right botonBorrar btn-floating red" data-ruta="eliminar-articulo.php" data-tooltip="Eliminar articulo"
                                data-id="' . $id . '" data-tipo="articulo">
                                    <i class="material-icons">delete_forever</i>
                                </a>';
            }
        }

        if (isset($_SESSION["privilegios"])) {
            if (binarySearch($_SESSION["privilegios"], 2)) {
                $articulos .= '<a href="editar-articulo.php?id=' . $id . '" data-tooltip="Editar articulo" class="right  btn-floating blue"><i class="material-icons">edit</i></a>';
            }
        }

        $articulos .= '             </span>
                                    </div>
                                    </div>
                                  </div>
                                </div>';
        $articulos .= '                 </div></div></div></div>';
        
        $cont++;
    }

    
    
    return $articulos;
}

function editarSubcategoria($IdSubcategoria, $DescripcionSubcategoria, $CorreoSubcategoria, $cc){
    $db = conectar();
    $cc = preg_replace('/\s+/', '', $cc);
    $query='UPDATE subcategoria SET Descripcion=?, CorreoSubcategoria=?, cc=?, RutaImagen=?, RutaImagenUsuario=? WHERE IdSubcategoria=' . $IdSubcategoria;
    $RutaImagen=$_SESSION['imagen']['nombre_real'];
    $RutaImagenUsuario=$_SESSION['imagen']['nombre_usuario'];
    unset($_SESSION['imagen']);
    
    // Preparing the statement
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params
    if (!$statement->bind_param("sssss", $DescripcionSubcategoria, $CorreoSubcategoria, $cc, $RutaImagen, $RutaImagenUsuario)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
    }
    // update execution
    if (!$statement->execute()) {
        die("Update failed: (" . $statement->errno . ") " . $statement->error);
    }

    desconectar($db);

}

function getSubCategoriasEdicion(){
    $categorias = getCategorias();
    $result = "";
    
    while($fila = mysqli_fetch_array($categorias, MYSQLI_BOTH)) {
        $subcategorias = getSubcategorias($fila["IdCategoria"]);
        
        $result .= '<ul class="collection with-header">
            <li class="collection-header">
                <h4><i class="material-icons left small light-blue-text">short_text</i>'.$fila["NombreCategoria"].'</h4>
            </li>';
        
        while($row = mysqli_fetch_array($subcategorias, MYSQLI_BOTH)) {
            $result .= '<li class="collection-item">
                <div>
                    <a href="editar-subcategoria.php?id='.$row["IdSubcategoria"].'"><i class="material-icons left small light-blue-text">edit</i>'.$row["NombreSubcategoria"].'</a>
                </div>
            </li>';
            
        }
        $result .= '</ul>';
    }
    
    return $result;
}

function getImagenSubcategoria($ruta,$tipo='normal'){
    if ($tipo == 'materialboxed'){
        $imagen = '<img class="materialboxed" width="650" src="../images/'.$ruta.'">';
    }else{
        $imagen = '<img height="550" src="../images/'.$ruta.'">';
    } 
    return $imagen;
}

?>