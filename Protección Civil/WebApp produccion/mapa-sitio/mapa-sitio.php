<?php
    session_start();
    require_once("../modelo.php");

    include('../_header.html');
    include('_mapa-sitio.html');

    if(isset($_SESSION["privilegios"])){
        include('../_user-menu.html');
    }
    include('../_footer.html');
?>
