<?php
    session_start();
    include('../_header.html');
    include('_eventos-main.html');

    if(isset($_SESSION["privilegios"])){
        include('../_user-menu.html');
    }

    include('../_footer.html');
        
    if(isset($_SESSION["mensaje"])) {
        $mensaje = $_SESSION["mensaje"];
        include('../_mensaje.html');
        unset($_SESSION["mensaje"]);
    }
?>