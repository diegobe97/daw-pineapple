<?php
    if (isset($_GET["id_evento"])) { 
        session_start();
        require_once "modelo-evento.php";
        
        $id_evento = $_GET["id_evento"];
        $evento = getEvento(conectar(), $id_evento);

        if (isset($evento)){
            if ($evento["EventoPublicado"] == 1){
                if (isset($_SESSION["privilegios"])) {
                    include("../_header.html");
                    include("_evento.html");
                    include("../_user-menu.html");
                    include("../_footer.html");
                }else{
                    include("../_header.html");
                    include("_evento.html");
                    include("../_footer.html");
                }
            }else {
                include("../error.html");    
            }
        }else{
            include("../error.html");
        }

    } else {
        include("../error.html");
    }
?>