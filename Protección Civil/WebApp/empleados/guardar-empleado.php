<?php
    session_start();
    require_once('modelo-empleados.php');

    if(isset($_POST["idUsu"])) {
        editarEmpleado(htmlspecialchars($_POST["nombreUsuario"]), htmlspecialchars($_POST["idEmpleado"]), htmlspecialchars($_POST["nombre"]), htmlspecialchars($_POST["apePaterno"]), htmlspecialchars($_POST["apeMaterno"]), htmlspecialchars($_POST["puesto"]), $_POST["rol"], $_POST["departamento"], $_POST["idUsu"]);

        $_SESSION["mensaje"] = $_POST["nombre"].' se actualizó correctamente.';
    } else {
        guardarEmpleado(htmlspecialchars($_POST["nombreUsuario"]), htmlspecialchars($_POST["idEmpleado"]), htmlspecialchars($_POST["nombre"]), htmlspecialchars($_POST["apePaterno"]), htmlspecialchars($_POST["apeMaterno"]), htmlspecialchars($_POST["puesto"]), $_POST["rol"], $_POST["departamento"]);

        $_SESSION["mensaje"] = $_POST["nombre"].' se creó correctamente.';
    }

    header("location:ver-empleados.php");
?>
