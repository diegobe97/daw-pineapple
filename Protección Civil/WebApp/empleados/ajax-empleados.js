$("#nombre").on('blur', function() {
    var error_nombre = document.getElementById('error-nombre');
    error_nombre.innerHTML = '';
    if (nombre.value == '') {
        error_nombre.innerHTML += 'Este campo es obligatorio';
    }
});

$("#apePaterno").on('blur', function() {
    var error_apePaterno = document.getElementById('error-apePaterno');
    error_apePaterno.innerHTML = '';
    if (apePaterno.value == '') {
        error_apePaterno.innerHTML += 'Este campo es obligatorio';
    }
});

$("#apeMaterno").on('blur', function() {
    var error_apeMaterno = document.getElementById('error-apeMaterno');
    error_apeMaterno.innerHTML = '';
    if (apeMaterno.value == '') {
        error_apeMaterno.innerHTML += 'Este campo es obligatorio';
    }
});

$("#idEmpleado").on('blur', function() {
    var error_idEmpleado = document.getElementById('error-idEmpleado');
    error_idEmpleado.innerHTML = '';
    if (idEmpleado.value == '') {
        error_idEmpleado.innerHTML += 'Este campo es obligatorio';
    }
});

$("#nombreUsuario").on('blur', function() {
    var error_nombreUsuario = document.getElementById('error-nombreUsuario');
    error_nombreUsuario.innerHTML = '';
    if (nombreUsuario.value == '') {
        error_nombreUsuario.innerHTML += 'Este campo es obligatorio';
    }
});

$("#puesto").on('blur', function() {
    var error_puesto = document.getElementById('error-puesto');
    error_puesto.innerHTML = '';
    if (puesto.value == '') {
        error_puesto.innerHTML += 'Este campo es obligatorio';
    }
});

$("#pass").on('blur', function() {
    var error_pass = document.getElementById('error-pass');
    error_pass.innerHTML = '';
    if (pass.value == '') {
        error_pass.innerHTML += 'Este campo es obligatorio';
    }
});

$("#confirmPass").on('blur', function() {
    var error_confirmPass = document.getElementById('error-confirmPass');
    error_confirmPass.innerHTML = '';
    if (confirmPass.value == '') {
        error_confirmPass.innerHTML += 'Este campo es obligatorio';
    }
});

//$(".select-dropdown").css("color", "#e57373");
