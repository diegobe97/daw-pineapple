-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 30, 2017 at 11:36 PM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cepcq`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `crearUsuario` (IN `usu` VARCHAR(255), IN `idE` INT(8), IN `nom` VARCHAR(50), IN `apeP` VARCHAR(50), IN `apeM` VARCHAR(50), IN `pu` VARCHAR(50), IN `idR` INT(8), IN `idDep` INT(8))  BEGIN
	DECLARE NombreUsuario VARCHAR(50) DEFAULT 'nombre usuario';
	DECLARE IdEmpleado INT(8) DEFAULT 0;
	DECLARE Nombre VARCHAR(50) DEFAULT 'nombre';
	DECLARE ApellidoP VARCHAR(50) DEFAULT 'apellido paterno';
	DECLARE ApellidoM VARCHAR(50) DEFAULT 'apellido materno';
	DECLARE Puesto VARCHAR(50) DEFAULT 'puesto';
	DECLARE IdRol INT(8) DEFAULT 0;
	DECLARE IdDepartamento INT(8) DEFAULT 0;
    DECLARE IdUsuario INT(11) DEFAULT 0;
	SET NombreUsuario = usu;
	SET nombre = nom;
	SET IdEmpleado = idE;
	SET ApellidoP = apeP;
	SET ApellidoM = apeM;
	SET Puesto = pu;
	SET IdRol = idR;
	SET IdDepartamento = idDep;
    SET IdUsuario = (SELECT `AUTO_INCREMENT`
					FROM  INFORMATION_SCHEMA.TABLES
					WHERE TABLE_SCHEMA = 'cepcq'
					AND   TABLE_NAME   = 'usuario');
    START TRANSACTION;
	INSERT INTO usuario (NombreUsuario, IdEmpleado, Nombre, ApellidoP, ApellidoM, Puesto) VALUES(NombreUsuario, IdEmpleado, Nombre, ApellidoP, ApellidoM, Puesto);
	INSERT INTO asignadoa (IdUsuario, IdRol) VALUES(IdUsuario, IdRol);
	INSERT INTO trabajaen (IdUsuario, IdDepartamento) VALUES(IdUsuario, IdDepartamento);
	COMMIT;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `editarUsuario` (IN `usu` VARCHAR(255), IN `idE` INT(8), IN `nom` VARCHAR(50), IN `apeP` VARCHAR(50), IN `apeM` VARCHAR(50), IN `pu` VARCHAR(50), IN `idR` INT(8), IN `idDep` INT(8), IN `idUsu` INT(11))  BEGIN
	DECLARE nomU VARCHAR(50) DEFAULT 'nombre usuario';
	DECLARE idEm INT(8) DEFAULT 0;
	DECLARE nomb VARCHAR(50) DEFAULT 'nombre';
	DECLARE apellP VARCHAR(50) DEFAULT 'apellido paterno';
	DECLARE apellM VARCHAR(50) DEFAULT 'apellido materno';
	DECLARE pues VARCHAR(50) DEFAULT 'puesto';
	DECLARE idRl INT(8) DEFAULT 0;
	DECLARE idDepar INT(8) DEFAULT 0;
    DECLARE idU INT(11) DEFAULT 0;
    SET idU = idUsu;
	SET nomU = usu;
	SET nomb = nom;
	SET idEm = idE;
	SET apellP = apeP;
	SET apellM = apeM;
	SET pues = pu;
	SET idRl = idR;
	SET idDepar = idDep;
	START TRANSACTION;
    UPDATE usuario SET NombreUsuario = nomU, IdEmpleado = idEm, Nombre = nomb, ApellidoP = apellP, ApellidoM = apellM, Puesto = pues WHERE IdUsuario = idU;
	UPDATE asignadoa SET IdRol = idRl WHERE IdUsuario = idU;
	UPDATE trabajaen SET IdDepartamento = idDepar WHERE IdUsuario = idU;
	COMMIT;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `archivosarticulo`
--

CREATE TABLE `archivosarticulo` (
  `idArchivosArticulos` int(11) NOT NULL,
  `rutaArchivos` varchar(300) NOT NULL,
  `idPublicacion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `archivosarticulo`
--

INSERT INTO `archivosarticulo` (`idArchivosArticulos`, `rutaArchivos`, `idPublicacion`) VALUES
(16, '1511221468-Ejercicio 1 - Caso farmacéutica.pdf', 106),
(17, '1511221808-Ejercicio 1 - Caso farmacéutica.pdf', 107),
(18, '1511221932-Ejercicio 1 - Caso farmacéutica.pdf', 108),
(19, '1511222098-Ejercicio 1 - Caso farmacéutica.pdf', 109),
(20, '1511222336-Ejercicio 1 - Caso farmacéutica.pdf', 110),
(21, '1511222644-Ejercicio 1 - Caso farmacéutica.pdf', 111),
(22, '1511222731-Ejercicio 1 - Caso farmacéutica.pdf', 112),
(23, '1511929914-Examen DAW AD 2017 parcial 2.pdf', NULL),
(24, '1511929916-Examen DAW AD 2017 parcial 2.pdf', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `archivosevento`
--

CREATE TABLE `archivosevento` (
  `IdArchivosEventos` int(11) NOT NULL,
  `RutaArchivos` varchar(300) CHARACTER SET utf8 NOT NULL,
  `IdEvento` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `archivosevento`
--

INSERT INTO `archivosevento` (`IdArchivosEventos`, `RutaArchivos`, `IdEvento`) VALUES
(1, '1511928959-24058687_1533893450033633_4137293198602541809_n.jpg', NULL),
(2, '1511929238-imagen-de-prueba-320x240.jpg', NULL),
(3, '1511929802-Examen DAW AD 2017 parcial 2.pdf', NULL),
(4, '1511930130-Examen DAW AD 2017 parcial 2.pdf', NULL),
(5, '1511930645-Examen DAW AD 2017 parcial 2.pdf', NULL),
(6, '1511930685-Examen DAW AD 2017 parcial 2.pdf', NULL),
(7, '1511930738-Examen DAW AD 2017 parcial 2.pdf', NULL),
(8, '1511931807-Examen DAW AD 2017 parcial 2.pdf', NULL),
(9, '1511932102-Examen DAW AD 2017 parcial 2.pdf', NULL),
(10, '1511932151-Examen DAW AD 2017 parcial 2.pdf', NULL),
(11, '1511932393-codiciosos.pdf', NULL),
(12, '1511935781-recursivos.pdf', NULL),
(13, '1511935972-recursivos.pdf', NULL),
(14, '1511936029-recursivos.pdf', NULL),
(15, '1511936031-recursivos.pdf', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `asignadoa`
--

CREATE TABLE `asignadoa` (
  `IdUsuario` int(11) NOT NULL,
  `IdRol` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asignadoa`
--

INSERT INTO `asignadoa` (`IdUsuario`, `IdRol`) VALUES
(5, 2),
(1, 1),
(6, 2),
(7, 2),
(8, 2),
(9, 2),
(10, 2);

-- --------------------------------------------------------

--
-- Table structure for table `atendemos`
--

CREATE TABLE `atendemos` (
  `IdAtendemos` int(11) NOT NULL,
  `RutaImagenAtendemos` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `atendemos`
--

INSERT INTO `atendemos` (`IdAtendemos`, `RutaImagenAtendemos`) VALUES
(1, 'atendemos1.jpeg'),
(2, 'atendemos2.jpeg'),
(4, 'atendemos3.jpeg'),
(5, 'atendemos4.jpeg'),
(6, 'atendemos5.jpeg'),
(10, '1512051077-23960764_1486021058111832_1623837839_o (1).jpg');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `IdBanner` int(11) NOT NULL,
  `RutaBanner` varchar(300) NOT NULL,
  `RutaBannerUsuario` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`IdBanner`, `RutaBanner`, `RutaBannerUsuario`) VALUES
(24, '1512052068-borrar_nuevobanner.jpg', 'borrar_nuevobanner.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `bitacora`
--

CREATE TABLE `bitacora` (
  `IdBitacora` int(11) NOT NULL,
  `IdUsuario` int(11) NOT NULL,
  `IdPublicacion` int(11) NOT NULL,
  `FechaBitacora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DescripcionBitacora` varchar(500) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `categoria`
--

CREATE TABLE `categoria` (
  `IdCategoria` int(11) NOT NULL,
  `NombreCategoria` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `categoria`
--

INSERT INTO `categoria` (`IdCategoria`, `NombreCategoria`) VALUES
(1, 'Nosotros'),
(2, 'Fenómenos'),
(3, 'Cultura de la Prevención'),
(4, 'Programas Especiales'),
(5, 'Trámites'),
(6, 'Servicios'),
(7, 'Documentos'),
(8, 'Noticias');

-- --------------------------------------------------------

--
-- Table structure for table `correspondea`
--

CREATE TABLE `correspondea` (
  `IdPublicacion` int(11) NOT NULL,
  `IdSubcategoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `correspondea`
--

INSERT INTO `correspondea` (`IdPublicacion`, `IdSubcategoria`) VALUES
(106, 1),
(108, 3),
(110, 5),
(111, 6),
(112, 7),
(109, 24),
(113, 24),
(114, 25),
(107, 26);

-- --------------------------------------------------------

--
-- Table structure for table `cuentacon`
--

CREATE TABLE `cuentacon` (
  `IdRol` int(8) NOT NULL,
  `IdPrivilegio` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cuentacon`
--

INSERT INTO `cuentacon` (`IdRol`, `IdPrivilegio`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 25),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(3, 22),
(3, 23),
(3, 24),
(2, 3),
(2, 4),
(2, 5),
(2, 6),
(2, 18),
(2, 19),
(2, 17),
(2, 21),
(2, 1),
(2, 2),
(56, 1),
(56, 2),
(56, 3),
(56, 8),
(56, 9),
(56, 10);

-- --------------------------------------------------------

--
-- Table structure for table `departamento`
--

CREATE TABLE `departamento` (
  `IdDepartamento` int(8) NOT NULL,
  `NombreDepartamento` varchar(50) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `DescripcionDepartamento` varchar(250) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departamento`
--

INSERT INTO `departamento` (`IdDepartamento`, `NombreDepartamento`, `DescripcionDepartamento`) VALUES
(1, 'Dirección de Gestión de Riesgos', 'Se encargan del control de posibles eventos futuros.'),
(2, 'Dirección de Atención de Emergencias', 'Se encargan de brindar atención inmediata a situaciones de emergencia y accidentes.'),
(3, 'Dirección de Promoción y Difusión', 'Se encarga de divulgar las actividades y eventos del CEPCQ.'),
(4, 'Dirección de Capacitación ', 'Realizan actividades que fomentan la cultura de la protección'),
(78, 'Àrea de Vinculación ', 'Encargados de la vinculación entre protección civil y los ciudadanos queretanos.');

-- --------------------------------------------------------

--
-- Table structure for table `evento`
--

CREATE TABLE `evento` (
  `IdEvento` int(11) NOT NULL,
  `TituloEvento` varchar(50) CHARACTER SET utf8 NOT NULL,
  `DescripcionEvento` text CHARACTER SET utf8 NOT NULL,
  `FechaPublicacionEvento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FechaInicio` datetime NOT NULL,
  `FechaFin` datetime NOT NULL,
  `URLVideoEvento` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `RutaAdjuntosEvento` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `RutaImagenesEvento` varchar(300) CHARACTER SET utf8 DEFAULT 'arcos.jpg',
  `EventoPublicado` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `evento`
--

INSERT INTO `evento` (`IdEvento`, `TituloEvento`, `DescripcionEvento`, `FechaPublicacionEvento`, `FechaInicio`, `FechaFin`, `URLVideoEvento`, `RutaAdjuntosEvento`, `RutaImagenesEvento`, `EventoPublicado`) VALUES
(9, 'Prueba evento capacitación', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu scelerisque ipsum. Nullam leo velit, imperdiet at vehicula ut, placerat quis lectus. Cras nec ex interdum, tempus erat in, aliquet arcu. Cras vitae fermentum nisl, in bibendum arcu. Pellentesque quis nisl mollis, mattis enim ut, laoreet enim. Nulla enim nunc, laoreet eu elementum vel, malesuada et ligula. Sed vitae felis in nunc ultricies tempor vel vel ex. Proin hendrerit vestibulum maximus. Integer congue velit facilisis, tempor ante quis, congue orci.\r\n\r\nOrci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras posuere ligula lectus, ut cursus mi venenatis nec. Nulla tortor mi, vestibulum in aliquet sed, semper quis lacus. Nulla interdum justo in fermentum faucibus. Donec euismod nisl ac venenatis dignissim. Nullam rhoncus eleifend nisi, sed posuere lectus aliquam non. Nam accumsan volutpat nisi, ac consectetur enim commodo non. Donec id lectus accumsan, pulvinar arcu eu, ornare eros. In suscipit nisl sit amet ligula convallis ullamcorper. Quisque sed interdum tellus. Proin luctus sem neque, et pulvinar neque malesuada sed.\r\n\r\nMorbi interdum eu lectus in tempor. Proin quis augue urna. Sed fringilla eget augue vitae aliquam. Pellentesque convallis tempus luctus. Integer vitae justo risus. Morbi lobortis sem ligula, vel imperdiet nibh blandit placerat. Morbi elementum enim orci, sagittis euismod risus scelerisque eu. Etiam sit amet neque vehicula, sollicitudin nisl at, rutrum nibh. Nunc odio tellus, hendrerit in risus eget, dignissim varius nulla. Sed id efficitur metus. Vivamus dolor magna, scelerisque in erat id, mattis dictum nunc. Vivamus hendrerit felis eu dolor convallis mollis.', '2017-11-23 18:55:06', '2017-11-23 12:54:00', '2017-11-24 13:54:00', '', '', '', 1),
(11, 'Prueba evento 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu scelerisque ipsum. Nullam leo velit, imperdiet at vehicula ut, placerat quis lectus. Cras nec ex interdum, tempus erat in, aliquet arcu. Cras vitae fermentum nisl, in bibendum arcu. Pellentesque quis nisl mollis, mattis enim ut, laoreet enim. Nulla enim nunc, laoreet eu elementum vel, malesuada et ligula. Sed vitae felis in nunc ultricies tempor vel vel ex. Proin hendrerit vestibulum maximus. Integer congue velit facilisis, tempor ante quis, congue orci.\r\n\r\nOrci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras posuere ligula lectus, ut cursus mi venenatis nec. Nulla tortor mi, vestibulum in aliquet sed, semper quis lacus. Nulla interdum justo in fermentum faucibus. Donec euismod nisl ac venenatis dignissim. Nullam rhoncus eleifend nisi, sed posuere lectus aliquam non. Nam accumsan volutpat nisi, ac consectetur enim commodo non. Donec id lectus accumsan, pulvinar arcu eu, ornare eros. In suscipit nisl sit amet ligula convallis ullamcorper. Quisque sed interdum tellus. Proin luctus sem neque, et pulvinar neque malesuada sed.\r\n\r\nMorbi interdum eu lectus in tempor. Proin quis augue urna. Sed fringilla eget augue vitae aliquam. Pellentesque convallis tempus luctus. Integer vitae justo risus. Morbi lobortis sem ligula, vel imperdiet nibh blandit placerat. Morbi elementum enim orci, sagittis euismod risus scelerisque eu. Etiam sit amet neque vehicula, sollicitudin nisl at, rutrum nibh. Nunc odio tellus, hendrerit in risus eget, dignissim varius nulla. Sed id efficitur metus. Vivamus dolor magna, scelerisque in erat id, mattis dictum nunc. Vivamus hendrerit felis eu dolor convallis mollis.\r\n\r\nAenean posuere vulputate massa id commodo. Curabitur molestie quam et nunc egestas egestas. Duis fermentum tellus ut quam lobortis semper. Pellentesque pharetra gravida lectus ac pellentesque. Morbi rhoncus, velit id varius malesuada, leo ligula vehicula metus, et luctus quam lectus sed neque. Cras tempor aliquet sem in iaculis. Donec et nibh vel est auctor elementum nec ac tellus. Vivamus placerat scelerisque leo sit amet maximus. In auctor in nulla rutrum ullamcorper. Vestibulum gravida augue sed massa feugiat egestas. Sed in mauris venenatis, venenatis erat nec, porttitor arcu. Nullam fringilla pretium cursus. Proin ut mi non felis aliquet rutrum sed vitae lorem. Maecenas ut lorem convallis, semper enim quis, gravida mauris. Suspendisse sed pharetra ex. Proin quis placerat sap', '2017-11-23 18:59:02', '2017-11-23 12:58:00', '2017-11-23 13:58:00', '', '', '', 1),
(12, 'Plática prueba', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu scelerisque ipsum. Nullam leo velit, imperdiet at vehicula ut, placerat quis lectus. Cras nec ex interdum, tempus erat in, aliquet arcu. Cras vitae fermentum nisl, in bibendum arcu. Pellentesque quis nisl mollis, mattis enim ut, laoreet enim. Nulla enim nunc, laoreet eu elementum vel, malesuada et ligula. Sed vitae felis in nunc ultricies tempor vel vel ex. Proin hendrerit vestibulum maximus. Integer congue velit facilisis, tempor ante quis, congue orci.\r\n\r\nOrci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras posuere ligula lectus, ut cursus mi venenatis nec. Nulla tortor mi, vestibulum in aliquet sed, semper quis lacus. Nulla interdum justo in fermentum faucibus. Donec euismod nisl ac venenatis dignissim. Nullam rhoncus eleifend nisi, sed posuere lectus aliquam non. Nam accumsan volutpat nisi, ac consectetur enim commodo non. Donec id lectus accumsan, pulvinar arcu eu, ornare eros. In suscipit nisl sit amet ligula convallis ullamcorper. Quisque sed interdum tellus. Proin luctus sem neque, et pulvinar neque malesuada sed.\r\n\r\nMorbi interdum eu lectus in tempor. Proin quis augue urna. Sed fringilla eget augue vitae aliquam. Pellentesque convallis tempus luctus. Integer vitae justo risus. Morbi lobortis sem ligula, vel imperdiet nibh blandit placerat. Morbi elementum enim orci, sagittis euismod risus scelerisque eu. Etiam sit amet neque vehicula, sollicitudin nisl at, rutrum nibh. Nunc odio tellus, hendrerit in risus eget, dignissim varius nulla. Sed id efficitur metus. Vivamus dolor magna, scelerisque in erat id, mattis dictum nunc. Vivamus hendrerit felis eu dolor convallis mollis.\r\n\r\nAenean posuere vulputate massa id commodo. Curabitur molestie quam et nunc egestas egestas. Duis fermentum tellus ut quam lobortis semper. Pellentesque pharetra gravida lectus ac pellentesque. Morbi rhoncus, velit id varius malesuada, leo ligula vehicula metus, et luctus quam lectus sed neque. Cras tempor aliquet sem in iaculis. Donec et nibh vel est auctor elementum nec ac tellus. Vivamus placerat scelerisque leo sit amet maximus. In auctor in nulla rutrum ullamcorper. Vestibulum gravida augue sed massa feugiat egestas. Sed in mauris venenatis, venenatis erat nec, porttitor arcu. Nullam fringilla pretium cursus. Proin ut mi non felis aliquet rutrum sed vitae lorem. Maecenas ut lorem convallis, semper enim quis, gravida mauris. Suspendisse sed pharetra ex. Proin quis placerat sap', '2017-11-23 18:59:51', '2017-11-23 15:59:00', '2017-11-28 17:59:00', '', '', '', 1),
(14, 'Prueba de evento', 'lalsdasdjalkdjalkdjalkd', '2017-11-29 04:20:45', '2017-11-28 22:20:00', '2017-11-29 22:20:00', '', '', 'imagen-de-prueba-320x240.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `imagenesarticulo`
--

CREATE TABLE `imagenesarticulo` (
  `idImagenesArticulos` int(11) NOT NULL,
  `rutaImagenes` varchar(300) NOT NULL,
  `idPublicacion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `imagenesarticulo`
--

INSERT INTO `imagenesarticulo` (`idImagenesArticulos`, `rutaImagenes`, `idPublicacion`) VALUES
(140, '1511221808-indio.jpg', 107),
(141, '1511221808-plaza-de-armas.jpg', 107),
(142, '1511221808-queretaro-centro.jpg', 107),
(143, '1511221932-queretaro-centro.jpg', 108),
(144, '1511221932-vista-centro.jpg', 108),
(145, '1511222098-rio.jpg', 109),
(146, '1511222336-peña-bernal.jpg', 110),
(147, '1511222336-rio.jpg', 110),
(148, '1511222644-indio.jpg', 111),
(149, '1511222644-queretaro-centro.jpg', 111),
(150, '1511222644-arcos.jpg', 111),
(151, '1511222731-arcos.jpg', 112),
(152, '1511222731-indio.jpg', 112),
(153, '1511222731-queretaro-centro.jpg', 112),
(154, '1512054629-img_9490.jpg', 106);

-- --------------------------------------------------------

--
-- Table structure for table `imagenesevento`
--

CREATE TABLE `imagenesevento` (
  `IdImagenesEventos` int(11) NOT NULL,
  `RutaImagenes` varchar(300) CHARACTER SET utf8 NOT NULL,
  `IdEvento` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `imagenesevento`
--

INSERT INTO `imagenesevento` (`IdImagenesEventos`, `RutaImagenes`, `IdEvento`) VALUES
(3, '1511928959-imagen-de-prueba-320x240.jpg', NULL),
(4, '1511929238-imagen-de-prueba-320x240.jpg', NULL),
(5, '1511929245-imagen-de-prueba-320x240.jpg', 14),
(6, '1511929802-Captura de pantalla 2017-02-12 a las 22.48.58.png', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `perteneceaevento`
--

CREATE TABLE `perteneceaevento` (
  `IdEvento` int(11) NOT NULL,
  `IdTipoEvento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `perteneceaevento`
--

INSERT INTO `perteneceaevento` (`IdEvento`, `IdTipoEvento`) VALUES
(9, 1),
(11, 1),
(11, 4),
(11, 5),
(12, 5),
(12, 2),
(14, 1);

-- --------------------------------------------------------

--
-- Table structure for table `perteneceapublicacion`
--

CREATE TABLE `perteneceapublicacion` (
  `IdTipoPublicacion` int(11) NOT NULL,
  `IdPublicacion` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `privilegio`
--

CREATE TABLE `privilegio` (
  `IdPrivilegio` int(8) NOT NULL,
  `NombrePrivilegio` varchar(50) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `DescripcionPrivilegio` varchar(250) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `privilegio`
--

INSERT INTO `privilegio` (`IdPrivilegio`, `NombrePrivilegio`, `DescripcionPrivilegio`) VALUES
(1, 'Registrar Articulo', 'Registro de artículos nuevos.'),
(2, 'Modificar Articulo', 'Modificación de artículos existentes.'),
(3, 'Eliminar Articulo', 'Eliminación de artículos existentes.'),
(4, 'Registrar Evento', 'Registro de eventos nuevos.'),
(5, 'Modificar Evento', 'Modificación de eventos existentes.'),
(6, 'Eliminar Evento', 'Eliminación de eventos existentes.'),
(7, 'Ver Usuarios', 'Visualición de usuarios existente en el sistema.'),
(8, 'Registrar Rol', 'Registro de rol con privilegios para usuarios.'),
(9, 'Modificar Rol', 'Modificación de nombre y privilegios de roles.'),
(10, 'Eliminar Rol', 'Eliminación de roles.'),
(11, 'Asignar Rol', 'Asignación de rol a un usuario.'),
(12, 'Modificar Privilegios', 'Modificación de privilegios de roles.'),
(13, 'Otorgar Privilegios', 'Otorgar privilegios a un rol.'),
(14, 'Registrar Empleado', 'Registro de usuarios con el rol de empleado en el sistema.'),
(15, 'Modificar Empleado', 'Modificar empleados en el sistema.\r\n'),
(16, 'Eliminar Empleado', 'Eliminar empleados del sistema.'),
(17, 'Registrar Ayudante', 'Registrar de usuarios con el rol de ayudante en el sistema.'),
(18, 'Modificar Ayudante', 'Modificar ayudantes de empleados en el sistema.'),
(19, 'Eliminar Ayudante', 'Eliminar ayudantes de empleados del sistema.'),
(20, 'Consultar Bitacora Empleado', 'Consultar Bitácora de modificaciones que han realizado los empleados.'),
(21, 'Consultar Bitacora Articulo', 'Consultar Bitácora de modificaciones que han sido realizadas a un artículo.'),
(22, 'Crear borrador.', 'Crear artículos nuevos como borrador.'),
(23, 'Cambiar publicación a borrador.', 'Cambiar un artículo publicado a borrador.'),
(25, 'Registrar Departamento', 'Registro de departamentos nuevos.'),
(26, 'Modificar Departamento', 'Modificación de departamentos existentes.'),
(27, 'Eliminar Departamento', 'Eliminación de departamentos existentes.'),
(28, 'Modificar Página Principal', 'Modificar textos e imagenes de la pagina principal'),
(29, 'Editar descripción subcategorias', 'Este privilegio permite al usuario editar la descripción de las subcategorias.');

-- --------------------------------------------------------

--
-- Table structure for table `publicacion`
--

CREATE TABLE `publicacion` (
  `IdPublicacion` int(11) NOT NULL,
  `FechaPublicacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `TituloPublicacion` varchar(200) COLLATE utf8_bin NOT NULL,
  `DescripcionPublicacion` text COLLATE utf8_bin NOT NULL,
  `RutaImagenesPublicacion` varchar(300) COLLATE utf8_bin NOT NULL DEFAULT 'arcos.jpg',
  `RutaAdjuntosPublicacion` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `URLVideoPublicacion` varchar(1000) COLLATE utf8_bin DEFAULT NULL,
  `FijoPublicacion` tinyint(1) NOT NULL,
  `FechaFinFijoPublicacion` datetime DEFAULT NULL,
  `PublicarDespues` tinyint(1) NOT NULL,
  `FechaPublicacionProgramada` datetime DEFAULT NULL,
  `Borrador` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `publicacion`
--

INSERT INTO `publicacion` (`IdPublicacion`, `FechaPublicacion`, `TituloPublicacion`, `DescripcionPublicacion`, `RutaImagenesPublicacion`, `RutaAdjuntosPublicacion`, `URLVideoPublicacion`, `FijoPublicacion`, `FechaFinFijoPublicacion`, `PublicarDespues`, `FechaPublicacionProgramada`, `Borrador`) VALUES
(106, '2017-11-20 23:44:29', 'Noticia 4', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis diam. Pellentesque ut neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In dui magna, posuere eget, vestibulum et, tempor auctor, justo. In ac felis quis tortor malesuada pretium. Pellentesque auctor neque nec urna. Proin sapien ipsum, porta a, auctor quis, euismod ut, mi. Aenean viverra rhoncus pede. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Phasellus a est. Phasellus magna. In hac habitasse platea dictumst. Curabitur at lacus ac velit ornare lobortis. Curabitur a felis in nunc fringilla tristique. Morbi mattis ullamcorper velit. Phasellus gravida semper nisi. Nullam vel sem. Pellentesque libero tortor, tincidunt et, tincidunt eget, semper nec, quam. Sed hendrerit. Morbi ac felis. Nunc egestas, augue at pellentesque laoreet, felis eros vehicula leo, at malesuada velit leo quis pede. Donec interdum, metus et hendrerit aliquet, dolor diam sagittis ligula, eget egestas libero turpis vel mi. Nunc nulla. Fusce risus nisl, viverra et, tempor et, pretium in, sapien. Donec venenatis vulputate lorem. Morbi nec metus. Phasellus blandit leo ut odio. Maecenas ullamcorper, dui et placerat feugiat, eros pede varius nisi, condimentum viverra felis nunc et lorem. Sed magna purus, fermentum eu, tincidunt eu, varius ut, felis. In auctor lobortis lacus. Quisque libero metus, condimentum nec, tempor a, commodo mollis, magna. Vestibulum ullamcorper mauris at ligula. Fusce fermentum. Nullam cursus lacinia erat. Praesent blandit laoreet nibh. Fusce convallis metus id felis luctus adipiscing. Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est. Quisque id mi. Ut tincidunt tincidunt erat. Etiam feugiat lorem non metus. Vestibulum dapibus nunc ac augue. Curabitur vestibulum aliquam leo. Praesent egestas neque eu enim. In hac habitasse platea dictumst. Fusce a quam. Etiam ut purus mattis mauris sodales aliquam. Curabitur nisi. Quisque malesuada placerat nisl. Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus. Sed augue ipsum, egestas nec, vestibulum et, malesuada adipiscing, dui. Vestibulum facilisis, purus nec pulvinar iaculis, ligula mi congue nunc, vitae euismod ligula urna in dolor. Mauris sollicitudin fermentum libero. Praesent nonummy mi in odio. Nunc interdum lacus sit amet orci. Vestibulum rutrum, mi nec elementum vehicula, eros quam gravida nisl, id fringilla neque ante vel mi. Morbi mollis tellus ac sapien. Phasellus volutpat, metus eget egestas mollis, lacus lacus blandit dui, id egestas quam mauris ut lacus. Fusce vel dui. Sed in libero ut nibh placerat accumsan. Proin faucibus arcu quis ante. In consectetuer turpis ut velit. Nulla sit amet est. Praesent metus tellus, elementum eu, semper a, adipiscing nec, purus. Cras risus ipsum, faucibus ut, ullamcorper id, varius ac, leo. Suspendisse feugiat. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Praesent nec nisl a purus blandit viverra. Praesent ac massa at ligula laoreet iaculis. Nulla neque dolor, sagittis eget, iaculis quis, molestie non, velit. Mauris turpis nunc, blandit et, volutpat molestie, porta ut, ligula. Fusce pharetra convallis urna. Quisque ut nisi. Donec mi odio, faucibus at, scelerisque quis, convallis in, nisi. Suspendisse non nisl sit amet velit hendrerit rutrum. Ut leo. Ut a nisl id ante tempus hendrerit. Proin pretium, leo ac pellentesque mollis, felis nunc ultrices eros, sed gravida augue augue mollis justo. Suspendisse eu ligula. Nulla facilisi. Donec id justo. Praesent porttitor, nulla vitae posuere iaculis, arcu nisl dignissim dolor, a pretium mi sem ut ipsum. Curabitur suscipit suscipit tellus. Praesent vestibulum dapibus nibh. Etiam iaculis nunc ac metus. Ut id nisl quis enim dignissim sagittis. Etiam sollicitudin, ipsum eu pulvinar rutrum, tellus ipsum laoreet sapien, quis venenatis ante odio sit amet eros. Proin magna. Duis vel nibh at velit scelerisque suscipit. Curabitur turpis. Vestibulum suscipit nulla quis orci. Fusce ac felis sit amet ligula pharetra condimentum. Maecenas egestas arcu quis ligula mattis placerat. Duis lobortis massa imperdiet quam. Suspendisse potenti. Pellentesque commodo eros a enim. Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl. Sed libero. Aliquam erat volutpat. Etiam vitae tortor. Morbi vestibulum volutpat enim. Aliquam eu nunc. Nunc sed turpis. Sed mollis, eros et ultrices tempus, mauris ipsum aliquam libero, non adipiscing dolor urna a orci. Nulla porta dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Pellentesque dapibus hendrerit tortor. Praesent egestas tristique nibh. Sed a libero. Cras varius. Donec vitae orci sed dolor rutrum auctor. Fusce egestas elit eget lorem. Suspendisse nisl elit, rhoncus eget, elementum ac, condimentum eget, diam. Nam at tortor in tellus interdum sagittis. Aliquam lobortis. Donec orci lectus, aliquam ut, faucibus non, euismod id, nulla. Curabitur blandit mollis lacus. Nam adipiscing. Vestibulum eu odio. Vivamus laoreet. Nullam tincidunt adipiscing enim. Phasellus tempus. Proin viverra, ligula sit amet ultrices semper, ligula arcu tristique sapien, a accumsan nisi mauris ac eros. Fusce neque. Suspendisse faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae iaculis lacus elit id tortor. Vivamus aliquet elit ac nisl. Fusce fermentum odio nec arcu. Vivamus euismod mauris. In ut quam vitae odio lacinia tincidunt. Praesent ut ligula non mi varius sagittis. Cras sagittis. Praesent ac sem eget est egestas volutpat. Vivamus consectetuer hendrerit lacus. Cras non dolor. Vivamus in erat ut urna cursus vestibulum. Fusce commodo aliquam arcu. Nam commodo suscipit quam. Quisque id odio. Praesent venenatis metus at tortor pulvinar varius. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vita', 'img_9490.jpg', 'Ejercicio 1 - Caso farmacéutica.pdf', 'https://www.youtube.com/watch?v=UaJSlo23_TQ', 0, '1970-01-01 00:00:00', 0, '1970-01-01 00:00:00', 0),
(107, '2017-11-20 23:50:08', 'Noticia 3', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis diam. Pellentesque ut neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In dui magna, posuere eget, vestibulum et, tempor auctor, justo. In ac felis quis tortor malesuada pretium. Pellentesque auctor neque nec urna. Proin sapien ipsum, porta a, auctor quis, euismod ut, mi. Aenean viverra rhoncus pede. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Phasellus a est. Phasellus magna. In hac habitasse platea dictumst. Curabitur at lacus ac velit ornare lobortis. Curabitur a felis in nunc fringilla tristique. Morbi mattis ullamcorper velit. Phasellus gravida semper nisi. Nullam vel sem. Pellentesque libero tortor, tincidunt et, tincidunt eget, semper nec, quam. Sed hendrerit. Morbi ac felis. Nunc egestas, augue at pellentesque laoreet, felis eros vehicula leo, at malesuada velit leo quis pede. Donec interdum, metus et hendrerit aliquet, dolor diam sagittis ligula, eget egestas libero turpis vel mi. Nunc nulla. Fusce risus nisl, viverra et, tempor et, pretium in, sapien. Donec venenatis vulputate lorem. Morbi nec metus. Phasellus blandit leo ut odio. Maecenas ullamcorper, dui et placerat feugiat, eros pede varius nisi, condimentum viverra felis nunc et lorem. Sed magna purus, fermentum eu, tincidunt eu, varius ut, felis. In auctor lobortis lacus. Quisque libero metus, condimentum nec, tempor a, commodo mollis, magna. Vestibulum ullamcorper mauris at ligula. Fusce fermentum. Nullam cursus lacinia erat. Praesent blandit laoreet nibh. Fusce convallis metus id felis luctus adipiscing. Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est. Quisque id mi. Ut tincidunt tincidunt erat. Etiam feugiat lorem non metus. Vestibulum dapibus nunc ac augue. Curabitur vestibulum aliquam leo. Praesent egestas neque eu enim. In hac habitasse platea dictumst. Fusce a quam. Etiam ut purus mattis mauris sodales aliquam. Curabitur nisi. Quisque malesuada placerat nisl. Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus. Sed augue ipsum, egestas nec, vestibulum et, malesuada adipiscing, dui. Vestibulum facilisis, purus nec pulvinar iaculis, ligula mi congue nunc, vitae euismod ligula urna in dolor. Mauris sollicitudin fermentum libero. Praesent nonummy mi in odio. Nunc interdum lacus sit amet orci. Vestibulum rutrum, mi nec elementum vehicula, eros quam gravida nisl, id fringilla neque ante vel mi. Morbi mollis tellus ac sapien. Phasellus volutpat, metus eget egestas mollis, lacus lacus blandit dui, id egestas quam mauris ut lacus. Fusce vel dui. Sed in libero ut nibh placerat accumsan. Proin faucibus arcu quis ante. In consectetuer turpis ut velit. Nulla sit amet est. Praesent metus tellus, elementum eu, semper a, adipiscing nec, purus. Cras risus ipsum, faucibus ut, ullamcorper id, varius ac, leo. Suspendisse feugiat. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Praesent nec nisl a purus blandit viverra. Praesent ac massa at ligula laoreet iaculis. Nulla neque dolor, sagittis eget, iaculis quis, molestie non, velit. Mauris turpis nunc, blandit et, volutpat molestie, porta ut, ligula. Fusce pharetra convallis urna. Quisque ut nisi. Donec mi odio, faucibus at, scelerisque quis, convallis in, nisi. Suspendisse non nisl sit amet velit hendrerit rutrum. Ut leo. Ut a nisl id ante tempus hendrerit. Proin pretium, leo ac pellentesque mollis, felis nunc ultrices eros, sed gravida augue augue mollis justo. Suspendisse eu ligula. Nulla facilisi. Donec id justo. Praesent porttitor, nulla vitae posuere iaculis, arcu nisl dignissim dolor, a pretium mi sem ut ipsum. Curabitur suscipit suscipit tellus. Praesent vestibulum dapibus nibh. Etiam iaculis nunc ac metus. Ut id nisl quis enim dignissim sagittis. Etiam sollicitudin, ipsum eu pulvinar rutrum, tellus ipsum laoreet sapien, quis venenatis ante odio sit amet eros. Proin magna. Duis vel nibh at velit scelerisque suscipit. Curabitur turpis. Vestibulum suscipit nulla quis orci. Fusce ac felis sit amet ligula pharetra condimentum. Maecenas egestas arcu quis ligula mattis placerat. Duis lobortis massa imperdiet quam. Suspendisse potenti. Pellentesque commodo eros a enim. Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl. Sed libero. Aliquam erat volutpat. Etiam vitae tortor. Morbi vestibulum volutpat enim. Aliquam eu nunc. Nunc sed turpis. Sed mollis, eros et ultrices tempus, mauris ipsum aliquam libero, non adipiscing dolor urna a orci. Nulla porta dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Pellentesque dapibus hendrerit tortor. Praesent egestas tristique nibh. Sed a libero. Cras varius. Donec vitae orci sed dolor rutrum auctor. Fusce egestas elit eget lorem. Suspendisse nisl elit, rhoncus eget, elementum ac, condimentum eget, diam. Nam at tortor in tellus interdum sagittis. Aliquam lobortis. Donec orci lectus, aliquam ut, faucibus non, euismod id, nulla. Curabitur blandit mollis lacus. Nam adipiscing. Vestibulum eu odio. Vivamus laoreet. Nullam tincidunt adipiscing enim. Phasellus tempus. Proin viverra, ligula sit amet ultrices semper, ligula arcu tristique sapien, a accumsan nisi mauris ac eros. Fusce neque. Suspendisse faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae iaculis lacus elit id tortor. Vivamus aliquet elit ac nisl. Fusce fermentum odio nec arcu. Vivamus euismod mauris. In ut quam vitae odio lacinia tincidunt. Praesent ut ligula non mi varius sagittis. Cras sagittis. Praesent ac sem eget est egestas volutpat. Vivamus consectetuer hendrerit lacus. Cras non dolor. Vivamus in erat ut urna cursus vestibulum. Fusce commodo aliquam arcu. Nam commodo suscipit quam. Quisque id odio. Praesent venenatis metus at tortor pulvinar varius. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vit', 'indio.jpg, plaza-de-armas.jpg, queretaro-centro.jpg', 'Ejercicio 1 - Caso farmacéutica.pdf', 'https://www.youtube.com/watch?v=2h7Dy7O2brs', 0, '1970-01-01 00:00:00', 0, '1970-01-01 00:00:00', 0),
(108, '2017-11-20 23:52:12', 'Noticia 2', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis diam. Pellentesque ut neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In dui magna, posuere eget, vestibulum et, tempor auctor, justo. In ac felis quis tortor malesuada pretium. Pellentesque auctor neque nec urna. Proin sapien ipsum, porta a, auctor quis, euismod ut, mi. Aenean viverra rhoncus pede. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Phasellus a est. Phasellus magna. In hac habitasse platea dictumst. Curabitur at lacus ac velit ornare lobortis. Curabitur a felis in nunc fringilla tristique. Morbi mattis ullamcorper velit. Phasellus gravida semper nisi. Nullam vel sem. Pellentesque libero tortor, tincidunt et, tincidunt eget, semper nec, quam. Sed hendrerit. Morbi ac felis. Nunc egestas, augue at pellentesque laoreet, felis eros vehicula leo, at malesuada velit leo quis pede. Donec interdum, metus et hendrerit aliquet, dolor diam sagittis ligula, eget egestas libero turpis vel mi. Nunc nulla. Fusce risus nisl, viverra et, tempor et, pretium in, sapien. Donec venenatis vulputate lorem. Morbi nec metus. Phasellus blandit leo ut odio. Maecenas ullamcorper, dui et placerat feugiat, eros pede varius nisi, condimentum viverra felis nunc et lorem. Sed magna purus, fermentum eu, tincidunt eu, varius ut, felis. In auctor lobortis lacus. Quisque libero metus, condimentum nec, tempor a, commodo mollis, magna. Vestibulum ullamcorper mauris at ligula. Fusce fermentum. Nullam cursus lacinia erat. Praesent blandit laoreet nibh. Fusce convallis metus id felis luctus adipiscing. Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est. Quisque id mi. Ut tincidunt tincidunt erat. Etiam feugiat lorem non metus. Vestibulum dapibus nunc ac augue. Curabitur vestibulum aliquam leo. Praesent egestas neque eu enim. In hac habitasse platea dictumst. Fusce a quam. Etiam ut purus mattis mauris sodales aliquam. Curabitur nisi. Quisque malesuada placerat nisl. Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus. Sed augue ipsum, egestas nec, vestibulum et, malesuada adipiscing, dui. Vestibulum facilisis, purus nec pulvinar iaculis, ligula mi congue nunc, vitae euismod ligula urna in dolor. Mauris sollicitudin fermentum libero. Praesent nonummy mi in odio. Nunc interdum lacus sit amet orci. Vestibulum rutrum, mi nec elementum vehicula, eros quam gravida nisl, id fringilla neque ante vel mi. Morbi mollis tellus ac sapien. Phasellus volutpat, metus eget egestas mollis, lacus lacus blandit dui, id egestas quam mauris ut lacus. Fusce vel dui. Sed in libero ut nibh placerat accumsan. Proin faucibus arcu quis ante. In consectetuer turpis ut velit. Nulla sit amet est. Praesent metus tellus, elementum eu, semper a, adipiscing nec, purus. Cras risus ipsum, faucibus ut, ullamcorper id, varius ac, leo. Suspendisse feugiat. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Praesent nec nisl a purus blandit viverra. Praesent ac massa at ligula laoreet iaculis. Nulla neque dolor, sagittis eget, iaculis quis, molestie non, velit. Mauris turpis nunc, blandit et, volutpat molestie, porta ut, ligula. Fusce pharetra convallis urna. Quisque ut nisi. Donec mi odio, faucibus at, scelerisque quis, convallis in, nisi. Suspendisse non nisl sit amet velit hendrerit rutrum. Ut leo. Ut a nisl id ante tempus hendrerit. Proin pretium, leo ac pellentesque mollis, felis nunc ultrices eros, sed gravida augue augue mollis justo. Suspendisse eu ligula. Nulla facilisi. Donec id justo. Praesent porttitor, nulla vitae posuere iaculis, arcu nisl dignissim dolor, a pretium mi sem ut ipsum. Curabitur suscipit suscipit tellus. Praesent vestibulum dapibus nibh. Etiam iaculis nunc ac metus. Ut id nisl quis enim dignissim sagittis. Etiam sollicitudin, ipsum eu pulvinar rutrum, tellus ipsum laoreet sapien, quis venenatis ante odio sit amet eros. Proin magna. Duis vel nibh at velit scelerisque suscipit. Curabitur turpis. Vestibulum suscipit nulla quis orci. Fusce ac felis sit amet ligula pharetra condimentum. Maecenas egestas arcu quis ligula mattis placerat. Duis lobortis massa imperdiet quam. Suspendisse potenti. Pellentesque commodo eros a enim. Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl. Sed libero. Aliquam erat volutpat. Etiam vitae tortor. Morbi vestibulum volutpat enim. Aliquam eu nunc. Nunc sed turpis. Sed mollis, eros et ultrices tempus, mauris ipsum aliquam libero, non adipiscing dolor urna a orci. Nulla porta dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Pellentesque dapibus hendrerit tortor. Praesent egestas tristique nibh. Sed a libero. Cras varius. Donec vitae orci sed dolor rutrum auctor. Fusce egestas elit eget lorem. Suspendisse nisl elit, rhoncus eget, elementum ac, condimentum eget, diam. Nam at tortor in tellus interdum sagittis. Aliquam lobortis. Donec orci lectus, aliquam ut, faucibus non, euismod id, nulla. Curabitur blandit mollis lacus. Nam adipiscing. Vestibulum eu odio. Vivamus laoreet. Nullam tincidunt adipiscing enim. Phasellus tempus. Proin viverra, ligula sit amet ultrices semper, ligula arcu tristique sapien, a accumsan nisi mauris ac eros. Fusce neque. Suspendisse faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae iaculis lacus elit id tortor. Vivamus aliquet elit ac nisl. Fusce fermentum odio nec arcu. Vivamus euismod mauris. In ut quam vitae odio lacinia tincidunt. Praesent ut ligula non mi varius sagittis. Cras sagittis. Praesent ac sem eget est egestas volutpat. Vivamus consectetuer hendrerit lacus. Cras non dolor. Vivamus in erat ut urna cursus vestibulum. Fusce commodo aliquam arcu. Nam commodo suscipit quam. Quisque id odio. Praesent venenatis metus at tortor pulvinar varius. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vit', 'queretaro-centro.jpg, vista-centro.jpg', 'Ejercicio 1 - Caso farmacéutica.pdf', 'https://www.youtube.com/watch?v=Dtw2vfKihXA', 0, '1970-01-01 01:00:00', 0, '1970-01-01 01:00:00', 0),
(109, '2017-11-20 23:54:58', '20Noticia 1', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis \r\n\r\nnatoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis diam. Pellentesque ut neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In dui magna, posuere eget, vestibulum et, tempor auctor, justo. In ac felis quis tortor malesuada pretium. Pellentesque auctor neque nec urna. Proin sapien ipsum, porta a, auctor quis, euismod ut, mi. Aenean viverra rhoncus pede. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Phasellus a est. Phasellus magna. In hac habitasse platea dictumst. Curabitur at lacus ac velit ornare lobortis. Curabitur a felis in nunc fringilla tristique. Morbi mattis ullamcorper velit. Phasellus gravida semper nisi. Nullam vel sem. Pellentesque libero tortor, tincidunt et, tincidunt eget, semper nec, quam. Sed hendrerit. Morbi ac felis. Nunc egestas, augue at pellentesque laoreet, felis eros vehicula leo, at malesuada velit leo quis pede. Donec interdum, metus et hendrerit aliquet, dolor diam sagittis ligula, eget egestas libero turpis vel mi. Nunc nulla. Fusce risus nisl, viverra et, tempor et, pretium in, sapien. Donec venenatis vulputate lorem. Morbi nec metus. Phasellus blandit leo ut odio. Maecenas ullamcorper, dui et placerat feugiat, eros pede varius nisi, condimentum viverra felis nunc et lorem. Sed magna purus, fermentum eu, tincidunt eu, varius ut, felis. In auctor lobortis lacus. Quisque libero metus, condimentum nec, tempor a, commodo mollis, magna. Vestibulum ullamcorper mauris at ligula. Fusce fermentum. Nullam cursus lacinia erat. Praesent blandit laoreet nibh. Fusce convallis metus id felis luctus adipiscing. Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est. Quisque id mi. Ut tincidunt tincidunt erat. Etiam feugiat lorem non metus. Vestibulum dapibus nunc ac augue. Curabitur vestibulum aliquam leo. Praesent egestas neque eu enim. In hac habitasse platea dictumst. Fusce a quam. Etiam ut purus mattis mauris sodales aliquam. Curabitur nisi. Quisque malesuada placerat nisl. Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus. Sed augue ipsum, egestas nec, vestibulum et, malesuada adipiscing, dui. Vestibulum facilisis, purus nec pulvinar iaculis, ligula mi congue nunc, vitae euismod ligula urna in dolor. Mauris sollicitudin fermentum libero. Praesent nonummy mi in odio. Nunc interdum lacus sit amet orci. Vestibulum rutrum, mi nec elementum vehicula, eros quam gravida nisl, id fringilla neque ante vel mi. Morbi mollis tellus ac sapien. Phasellus volutpat, metus eget egestas mollis, lacus lacus blandit dui, id egestas quam mauris ut lacus. Fusce vel dui. Sed in libero ut nibh placerat accumsan. Proin faucibus arcu quis ante. In consectetuer turpis ut velit. Nulla sit amet est. Praesent metus tellus, elementum eu, semper a, adipiscing nec, purus. Cras risus ipsum, faucibus ut, ullamcorper id, varius ac, leo. Suspendisse feugiat. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Praesent nec nisl a purus blandit viverra. Praesent ac massa at ligula laoreet iaculis. Nulla neque dolor, sagittis eget, iaculis quis, molestie non, velit. Mauris turpis nunc, blandit et, volutpat molestie, porta ut, ligula. Fusce pharetra convallis urna. Quisque ut nisi. Donec mi odio, faucibus at, scelerisque quis, convallis in, nisi. Suspendisse non nisl sit amet velit hendrerit rutrum. Ut leo. Ut a nisl id ante tempus hendrerit. Proin pretium, leo ac pellentesque mollis, felis nunc ultrices eros, sed gravida augue augue mollis justo. Suspendisse eu ligula. Nulla facilisi. Donec id justo. Praesent porttitor, nulla vitae posuere iaculis, arcu nisl dignissim dolor, a pretium mi sem ut ipsum. Curabitur suscipit suscipit tellus. Praesent vestibulum dapibus nibh. Etiam iaculis nunc ac metus. Ut id nisl quis enim dignissim sagittis. Etiam sollicitudin, ipsum eu pulvinar rutrum, tellus ipsum laoreet sapien, quis venenatis ante odio sit amet eros. Proin magna. Duis vel nibh at velit scelerisque suscipit. Curabitur turpis. Vestibulum suscipit nulla quis orci. Fusce ac felis sit amet ligula pharetra condimentum. Maecenas egestas arcu quis ligula mattis placerat. Duis lobortis massa imperdiet quam. Suspendisse potenti. Pellentesque commodo eros a enim. Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl. Sed libero. Aliquam erat volutpat. Etiam vitae tortor. Morbi vestibulum volutpat enim. Aliquam eu nunc. Nunc sed turpis. Sed mollis, eros et ultrices tempus, mauris ipsum aliquam libero, non adipiscing dolor urna a orci. Nulla porta dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Pellentesque dapibus hendrerit tortor. Praesent egestas tristique nibh. Sed a libero. Cras varius. Donec vitae orci sed dolor rutrum auctor. Fusce egestas elit eget lorem. Suspendisse nisl elit, rhoncus eget, elementum ac, condimentum eget, diam. Nam at tortor in tellus interdum sagittis. Aliquam lobortis. Donec orci lectus, aliquam ut, faucibus non, euismod id, nulla. Curabitur blandit mollis lacus. Nam adipiscing. Vestibulum eu odio. Vivamus laoreet. Nullam tincidunt adipiscing enim. Phasellus tempus. Proin viverra, ligula sit amet ultrices semper, ligula arcu tristique sapien, a accumsan nisi mauris ac eros. Fusce neque. Suspendisse faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae iaculis lacus elit id tortor. Vivamus aliquet elit ac nisl. Fusce fermentum odio nec arcu. Vivamus euismod mauris. In ut quam vitae odio lacinia tincidunt. Praesent ut ligula non mi varius sagittis. Cras sagittis. Praesent ac sem eget est egestas volutpat. Vivamus consectetuer hendrerit lacus. Cras non dolor. Vivamus in erat ut urna cursus vestibulum. Fusce commodo aliquam arcu. Nam commodo suscipit quam. Quisque id odio. Praesent venenatis metus at tortor pulvinar varius. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vit', 'rio.jpg', 'Ejercicio 1 - Caso farmacéutica.pdf', '', 0, '1970-01-01 00:00:00', 0, '1970-01-01 00:00:00', 0);
INSERT INTO `publicacion` (`IdPublicacion`, `FechaPublicacion`, `TituloPublicacion`, `DescripcionPublicacion`, `RutaImagenesPublicacion`, `RutaAdjuntosPublicacion`, `URLVideoPublicacion`, `FijoPublicacion`, `FechaFinFijoPublicacion`, `PublicarDespues`, `FechaPublicacionProgramada`, `Borrador`) VALUES
(110, '2017-11-20 23:58:56', 'Notica', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis diam. Pellentesque ut neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In dui magna, posuere eget, vestibulum et, tempor auctor, justo. In ac felis quis tortor malesuada pretium. Pellentesque auctor neque nec urna. Proin sapien ipsum, porta a, auctor quis, euismod ut, mi. Aenean viverra rhoncus pede. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Phasellus a est. Phasellus magna. In hac habitasse platea dictumst. Curabitur at lacus ac velit ornare lobortis. Curabitur a felis in nunc fringilla tristique. Morbi mattis ullamcorper velit. Phasellus gravida semper nisi. Nullam vel sem. Pellentesque libero tortor, tincidunt et, tincidunt eget, semper nec, quam. Sed hendrerit. Morbi ac felis. Nunc egestas, augue at pellentesque laoreet, felis eros vehicula leo, at malesuada velit leo quis pede. Donec interdum, metus et hendrerit aliquet, dolor diam sagittis ligula, eget egestas libero turpis vel mi. Nunc nulla. Fusce risus nisl, viverra et, tempor et, pretium in, sapien. Donec venenatis vulputate lorem. Morbi nec metus. Phasellus blandit leo ut odio. Maecenas ullamcorper, dui et placerat feugiat, eros pede varius nisi, condimentum viverra felis nunc et lorem. Sed magna purus, fermentum eu, tincidunt eu, varius ut, felis. In auctor lobortis lacus. Quisque libero metus, condimentum nec, tempor a, commodo mollis, magna. Vestibulum ullamcorper mauris at ligula. Fusce fermentum. Nullam cursus lacinia erat. Praesent blandit laoreet nibh. Fusce convallis metus id felis luctus adipiscing. Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est. Quisque id mi. Ut tincidunt tincidunt erat. Etiam feugiat lorem non metus. Vestibulum dapibus nunc ac augue. Curabitur vestibulum aliquam leo. Praesent egestas neque eu enim. In hac habitasse platea dictumst. Fusce a quam. Etiam ut purus mattis mauris sodales aliquam. Curabitur nisi. Quisque malesuada placerat nisl. Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus. Sed augue ipsum, egestas nec, vestibulum et, malesuada adipiscing, dui. Vestibulum facilisis, purus nec pulvinar iaculis, ligula mi congue nunc, vitae euismod ligula urna in dolor. Mauris sollicitudin fermentum libero. Praesent nonummy mi in odio. Nunc interdum lacus sit amet orci. Vestibulum rutrum, mi nec elementum vehicula, eros quam gravida nisl, id fringilla neque ante vel mi. Morbi mollis tellus ac sapien. Phasellus volutpat, metus eget egestas mollis, lacus lacus blandit dui, id egestas quam mauris ut lacus. Fusce vel dui. Sed in libero ut nibh placerat accumsan. Proin faucibus arcu quis ante. In consectetuer turpis ut velit. Nulla sit amet est. Praesent metus tellus, elementum eu, semper a, adipiscing nec, purus. Cras risus ipsum, faucibus ut, ullamcorper id, varius ac, leo. Suspendisse feugiat. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Praesent nec nisl a purus blandit viverra. Praesent ac massa at ligula laoreet iaculis. Nulla neque dolor, sagittis eget, iaculis quis, molestie non, velit. Mauris turpis nunc, blandit et, volutpat molestie, porta ut, ligula. Fusce pharetra convallis urna. Quisque ut nisi. Donec mi odio, faucibus at, scelerisque quis, convallis in, nisi. Suspendisse non nisl sit amet velit hendrerit rutrum. Ut leo. Ut a nisl id ante tempus hendrerit. Proin pretium, leo ac pellentesque mollis, felis nunc ultrices eros, sed gravida augue augue mollis justo. Suspendisse eu ligula. Nulla facilisi. Donec id justo. Praesent porttitor, nulla vitae posuere iaculis, arcu nisl dignissim dolor, a pretium mi sem ut ipsum. Curabitur suscipit suscipit tellus. Praesent vestibulum dapibus nibh. Etiam iaculis nunc ac metus. Ut id nisl quis enim dignissim sagittis. Etiam sollicitudin, ipsum eu pulvinar rutrum, tellus ipsum laoreet sapien, quis venenatis ante odio sit amet eros. Proin magna. Duis vel nibh at velit scelerisque suscipit. Curabitur turpis. Vestibulum suscipit nulla quis orci. Fusce ac felis sit amet ligula pharetra condimentum. Maecenas egestas arcu quis ligula mattis placerat. Duis lobortis massa imperdiet quam. Suspendisse potenti. Pellentesque commodo eros a enim. Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl. Sed libero. Aliquam erat volutpat. Etiam vitae tortor. Morbi vestibulum volutpat enim. Aliquam eu nunc. Nunc sed turpis. Sed mollis, eros et ultrices tempus, mauris ipsum aliquam libero, non adipiscing dolor urna a orci. Nulla porta dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Pellentesque dapibus hendrerit tortor. Praesent egestas tristique nibh. Sed a libero. Cras varius. Donec vitae orci sed dolor rutrum auctor. Fusce egestas elit eget lorem. Suspendisse nisl elit, rhoncus eget, elementum ac, condimentum eget, diam. Nam at tortor in tellus interdum sagittis. Aliquam lobortis. Donec orci lectus, aliquam ut, faucibus non, euismod id, nulla. Curabitur blandit mollis lacus. Nam adipiscing. Vestibulum eu odio. Vivamus laoreet. Nullam tincidunt adipiscing enim. Phasellus tempus. Proin viverra, ligula sit amet ultrices semper, ligula arcu tristique sapien, a accumsan nisi mauris ac eros. Fusce neque. Suspendisse faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae iaculis lacus elit id tortor. Vivamus aliquet elit ac nisl. Fusce fermentum odio nec arcu. Vivamus euismod mauris. In ut quam vitae odio lacinia tincidunt. Praesent ut ligula non mi varius sagittis. Cras sagittis. Praesent ac sem eget est egestas volutpat. Vivamus consectetuer hendrerit lacus. Cras non dolor. Vivamus in erat ut urna cursus vestibulum. Fusce commodo aliquam arcu. Nam commodo suscipit quam. Quisque id odio. Praesent venenatis metus at tortor pulvinar varius. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vita', 'peña-bernal.jpg, rio.jpg', 'Ejercicio 1 - Caso farmacéutica.pdf', 'https://www.youtube.com/watch?v=zrHlFPRI_0c', 0, '1970-01-01 01:00:00', 0, '1970-01-01 01:00:00', 0),
(111, '2017-11-21 00:04:04', 'Noticia nueva', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis diam. Pellentesque ut neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In dui magna, posuere eget, vestibulum et, tempor auctor, justo. In ac felis quis tortor malesuada pretium. Pellentesque auctor neque nec urna. Proin sapien ipsum, porta a, auctor quis, euismod ut, mi. Aenean viverra rhoncus pede. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Phasellus a est. Phasellus magna. In hac habitasse platea dictumst. Curabitur at lacus ac velit ornare lobortis. Curabitur a felis in nunc fringilla tristique. Morbi mattis ullamcorper velit. Phasellus gravida semper nisi. Nullam vel sem. Pellentesque libero tortor, tincidunt et, tincidunt eget, semper nec, quam. Sed hendrerit. Morbi ac felis. Nunc egestas, augue at pellentesque laoreet, felis eros vehicula leo, at malesuada velit leo quis pede. Donec interdum, metus et hendrerit aliquet, dolor diam sagittis ligula, eget egestas libero turpis vel mi. Nunc nulla. Fusce risus nisl, viverra et, tempor et, pretium in, sapien. Donec venenatis vulputate lorem. Morbi nec metus. Phasellus blandit leo ut odio. Maecenas ullamcorper, dui et placerat feugiat, eros pede varius nisi, condimentum viverra felis nunc et lorem. Sed magna purus, fermentum eu, tincidunt eu, varius ut, felis. In auctor lobortis lacus. Quisque libero metus, condimentum nec, tempor a, commodo mollis, magna. Vestibulum ullamcorper mauris at ligula. Fusce fermentum. Nullam cursus lacinia erat. Praesent blandit laoreet nibh. Fusce convallis metus id felis luctus adipiscing. Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est. Quisque id mi. Ut tincidunt tincidunt erat. Etiam feugiat lorem non metus. Vestibulum dapibus nunc ac augue. Curabitur vestibulum aliquam leo. Praesent egestas neque eu enim. In hac habitasse platea dictumst. Fusce a quam. Etiam ut purus mattis mauris sodales aliquam. Curabitur nisi. Quisque malesuada placerat nisl. Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus. Sed augue ipsum, egestas nec, vestibulum et, malesuada adipiscing, dui. Vestibulum facilisis, purus nec pulvinar iaculis, ligula mi congue nunc, vitae euismod ligula urna in dolor. Mauris sollicitudin fermentum libero. Praesent nonummy mi in odio. Nunc interdum lacus sit amet orci. Vestibulum rutrum, mi nec elementum vehicula, eros quam gravida nisl, id fringilla neque ante vel mi. Morbi mollis tellus ac sapien. Phasellus volutpat, metus eget egestas mollis, lacus lacus blandit dui, id egestas quam mauris ut lacus. Fusce vel dui. Sed in libero ut nibh placerat accumsan. Proin faucibus arcu quis ante. In consectetuer turpis ut velit. Nulla sit amet est. Praesent metus tellus, elementum eu, semper a, adipiscing nec, purus. Cras risus ipsum, faucibus ut, ullamcorper id, varius ac, leo. Suspendisse feugiat. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Praesent nec nisl a purus blandit viverra. Praesent ac massa at ligula laoreet iaculis. Nulla neque dolor, sagittis eget, iaculis quis, molestie non, velit. Mauris turpis nunc, blandit et, volutpat molestie, porta ut, ligula. Fusce pharetra convallis urna. Quisque ut nisi. Donec mi odio, faucibus at, scelerisque quis, convallis in, nisi. Suspendisse non nisl sit amet velit hendrerit rutrum. Ut leo. Ut a nisl id ante tempus hendrerit. Proin pretium, leo ac pellentesque mollis, felis nunc ultrices eros, sed gravida augue augue mollis justo. Suspendisse eu ligula. Nulla facilisi. Donec id justo. Praesent porttitor, nulla vitae posuere iaculis, arcu nisl dignissim dolor, a pretium mi sem ut ipsum. Curabitur suscipit suscipit tellus. Praesent vestibulum dapibus nibh. Etiam iaculis nunc ac metus. Ut id nisl quis enim dignissim sagittis. Etiam sollicitudin, ipsum eu pulvinar rutrum, tellus ipsum laoreet sapien, quis venenatis ante odio sit amet eros. Proin magna. Duis vel nibh at velit scelerisque suscipit. Curabitur turpis. Vestibulum suscipit nulla quis orci. Fusce ac felis sit amet ligula pharetra condimentum. Maecenas egestas arcu quis ligula mattis placerat. Duis lobortis massa imperdiet quam. Suspendisse potenti. Pellentesque commodo eros a enim. Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl. Sed libero. Aliquam erat volutpat. Etiam vitae tortor. Morbi vestibulum volutpat enim. Aliquam eu nunc. Nunc sed turpis. Sed mollis, eros et ultrices tempus, mauris ipsum aliquam libero, non adipiscing dolor urna a orci. Nulla porta dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Pellentesque dapibus hendrerit tortor. Praesent egestas tristique nibh. Sed a libero. Cras varius. Donec vitae orci sed dolor rutrum auctor. Fusce egestas elit eget lorem. Suspendisse nisl elit, rhoncus eget, elementum ac, condimentum eget, diam. Nam at tortor in tellus interdum sagittis. Aliquam lobortis. Donec orci lectus, aliquam ut, faucibus non, euismod id, nulla. Curabitur blandit mollis lacus. Nam adipiscing. Vestibulum eu odio. Vivamus laoreet. Nullam tincidunt adipiscing enim. Phasellus tempus. Proin viverra, ligula sit amet ultrices semper, ligula arcu tristique sapien, a accumsan nisi mauris ac eros. Fusce neque. Suspendisse faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae iaculis lacus elit id tortor. Vivamus aliquet elit ac nisl. Fusce fermentum odio nec arcu. Vivamus euismod mauris. In ut quam vitae odio lacinia tincidunt. Praesent ut ligula non mi varius sagittis. Cras sagittis. Praesent ac sem eget est egestas volutpat. Vivamus consectetuer hendrerit lacus. Cras non dolor. Vivamus in erat ut urna cursus vestibulum. Fusce commodo aliquam arcu. Nam commodo suscipit quam. Quisque id odio. Praesent venenatis metus at tortor pulvinar varius. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vita', 'indio.jpg, queretaro-centro.jpg, arcos.jpg', 'Ejercicio 1 - Caso farmacéutica.pdf', 'https://www.youtube.com/watch?v=FiS4u98Scx8', 0, '1970-01-01 01:00:00', 0, '1970-01-01 01:00:00', 0),
(112, '2017-11-21 00:05:31', 'Noticia', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis diam. Pellentesque ut neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In dui magna, posuere eget, vestibulum et, tempor auctor, justo. In ac felis quis tortor malesuada pretium. Pellentesque auctor neque nec urna. Proin sapien ipsum, porta a, auctor quis, euismod ut, mi. Aenean viverra rhoncus pede. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut non enim eleifend felis pretium feugiat. Vivamus quis mi. Phasellus a est. Phasellus magna. In hac habitasse platea dictumst. Curabitur at lacus ac velit ornare lobortis. Curabitur a felis in nunc fringilla tristique. Morbi mattis ullamcorper velit. Phasellus gravida semper nisi. Nullam vel sem. Pellentesque libero tortor, tincidunt et, tincidunt eget, semper nec, quam. Sed hendrerit. Morbi ac felis. Nunc egestas, augue at pellentesque laoreet, felis eros vehicula leo, at malesuada velit leo quis pede. Donec interdum, metus et hendrerit aliquet, dolor diam sagittis ligula, eget egestas libero turpis vel mi. Nunc nulla. Fusce risus nisl, viverra et, tempor et, pretium in, sapien. Donec venenatis vulputate lorem. Morbi nec metus. Phasellus blandit leo ut odio. Maecenas ullamcorper, dui et placerat feugiat, eros pede varius nisi, condimentum viverra felis nunc et lorem. Sed magna purus, fermentum eu, tincidunt eu, varius ut, felis. In auctor lobortis lacus. Quisque libero metus, condimentum nec, tempor a, commodo mollis, magna. Vestibulum ullamcorper mauris at ligula. Fusce fermentum. Nullam cursus lacinia erat. Praesent blandit laoreet nibh. Fusce convallis metus id felis luctus adipiscing. Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est. Quisque id mi. Ut tincidunt tincidunt erat. Etiam feugiat lorem non metus. Vestibulum dapibus nunc ac augue. Curabitur vestibulum aliquam leo. Praesent egestas neque eu enim. In hac habitasse platea dictumst. Fusce a quam. Etiam ut purus mattis mauris sodales aliquam. Curabitur nisi. Quisque malesuada placerat nisl. Nam ipsum risus, rutrum vitae, vestibulum eu, molestie vel, lacus. Sed augue ipsum, egestas nec, vestibulum et, malesuada adipiscing, dui. Vestibulum facilisis, purus nec pulvinar iaculis, ligula mi congue nunc, vitae euismod ligula urna in dolor. Mauris sollicitudin fermentum libero. Praesent nonummy mi in odio. Nunc interdum lacus sit amet orci. Vestibulum rutrum, mi nec elementum vehicula, eros quam gravida nisl, id fringilla neque ante vel mi. Morbi mollis tellus ac sapien. Phasellus volutpat, metus eget egestas mollis, lacus lacus blandit dui, id egestas quam mauris ut lacus. Fusce vel dui. Sed in libero ut nibh placerat accumsan. Proin faucibus arcu quis ante. In consectetuer turpis ut velit. Nulla sit amet est. Praesent metus tellus, elementum eu, semper a, adipiscing nec, purus. Cras risus ipsum, faucibus ut, ullamcorper id, varius ac, leo. Suspendisse feugiat. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Praesent nec nisl a purus blandit viverra. Praesent ac massa at ligula laoreet iaculis. Nulla neque dolor, sagittis eget, iaculis quis, molestie non, velit. Mauris turpis nunc, blandit et, volutpat molestie, porta ut, ligula. Fusce pharetra convallis urna. Quisque ut nisi. Donec mi odio, faucibus at, scelerisque quis, convallis in, nisi. Suspendisse non nisl sit amet velit hendrerit rutrum. Ut leo. Ut a nisl id ante tempus hendrerit. Proin pretium, leo ac pellentesque mollis, felis nunc ultrices eros, sed gravida augue augue mollis justo. Suspendisse eu ligula. Nulla facilisi. Donec id justo. Praesent porttitor, nulla vitae posuere iaculis, arcu nisl dignissim dolor, a pretium mi sem ut ipsum. Curabitur suscipit suscipit tellus. Praesent vestibulum dapibus nibh. Etiam iaculis nunc ac metus. Ut id nisl quis enim dignissim sagittis. Etiam sollicitudin, ipsum eu pulvinar rutrum, tellus ipsum laoreet sapien, quis venenatis ante odio sit amet eros. Proin magna. Duis vel nibh at velit scelerisque suscipit. Curabitur turpis. Vestibulum suscipit nulla quis orci. Fusce ac felis sit amet ligula pharetra condimentum. Maecenas egestas arcu quis ligula mattis placerat. Duis lobortis massa imperdiet quam. Suspendisse potenti. Pellentesque commodo eros a enim. Vestibulum turpis sem, aliquet eget, lobortis pellentesque, rutrum eu, nisl. Sed libero. Aliquam erat volutpat. Etiam vitae tortor. Morbi vestibulum volutpat enim. Aliquam eu nunc. Nunc sed turpis. Sed mollis, eros et ultrices tempus, mauris ipsum aliquam libero, non adipiscing dolor urna a orci. Nulla porta dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Pellentesque dapibus hendrerit tortor. Praesent egestas tristique nibh. Sed a libero. Cras varius. Donec vitae orci sed dolor rutrum auctor. Fusce egestas elit eget lorem. Suspendisse nisl elit, rhoncus eget, elementum ac, condimentum eget, diam. Nam at tortor in tellus interdum sagittis. Aliquam lobortis. Donec orci lectus, aliquam ut, faucibus non, euismod id, nulla. Curabitur blandit mollis lacus. Nam adipiscing. Vestibulum eu odio. Vivamus laoreet. Nullam tincidunt adipiscing enim. Phasellus tempus. Proin viverra, ligula sit amet ultrices semper, ligula arcu tristique sapien, a accumsan nisi mauris ac eros. Fusce neque. Suspendisse faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae iaculis lacus elit id tortor. Vivamus aliquet elit ac nisl. Fusce fermentum odio nec arcu. Vivamus euismod mauris. In ut quam vitae odio lacinia tincidunt. Praesent ut ligula non mi varius sagittis. Cras sagittis. Praesent ac sem eget est egestas volutpat. Vivamus consectetuer hendrerit lacus. Cras non dolor. Vivamus in erat ut urna cursus vestibulum. Fusce commodo aliquam arcu. Nam commodo suscipit quam. Quisque id odio. Praesent venenatis metus at tortor pulvinar varius. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vita', 'arcos.jpg, indio.jpg, queretaro-centro.jpg', 'Ejercicio 1 - Caso farmacéutica.pdf', 'https://www.youtube.com/watch?v=PdCylcA_c40', 0, '1970-01-01 00:00:00', 0, '1970-01-01 00:00:00', 1),
(113, '2017-11-30 17:58:20', 'Geologico', 'Cuerpo geologicooo', '', '', '', 0, '1970-01-01 00:00:00', 0, '1970-01-01 00:00:00', 0),
(114, '2017-11-30 18:29:52', 'Hidro', 'asdgfsa asdbgasfdg', '', '', '', 0, '1970-01-01 01:00:00', 0, '1970-01-01 01:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `rol`
--

CREATE TABLE `rol` (
  `IdRol` int(8) NOT NULL,
  `NombreRol` varchar(50) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `DescripcionRol` varchar(250) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rol`
--

INSERT INTO `rol` (`IdRol`, `NombreRol`, `DescripcionRol`) VALUES
(1, 'Administrador', 'Administrador del sitio. Puede hacer uso de todos los privilegios.'),
(2, 'Empleado', 'Empleados de Protección Civil que pueden aportar al sitio, cuentan con ciertos privilegios.'),
(3, 'Ayudante', 'Ayudantes de empleados de Protección Civil, necesitan aprobación de su superior para que se realice una publicación suya.');

-- --------------------------------------------------------

--
-- Table structure for table `subcategoria`
--

CREATE TABLE `subcategoria` (
  `IdSubcategoria` int(11) NOT NULL,
  `IdCategoria` int(11) NOT NULL,
  `NombreSubcategoria` varchar(50) COLLATE utf8_bin NOT NULL,
  `Descripcion` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `CorreoSubcategoria` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `cc` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `subcategoria`
--

INSERT INTO `subcategoria` (`IdSubcategoria`, `IdCategoria`, `NombreSubcategoria`, `Descripcion`, `CorreoSubcategoria`, `cc`) VALUES
(1, 1, 'Conócenos', NULL, NULL, NULL),
(2, 1, 'Misión', NULL, NULL, NULL),
(3, 1, 'Ubicación', NULL, NULL, NULL),
(4, 2, 'Naturales', 'Naturales: Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim', NULL, NULL),
(5, 2, 'Antrópicos', 'Antropicos: Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim', NULL, NULL),
(6, 2, 'Astronómicos', 'Astronomicos: Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim', NULL, NULL),
(7, 3, 'Niños', NULL, NULL, NULL),
(8, 3, 'Jóvenes', NULL, NULL, NULL),
(9, 3, 'Adultos mayores', NULL, NULL, NULL),
(10, 3, 'Familiar', NULL, NULL, NULL),
(11, 4, 'Temporada de frío y heladas', NULL, NULL, NULL),
(12, 5, 'Aforos', NULL, 'vhernandezm@queretaro.gob.mx', 'jmendozam@queretaro.gob.mx,lnicteha@queretaro.gob.mx'),
(13, 5, 'Eventos masivos', NULL, 'cmendoza@queretaro.gob.mx', 'farroy@queretaro.gob.mx,lnicteha@queretaro.gob.mx'),
(14, 5, 'Licencia de alcoholes', NULL, 'cmendoza@queretaro.gob.mx', 'farroy@queretaro.gob.mx,lnicteha@queretaro.gob.mx'),
(15, 5, 'Registro de terceros acreditados', NULL, 'mdenis@@queretaro.gob.mx', 'lnicteha@queretaro.gob.mx'),
(16, 5, 'Registro de grupos voluntarios', NULL, 'hgarciah@queretaro.gob.mx', 'lnicteha@queretaro.gob.mx'),
(17, 6, 'Capacitación', NULL, 'mlanderost@queretaro.gob.mx', 'aoropeza@queretaro.gob.mx,lnicteha@queretaro.gob.mx'),
(18, 6, 'Análisis de riesgos', NULL, 'sgarcias@queretaro.gob.mx ', 'jmendozam@queretaro.gob.mx,lnicteha@queretaro.gob.mx'),
(19, 7, 'Marco jurídico', NULL, NULL, NULL),
(20, 7, 'Guías', NULL, NULL, NULL),
(21, 7, 'Manuales', NULL, NULL, NULL),
(22, 7, 'Directorio', NULL, NULL, NULL),
(23, 8, 'Noticias', NULL, NULL, NULL),
(24, 2, 'Geológicos', 'Geólogicos que pertenece a Naturales: Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasell', NULL, NULL),
(25, 2, 'Hidrometeorológicos', 'Hidrometeorológicos de Naturales: Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenea', NULL, NULL),
(26, 2, 'Sanitarios - Ecologicos', 'Sanitarios - Ecologicos pertenece a 	\r\nAntrópicos: Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra ', NULL, NULL),
(27, 2, 'Químicos - Tecnológicos', 'Químicos - Tecnológicos pertenece a 	\r\nAntrópicos: Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra ', NULL, NULL),
(28, 2, 'Socio - Organizativos', 'Socio - Organizativos - Pertenece a 	\r\nAntrópicos: Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra ', NULL, NULL),
(29, 4, 'Incendios forestales', NULL, NULL, NULL),
(30, 4, 'Temporada de lluvias', NULL, NULL, NULL),
(31, 4, 'Respuesta y vinculación por sismos', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `textoinicio`
--

CREATE TABLE `textoinicio` (
  `Id` int(11) NOT NULL,
  `Texto` varchar(4000) NOT NULL,
  `RutaImagen` varchar(300) NOT NULL,
  `RutaImagenUsuario` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `textoinicio`
--

INSERT INTO `textoinicio` (`Id`, `Texto`, `RutaImagen`, `RutaImagenUsuario`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duo enim genera quae erant, fecit tria. Quid de Platone aut de Democrito loquar? Minime vero istorum quidem, inquit. Teneo, inquit, finem illi videri nihil dolere. Nunc ita separantur, ut disiuncta sint, quo nihil potest esse perversius. Quod non faceret, si in voluptate summum bonum poneret. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duo enim genera quae erant, fecit tria. Quid de Platone aut de Democrito loquar? Minime vero istorum quidem, inquit. Teneo, inquit, finem illi videri nihil dolere. Nunc ita separantur, ut disiuncta sint, quo nihil potest esse perversius. Quod non faceret, si in voluptate summum bonum', '1512008875-hd-desktop-wallpaper-22.jpg', 'hd-desktop-wallpaper-22.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tipodepublicacion`
--

CREATE TABLE `tipodepublicacion` (
  `IdTipoPublicacion` int(11) NOT NULL,
  `NombreTipoPublicacion` varchar(50) COLLATE utf8_bin NOT NULL,
  `DescripcionTipoPublicacion` varchar(250) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `tipoevento`
--

CREATE TABLE `tipoevento` (
  `IdTipoEvento` int(11) NOT NULL,
  `NombreTipoEvento` varchar(50) CHARACTER SET utf8 NOT NULL,
  `DescripcionTipoEvento` varchar(250) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipoevento`
--

INSERT INTO `tipoevento` (`IdTipoEvento`, `NombreTipoEvento`, `DescripcionTipoEvento`) VALUES
(1, 'Capacitación', 'Evento en el que se dará una capacitación relacionada a Protección Civil.'),
(2, 'Plática', 'Evento en el que un ponente importante dentro del área de Protección Civil dará una charla informativa.'),
(3, 'Taller', 'Evento en el que los asistentes podrán formar parte de un taller que les ayudará a estar preparados ante cualquier situación.'),
(4, 'Curso', 'Evento que consiste en una serie de actividades para preparar a los asistentes sobre algún tema en específico.'),
(5, 'Junta', 'Evento en el que se convoca a los miembros de Protección Civil para hablar sobre temas importantes.');

-- --------------------------------------------------------

--
-- Table structure for table `trabajaen`
--

CREATE TABLE `trabajaen` (
  `IdUsuario` int(11) NOT NULL,
  `IdDepartamento` int(8) NOT NULL,
  `Fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trabajaen`
--

INSERT INTO `trabajaen` (`IdUsuario`, `IdDepartamento`, `Fecha`) VALUES
(5, 3, NULL),
(1, 3, NULL),
(6, 4, NULL),
(7, 1, NULL),
(8, 78, NULL),
(9, 1, NULL),
(10, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tramitamos`
--

CREATE TABLE `tramitamos` (
  `IdTramitamos` int(11) NOT NULL,
  `RutaImagenTramitamos` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tramitamos`
--

INSERT INTO `tramitamos` (`IdTramitamos`, `RutaImagenTramitamos`) VALUES
(2, 'tramitamos2.jpeg'),
(3, 'tramitamos3.jpeg'),
(4, 'tramitamos4.jpeg'),
(5, 'tramitamos5.jpeg'),
(6, '1512054301-myw3schoolsimage.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `IdUsuario` int(11) NOT NULL,
  `NombreUsuario` varchar(50) NOT NULL,
  `IdEmpleado` int(8) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  `ApellidoP` varchar(50) NOT NULL,
  `ApellidoM` varchar(50) DEFAULT NULL,
  `Puesto` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`IdUsuario`, `NombreUsuario`, `IdEmpleado`, `Nombre`, `ApellidoP`, `ApellidoM`, `Puesto`) VALUES
(1, 'lnicteha@queretaro.gob.mx', 57926, 'Lucía Nicté-Há', 'Koh', 'Lira', 'Directora de Promoción y Difusión '),
(5, 'mdenis@queretaro.gob.mx', 19020, 'María de Lourdes', 'Denis', 'Estrada', 'Promotor de Programas de Protección Civil '),
(6, 'mlanderost@queretaro.gob.mx', 189265, 'Lucero', 'Landeros', 'Tejeida', 'Administradora del Centro de Capacitación'),
(7, 'vhernandezm@queretaro.gob.mx', 55696, 'Veronica', 'Hernández', 'Moran', 'Auxiliar de Información'),
(8, 'hgarciah@queretaro.gob.mx', 59816, 'Humberto', 'García', 'Herrera', 'Jefe de Área'),
(9, 'lnavarrete@queretarogob.mx', 154831, 'Luis Alfredo', 'Navarrete', 'Ramos', 'Instructor en Atención de Emergencias '),
(10, 'cmendoza@queretaro.gob.mx', 78644, 'Carlos', 'Mendoza', 'Martínez', 'Jefe del Área de Atención de Riesgos y Eventos ');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `archivosarticulo`
--
ALTER TABLE `archivosarticulo`
  ADD PRIMARY KEY (`idArchivosArticulos`),
  ADD KEY `idPublicacion` (`idPublicacion`);

--
-- Indexes for table `archivosevento`
--
ALTER TABLE `archivosevento`
  ADD PRIMARY KEY (`IdArchivosEventos`),
  ADD KEY `IdPublicacion` (`IdEvento`);

--
-- Indexes for table `asignadoa`
--
ALTER TABLE `asignadoa`
  ADD KEY `fk_idRol` (`IdRol`),
  ADD KEY `fk_AsignadoAUsuario` (`IdUsuario`);

--
-- Indexes for table `atendemos`
--
ALTER TABLE `atendemos`
  ADD PRIMARY KEY (`IdAtendemos`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`IdBanner`);

--
-- Indexes for table `bitacora`
--
ALTER TABLE `bitacora`
  ADD KEY `IdPublicacion` (`IdPublicacion`),
  ADD KEY `IdUsuario` (`IdUsuario`);

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`IdCategoria`);

--
-- Indexes for table `correspondea`
--
ALTER TABLE `correspondea`
  ADD PRIMARY KEY (`IdPublicacion`,`IdSubcategoria`),
  ADD KEY `IdPublicacion` (`IdPublicacion`,`IdSubcategoria`),
  ADD KEY `IdCategoria` (`IdSubcategoria`),
  ADD KEY `IdPublicacion_2` (`IdPublicacion`);

--
-- Indexes for table `cuentacon`
--
ALTER TABLE `cuentacon`
  ADD KEY `IdPrivilegio` (`IdPrivilegio`),
  ADD KEY `IdRol` (`IdRol`);

--
-- Indexes for table `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`IdDepartamento`);

--
-- Indexes for table `evento`
--
ALTER TABLE `evento`
  ADD PRIMARY KEY (`IdEvento`);

--
-- Indexes for table `imagenesarticulo`
--
ALTER TABLE `imagenesarticulo`
  ADD PRIMARY KEY (`idImagenesArticulos`),
  ADD KEY `idPublicacion` (`idPublicacion`);

--
-- Indexes for table `imagenesevento`
--
ALTER TABLE `imagenesevento`
  ADD PRIMARY KEY (`IdImagenesEventos`),
  ADD KEY `IdPublicacion` (`IdEvento`);

--
-- Indexes for table `perteneceaevento`
--
ALTER TABLE `perteneceaevento`
  ADD KEY `IdEvento` (`IdEvento`),
  ADD KEY `IdTipoEvento` (`IdTipoEvento`);

--
-- Indexes for table `perteneceapublicacion`
--
ALTER TABLE `perteneceapublicacion`
  ADD PRIMARY KEY (`IdTipoPublicacion`,`IdPublicacion`),
  ADD KEY `IdTipoPublicacion` (`IdTipoPublicacion`),
  ADD KEY `IdPublicacion` (`IdPublicacion`),
  ADD KEY `IdTipoPublicacion_2` (`IdTipoPublicacion`);

--
-- Indexes for table `privilegio`
--
ALTER TABLE `privilegio`
  ADD PRIMARY KEY (`IdPrivilegio`);

--
-- Indexes for table `publicacion`
--
ALTER TABLE `publicacion`
  ADD PRIMARY KEY (`IdPublicacion`);

--
-- Indexes for table `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`IdRol`);

--
-- Indexes for table `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD PRIMARY KEY (`IdSubcategoria`,`IdCategoria`),
  ADD KEY `IdCategoria` (`IdCategoria`);

--
-- Indexes for table `textoinicio`
--
ALTER TABLE `textoinicio`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `tipodepublicacion`
--
ALTER TABLE `tipodepublicacion`
  ADD PRIMARY KEY (`IdTipoPublicacion`);

--
-- Indexes for table `tipoevento`
--
ALTER TABLE `tipoevento`
  ADD PRIMARY KEY (`IdTipoEvento`);

--
-- Indexes for table `trabajaen`
--
ALTER TABLE `trabajaen`
  ADD KEY `fk_TrabajaEnUsuario` (`IdUsuario`);

--
-- Indexes for table `tramitamos`
--
ALTER TABLE `tramitamos`
  ADD PRIMARY KEY (`IdTramitamos`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`IdUsuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `archivosarticulo`
--
ALTER TABLE `archivosarticulo`
  MODIFY `idArchivosArticulos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `archivosevento`
--
ALTER TABLE `archivosevento`
  MODIFY `IdArchivosEventos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `atendemos`
--
ALTER TABLE `atendemos`
  MODIFY `IdAtendemos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `IdBanner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `IdCategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `departamento`
--
ALTER TABLE `departamento`
  MODIFY `IdDepartamento` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `evento`
--
ALTER TABLE `evento`
  MODIFY `IdEvento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `imagenesarticulo`
--
ALTER TABLE `imagenesarticulo`
  MODIFY `idImagenesArticulos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;
--
-- AUTO_INCREMENT for table `imagenesevento`
--
ALTER TABLE `imagenesevento`
  MODIFY `IdImagenesEventos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `privilegio`
--
ALTER TABLE `privilegio`
  MODIFY `IdPrivilegio` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `publicacion`
--
ALTER TABLE `publicacion`
  MODIFY `IdPublicacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT for table `rol`
--
ALTER TABLE `rol`
  MODIFY `IdRol` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `subcategoria`
--
ALTER TABLE `subcategoria`
  MODIFY `IdSubcategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `textoinicio`
--
ALTER TABLE `textoinicio`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tramitamos`
--
ALTER TABLE `tramitamos`
  MODIFY `IdTramitamos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `IdUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `archivosarticulo`
--
ALTER TABLE `archivosarticulo`
  ADD CONSTRAINT `archivosarticulo_ibfk_1` FOREIGN KEY (`idPublicacion`) REFERENCES `publicacion` (`IdPublicacion`);

--
-- Constraints for table `archivosevento`
--
ALTER TABLE `archivosevento`
  ADD CONSTRAINT `archivosevento_ibfk_1` FOREIGN KEY (`IdEvento`) REFERENCES `evento` (`IdEvento`) ON DELETE CASCADE;

--
-- Constraints for table `asignadoa`
--
ALTER TABLE `asignadoa`
  ADD CONSTRAINT `fk_AsignadoAUsuario` FOREIGN KEY (`IdUsuario`) REFERENCES `usuario` (`IdUsuario`);

--
-- Constraints for table `bitacora`
--
ALTER TABLE `bitacora`
  ADD CONSTRAINT `IdPublicacion` FOREIGN KEY (`IdPublicacion`) REFERENCES `publicacion` (`IdPublicacion`),
  ADD CONSTRAINT `IdUsuario` FOREIGN KEY (`IdUsuario`) REFERENCES `usuario` (`IdUsuario`);

--
-- Constraints for table `correspondea`
--
ALTER TABLE `correspondea`
  ADD CONSTRAINT `correspondea_ibfk_1` FOREIGN KEY (`IdSubcategoria`) REFERENCES `subcategoria` (`IdSubcategoria`),
  ADD CONSTRAINT `correspondea_ibfk_2` FOREIGN KEY (`IdPublicacion`) REFERENCES `publicacion` (`IdPublicacion`);

--
-- Constraints for table `imagenesarticulo`
--
ALTER TABLE `imagenesarticulo`
  ADD CONSTRAINT `imagenesarticulo_ibfk_1` FOREIGN KEY (`idPublicacion`) REFERENCES `publicacion` (`IdPublicacion`);

--
-- Constraints for table `imagenesevento`
--
ALTER TABLE `imagenesevento`
  ADD CONSTRAINT `imagenesevento_ibfk_1` FOREIGN KEY (`IdEvento`) REFERENCES `evento` (`IdEvento`) ON DELETE CASCADE;

--
-- Constraints for table `perteneceaevento`
--
ALTER TABLE `perteneceaevento`
  ADD CONSTRAINT `perteneceaevento_ibfk_1` FOREIGN KEY (`IdEvento`) REFERENCES `evento` (`IdEvento`) ON DELETE CASCADE,
  ADD CONSTRAINT `perteneceaevento_ibfk_2` FOREIGN KEY (`IdTipoEvento`) REFERENCES `tipoevento` (`IdTipoEvento`) ON DELETE CASCADE;

--
-- Constraints for table `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD CONSTRAINT `subcategoria_ibfk_1` FOREIGN KEY (`IdCategoria`) REFERENCES `categoria` (`IdCategoria`);

--
-- Constraints for table `trabajaen`
--
ALTER TABLE `trabajaen`
  ADD CONSTRAINT `fk_TrabajaEnUsuario` FOREIGN KEY (`IdUsuario`) REFERENCES `usuario` (`IdUsuario`);

DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `eliminarArchivosSinEvento` ON SCHEDULE EVERY 1 DAY STARTS '2017-12-01 02:00:00' ON COMPLETION NOT PRESERVE ENABLE DO DELETE FROM archivosevento WHERE idEvento = NULL$$

CREATE DEFINER=`root`@`localhost` EVENT `eliminarImagenesSinArticulo` ON SCHEDULE EVERY 1 DAY STARTS '2017-12-01 02:00:00' ON COMPLETION NOT PRESERVE ENABLE DO DELETE FROM imagenesarticulo WHERE idPublicacion = NULL$$

CREATE DEFINER=`root`@`localhost` EVENT `eliminarImagenesSinEvento` ON SCHEDULE EVERY 1 DAY STARTS '2017-12-01 02:00:00' ON COMPLETION NOT PRESERVE ENABLE DO DELETE FROM imagenesevento WHERE idEvento = NULL$$

CREATE DEFINER=`root`@`localhost` EVENT `eliminarArchivosSinArticulo` ON SCHEDULE EVERY 1 DAY STARTS '2017-12-01 02:00:00' ON COMPLETION NOT PRESERVE ENABLE DO DELETE FROM archivosarticulo WHERE idPublicacion = NULL$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
