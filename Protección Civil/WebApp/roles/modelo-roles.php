<?php
    require_once("../modelo.php");
    require_once("privilegios/modelo-privilegios.php");

    function getRoles() {
        $db = conectar();

        //Specification of the SQL query
        $query='SELECT * FROM rol';
        // Query execution; returns identifier of the result group
        $roles = $db->query($query);

        $i = 1;
        
        $result = '<table class="striped">
            <thead>
                <tr>
                    <th></th>
                    <th>Nombre</th>
                    <th>Descripción</th>
                </tr>
            </thead>
            <tbody>';

        // cycle to explode every line of the results
        while ($fila = mysqli_fetch_array($roles, MYSQLI_BOTH)) {

            $result .= '<tr>
                <td>'.$i.'.  </td>
                <td>'.$fila["NombreRol"].'</td>
                <td>'.$fila["DescripcionRol"].'</td>';
            
            if($i == 1){
                $result .= '<td></td><td></td>';
            }else{
                $result .= '<td>
                                <a href="editar-rol.php?IdRol='.$fila["IdRol"].'")" class="btn-floating blue tooltipped" data-position="top" data-delay="50" data-tooltip="Modificar rol"><i class="material-icons">edit</i></a>
                            </td>
                            <td>
                                <a href="#" class="btn-floating red tooltipped botonBorrar" data-id="'.$fila["IdRol"].'" data-ruta="eliminar-rol.php" data-tipo="rol" data-position="top" data-delay="50" data-tooltip="Eliminar rol"><i class="material-icons">delete_forever</i></a>
                            </td>';
            }

            $result .= '</tr>';

            $i++;
        }
        
        $result .= '</tbody></table>';

        // it releases the associated results
        mysqli_free_result($roles);

        desconectar($db);

        return $result;
    }

    function getDropdownRoles($IdRol = 0){
        $db = conectar();

        //Specification of the SQL query
        $query='SELECT * FROM rol ORDER BY NombreRol';
        // Query execution; returns identifier of the result group
        $roles = $db->query($query);

        // cycle to explode every line of the results
        while ($fila = mysqli_fetch_array($roles, MYSQLI_BOTH)) {
            if($fila["NombreRol"] != "Administrador"){
                if($fila["IdRol"] == $IdRol){
                    $result .= '<option value="'.$fila["IdRol"].'" selected>'.$fila["NombreRol"].'</option>';
                }else{
                    $result .= '<option value="'.$fila["IdRol"].'">'.$fila["NombreRol"].'</option>';
                }
            }
        }

        // it releases the associated results
        mysqli_free_result($roles);

        desconectar($db);

        return $result;
    }

    function getDropdownEmpleado(){
        $db = conectar();

        //Specification of the SQL query
        $query='SELECT * FROM rol';
        // Query execution; returns identifier of the result group
        $roles = $db->query($query);

        $i = 1;

        // cycle to explode every line of the results
        while ($fila = mysqli_fetch_array($roles, MYSQLI_BOTH)) {
            if($i == 1){
                $result = "";
            }else{
                $result .= '<option value="'.$i.'">'.$fila["NombreRol"].'</option>';
            }
            $i++;
        }

        // it releases the associated results
        mysqli_free_result($roles);

        desconectar($db);

        return $result;
    }

    function guardarRol($nombre, $descripcion){
        $db = conectar();

        $query='INSERT INTO rol (`NombreRol`, `DescripcionRol`) VALUES (?,?)';

        // Preparing the statement
        if (!($statement = $db->prepare($query))) {
            die("Preparation failed: (" . $db->errno . ") " . $db->error);
        }
        // Binding statement params
        if (!$statement->bind_param("ss", $nombre, $descripcion)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
        }
        // Executing the statement
        if (!$statement->execute()) {
            die("Execution failed: (" . $statement->errno . ") " . $statement->error);
         }

        desconectar($db);
    }

    function editarRol($id, $nombre, $descripcion){
        $db = conectar();

        // insert command specification
        $query='UPDATE rol SET NombreRol=?, DescripcionRol=? WHERE IdRol=?';
        // Preparing the statement
        if (!($statement = $db->prepare($query))) {
            die("Preparation failed: (" . $db->errno . ") " . $db->error);
        }
        // Binding statement params
        if (!$statement->bind_param("sss", $nombre, $descripcion, $id)) {
            die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
        }
        // update execution
        if ($statement->execute()) {
            //echo 'There were ' . mysqli_affected_rows($db) . ' affected rows';
        } else {
            die("Update failed: (" . $statement->errno . ") " . $statement->error);
        }

        desconectar($db);
    }

    function getByIdRol($db, $IdRol){
        //Specification of the SQL query
        $query='SELECT * FROM rol WHERE IdRol = "'.$IdRol.'"';
         // Query execution; returns identifier of the result group
        $registros = $db->query($query);
        $rol = mysqli_fetch_array($registros, MYSQLI_BOTH);
        return $rol;
    }

    function eliminarRol($IdRol){
        //Eliminar Privilegios del Rol.
        eliminarPrivilegios($IdRol);

        $db = conectar();

        // insert command specification
        $query='DELETE FROM rol WHERE IdRol = ?';

        // Preparing the statement
        if (!($statement = $db->prepare($query))) {
            die("Preparation failed: (" . $db->errno . ") " . $db->error);
        }
        // Binding statement params
        if (!$statement->bind_param("s", $IdRol)) {
                die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
            }
        // Executing the statement
        if (!$statement->execute()) {
            die("Execution failed: (" . $statement->errno . ") " . $statement->error);
        }

        desconectar($db);
    }

    function nombreRolUnico($nombreRol){
        $db = conectar();

        //Specification of the SQL query
        $query='SELECT * FROM rol';
        // Query execution; returns identifier of the result group
        $roles = $db->query($query);

        $i = 0;

        while ($fila = mysqli_fetch_array($roles, MYSQLI_BOTH)) {
            if($fila["NombreRol"] == $nombreRol){
                mysqli_free_result($roles);
                desconectar($db);
                return false;
            }
        }
        mysqli_free_result($roles);
        return true;
    }

    function getIdRolByName($nombreRol){
        $db = conectar();
        //Specification of the SQL query
        $query='SELECT * FROM rol WHERE NombreRol LIKE \''.$nombreRol.'\'';
         // Query execution; returns identifier of the result group
        $registros = $db->query($query);

        $id = mysqli_fetch_array($registros, MYSQLI_BOTH);

        return $id["IdRol"];
    }

    function getIdRolByUserId($idUsuario){
        $db = conectar();
        //Specification of the SQL query
        $query='SELECT * FROM asignadoa WHERE NombreUsuario = "'.$idUsuario.'"';
         // Query execution; returns identifier of the result group
        $registros = $db->query($query);

        $id = mysqli_fetch_array($registros, MYSQLI_BOTH);

        return $id["IdRol"];
    }
?>
