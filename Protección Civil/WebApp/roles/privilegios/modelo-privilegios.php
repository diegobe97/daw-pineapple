<?php
    require_once("../modelo.php");

    function getPrivilegios($IdRol = 0){
        $db = conectar();
        
        if($IdRol > 0){
            //Specification of the SQL query
            $query='SELECT * FROM cuentacon WHERE IdRol = '.$IdRol.' ORDER BY IdPrivilegio';
            // Query execution; returns identifier of the result group
            $privilegiosDeRol = $db->query($query);

            $n = $privilegiosDeRol->num_rows;
            
            $arrayPrivilegios = array_fill(0,$n,-1);

            $i = 0;

            while ($filaPrivilegiosRol = mysqli_fetch_array($privilegiosDeRol, MYSQLI_BOTH)) {
                $arrayPrivilegios[$i] = $filaPrivilegiosRol["IdPrivilegio"];
                $i++;
            }
        }
        
        //Specification of the SQL query
        $query='SELECT * FROM privilegio';
        // Query execution; returns identifier of the result group
        $privilegios = $db->query($query);

        $result = "";
        $privilegioSeleccionado = false;
        
        $i = 0;
        $j = 0;

        // cycle to explode every line of the results
        while ($filaPrivilegios = mysqli_fetch_array($privilegios, MYSQLI_BOTH)) {
            if($j % 7 == 0){
                $result .= '<div class="col s12 m6 l3">';
            }
            
            if($filaPrivilegios["IdPrivilegio"] != 24){
                if($IdRol > 0){
                    $privilegioSeleccionado = false;
                    if($i < $n && $filaPrivilegios["IdPrivilegio"] == $arrayPrivilegios[$i]){
                        $result .= '<p>
                                        <input type="checkbox" class="filled-in checkbox-rol privilegio" name="privilegio'.$filaPrivilegios["IdPrivilegio"].'" id="privilegio'.$filaPrivilegios["IdPrivilegio"].'" checked="checked"/>
                                        <label class="black-text" for="privilegio'.$filaPrivilegios["IdPrivilegio"].'">'.$filaPrivilegios["NombrePrivilegio"].'</label>
                                    </p>';
                                    $privilegioSeleccionado = true;
                        $i++;
                    }
                }
                if(!$privilegioSeleccionado){
                    $result .= '<p>
                        <input type="checkbox" class="filled-in checkbox-rol privilegio" name="privilegio'.$filaPrivilegios["IdPrivilegio"].'" id="privilegio'.$filaPrivilegios["IdPrivilegio"].'"/>
                        <label class="black-text" for="privilegio'.$filaPrivilegios["IdPrivilegio"].'">'.$filaPrivilegios["NombrePrivilegio"].'</label>
                    </p>';
                }
            }
            
            if($j % 7 == 6){
                $result .= '</div>';
            }
            $j++;
        }
        // it releases the associated results
        mysqli_free_result($privilegios);

        desconectar($db);

        return $result;
    }

    function countPrivilegios(){
        $db = conectar();

        //Specification of the SQL query
        $query='SELECT count(*) As total FROM privilegio';
        // Query execution; returns identifier of the result group
        $cant = $db->query($query);

        $result = mysqli_fetch_array($cant, MYSQLI_BOTH);
        
        desconectar($db);

        return $result["total"];
    }

    function countPrivilegiosByIdRol($IdRol){
        $db = conectar();

        //Specification of the SQL query
        $query='SELECT count(*) As total FROM cuentacon WHERE IdRol = "'.$IdRol.'"';
        // Query execution; returns identifier of the result group
        $cant = $db->query($query);

        $result = mysqli_fetch_array($cant, MYSQLI_BOTH);
        
        desconectar($db);

        return $result["total"];
    }

    function getPrivilegiosByIdRol($db, $IdRol){
        //Specification of the SQL query
        $query='SELECT * FROM cuentacon WHERE IdRol = "'.$IdRol.'"';
         // Query execution; returns identifier of the result group
        $registros = $db->query($query);   
        $privilegios = mysqli_fetch_array($registros, MYSQLI_BOTH);
        return $privilegios;
    }

    function guardarPrivilegios($IdRol, $arrayPrivilegios){
        for($i = 1; $i <= count($arrayPrivilegios); $i++){
            if($arrayPrivilegios[$i] == 1){
                $db = conectar();
                
                $query='INSERT INTO cuentacon (`IdRol`, `IdPrivilegio`) VALUES (?,?)';

                // Preparing the statement 
                if (!($statement = $db->prepare($query))) {
                    die("Preparation failed: (" . $db->errno . ") " . $db->error);
                }
                // Binding statement params 
                if (!$statement->bind_param("ss", $IdRol, $i)) {
                    die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
                }
                // Executing the statement
                if (!$statement->execute()) {
                    die("Execution failed: (" . $statement->errno . ") " . $statement->error);
                }
                
                desconectar($db);
            }
        }
    }

    function eliminarPrivilegios($IdRol){
        $db = conectar();
        
        // insert command specification 
        $query='DELETE FROM cuentacon WHERE IdRol = ?';
        
        // Preparing the statement 
        if (!($statement = $db->prepare($query))) {
            die("Preparation failed: (" . $db->errno . ") " . $db->error);
        }
        // Binding statement params 
        if (!$statement->bind_param("s", $IdRol)) {
                die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error);
            }
        // Executing the statement
        if (!$statement->execute()) {
            die("Execution failed: (" . $statement->errno . ") " . $statement->error);
        }
        
        desconectar($db);
    }

    function getPrivilegiosFor($IdRol){
        $db = conectar();
        
        //Specification of the SQL query
        $query='SELECT * FROM cuentacon WHERE IdRol = "'.$IdRol.'"';
         // Query execution; returns identifier of the result group
        $registros = $db->query($query);
        
        $privilegios = mysqli_fetch_array($registros, MYSQLI_BOTH);
        
        //Specification of the SQL query
        $query='SELECT count(*) As total FROM cuentacon WHERE IdRol = "'.$IdRol.'"';
        // Query execution; returns identifier of the result group
        $n = $db->query($query);
        
        $arrayPrivilegios = array_fill(0,$n,-1);
        
        $i = 0;
        
        while ($filaPrivilegios = mysqli_fetch_array($privilegios, MYSQLI_BOTH)) {
            $arrayPrivilegios[$i] = $filaPrivilegios["IdPrivilegio"];
            $i++;
        }
        
        // it releases the associated results
        mysqli_free_result($privilegios);
        
        return $arrayPrivilegios;
    }
?>