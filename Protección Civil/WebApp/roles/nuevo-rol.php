<?php
    session_start();
    if(isset($_SESSION["privilegios"])){
        require_once("modelo-roles.php");
        include('../_header.html');
        
        include("_form-rol.html");

        include('../_user-menu.html');
        include('../_footer.html');
    }else{
        include('../error.html');
    }
?>