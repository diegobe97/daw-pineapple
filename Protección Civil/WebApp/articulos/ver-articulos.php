<?php
    session_start();
    require_once("modelo-articulos.php");
    
    if (isset($_GET["id"])){
        $id = $_GET["id"];
        $subcategoria = getSubcategoria($id);
    }
    if (isset($_GET["sub"])){
        $sub = $_GET["sub"];
    }
    if(isset($sub)){
        $id=getIdSubcategoria($sub);
    }


    if(isset($id)){
        $subcategoria=getSubcategoria($id);    
    }

    if(isset($_SESSION["privilegios"])){
        include('../_header.html');
        include('_articulos-main.html');
        
        if(isset($sub)){
            if($sub == "Naturales"){
                include('_naturales-main.html');
            }else if($sub == "Antrópicos"){
                include('_antropicos-main.html');
            }else if($sub == "Astronómicos"){
                include('_astronomicos-main.html');
            }
        }
        
        include('../_user-menu.html');
        include('../_footer.html');

        if (isset($_SESSION["mensaje"])) {
            $mensaje = $_SESSION["mensaje"];
            include('../_mensaje.html');
            unset($_SESSION["mensaje"]);
        }
    }else{
        if(isset($id)){
            include('../_header.html');
            include('_articulos-main.html');
            if(isset($sub)){
                if($sub == "Naturales"){
                    include('_naturales-main.html');
                }else if($sub == "Antrópicos"){
                    include('_antropicos-main.html');
                }else if($sub == "Astronómicos"){
                    include('_astronomicos-main.html');
                }
            }
            include('../_footer.html');
        }else{
            include('../error.html');
        }
    }
?>