<?php
    session_start();
    require_once("modelo-articulos.php");
    if(isset($_SESSION["privilegios"])){
        if(binarySearch($_SESSION["privilegios"], 29)){
            if(isset($_GET["id"])){
                $subcategoria = getSubcategoria($_GET["id"]);
                if($subcategoria!="error"){
                    include("../_header.html");
                    include("_form-subcategoria.html");
                    include('../_user-menu.html');
                    include("../_footer.html");
                }else{
                    include('../error.html');
                }
            }else{
                include('../error.html');
            }  
        }else{
          include('../error.html');  
        } 
    }else{
        include('../error.html');
    }
?>