<?php
    require_once("modelo-articulos.php");
    
    function getNoticiaCard($n){
        revisarPublicarDespues();
        revisarFijos();
        $db = conectar(); 

        $query='SELECT * FROM publicacion p, correspondea c WHERE p.Borrador = 0 AND p.PublicarDespues = 0 AND p.IdPublicacion = c.IdPublicacion AND c.IdSubcategoria=23 ORDER BY FijoPublicacion DESC, FechaPublicacion DESC LIMIT '.($n-1).', 1';
        $registros = $db->query($query);
        $publicacion = mysqli_fetch_array($registros, MYSQLI_BOTH);

        $imagen = "../images/default.png";
        $fecha  =   date('F d, Y', strtotime($publicacion["FechaPublicacion"]));
        $titulo =   trunc($publicacion["TituloPublicacion"], 20);
        $cuerpo =   trunc($publicacion["DescripcionPublicacion"], 480);
        $id = $publicacion["IdPublicacion"];
        $fijo = $publicacion["FijoPublicacion"];

        if(isset($publicacion['RutaImagenesPublicacion']) && $publicacion['RutaImagenesPublicacion'] != ''){
            $query          = 'SELECT * FROM imagenesarticulo WHERE idPublicacion=' . $id . ' ORDER BY idImagenesArticulos ASC LIMIT 1';
            $imagenes       = $db->query($query);
            $imagenarticulo = mysqli_fetch_array($imagenes, MYSQLI_BOTH);
            $imagen         = "../images/" . $imagenarticulo["rutaImagenes"];
        }

        $trans = array("January" => "Enero", 
                        "February" => "Febrero", 
                        "March" => "Marzo", 
                        "April" => "Abril", 
                        "May" => "Mayo", 
                        "June" => "Junio", 
                        "July" => "Julio",
                        "August" => "Agosto", 
                        "September" => "Septiembre", 
                        "October" => "Octubre", 
                        "November" => "Noviembre", 
                        "December" => "Diciembre", );
        
        $fecha = strtr($fecha, $trans);
        
        $card = '';

        if($n == 1){
            $card .=
            "<div class='card z-depth-0' id='noticia1'>
                <a href=\"../articulos/_articulo.php?id=".$id."\">";
            if ($imagen == '../images/default.png'){
                $card .= "<div class='card-image default'>";
            }else{
                $card .= "<div class='card-image'>";
            }     
                $card .= "<img class='responsive-img' src='$imagen'>
                    </div>
                </a>
                <div class='card-content left-align'>
                    <div class='white-text date-wrapper center-align'>
                        <p><small>".$fecha."</small></p>
                    </div>
                    <br>
                    <a href='../articulos/_articulo.php?id=".$id."'>
                        <span class='card-title'>";
                        if ($fijo == 1) {
                            $card .= '<i class="material-icons tiny tooltipped" data-position="top" data-delay="50" data-tooltip="Artículo fijo" style="margin-right: 5px;">gps_fixed</i>';
                        }
            $card .=        $titulo.
                        "</span>
                    </a>
                    <p class='grey-text text-darken truncate-long-text' data-truncate=>".$cuerpo."</p>
                </div>
            </div>
            ";
        }elseif($n <= 5){
            $card .=
            "
            <div class='row'>
                <div class='col s12'>
                    <div class='card horizontal z-depth-0' id='noticia".$n."'>";

             if ($imagen == '../images/default.png'){
                $card .= "<div class='card-image valign-wrapper noticias-small-img-container default'>";
            }else{
                $card .= "<div class='card-image valign-wrapper noticias-small-img-container'>";
            }
            $card .= "  
                            <a href='../articulos/_articulo.php?id=".$id."'>
                                <img src='$imagen'>
                            </a>    
                        </div>
                        <div class='card-stacked'>
                            <div class='card-content'>
                                <p class='date_noticia'><small><i class='material-icons tiny'>date_range</i>".$fecha."</small></p>
                                <a href='../articulos/_articulo.php?id=".$id."'>
                                <span class='card-title'>";
                                    if ($fijo == 1) {
                                        $card .= '<i class="material-icons tiny tooltipped" data-position="top" data-delay="50" data-tooltip="Artículo fijo" style="margin-right: 5px;">gps_fixed</i>';
                                    }
            $card .=                $titulo."
                                </span>
                                </a>
                            </div>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            ";
        }

        desconectar($db);
        return $card;
}
