<?php
    session_start();
    require_once("modelo-articulos.php");

    include('../_header.html');

    if(isset($_POST["search"])){
        $busqueda = htmlspecialchars($_POST["search"]);
    }else if(isset($_POST["searchmobile"])){
        $busqueda = htmlspecialchars($_POST["searchmobile"]);
    }else{
        $busqueda = "";
    }

    include("_resultados-busqueda-main.html");

    if(isset($_SESSION["privilegios"])){
        include('../_user-menu.html');
    }

    include('../_footer.html');
?>