function articulo(id, instruccion) {
    if (instruccion === 'editar') {
        $.getJSON("articulo.php", {
            id: id
        }).done(function(data) {
            var modal = document.getElementById('articulo-modal');
            var header = modal.getElementsByTagName('h1');
            var titulo = modal.getElementsByClassName('titulo');
            var cuerpo = modal.getElementsByClassName('cuerpo');
            var imagenes = modal.getElementsByClassName('imagenes');
            var fijo = modal.getElementsByClassName('fijo');
            header[0].innerText = 'Editar artículo - con JQUERY'
            titulo[0].value = data.titulo;
            cuerpo[0].value = data.cuerpo;
            imagenes[0].value = data.imagen;
            if (data.fijo === 1) {
                fijo[0].checked = true;
            } else {
                fijo[0].checked = false;
            }
            $('#' + modal.id).modal('open');
        });
    } else if (instruccion === 'nuevo') {}
}

function printErrorsArticulos(field, response){
    var msg = document.getElementById('error-'+field);
    msg.innerHTML = '';
    if (response[field].hasOwnProperty('mensaje')) {
        msg.innerHTML += '<span>' + response[field]['mensaje'] + '</span>';
    }else{
        jQuery.each(response[field], function(i, element) {
            if( element['upload']==false ) {
                if( element.hasOwnProperty('nombre')) { 
                    msg.innerHTML += element['nombre'] + '<br>' 
                };
                jQuery.each(element['mensaje'], function() {
                    msg.innerHTML += '<span class="tab">' + this + '</span>';
                    msg.innerHTML += '<br>'
                }); 
            }
        });
    }  
}

function resetErrors() {
    document.getElementById('error-imagenes').innerHTML = '';
    document.getElementById('error-archivos').innerHTML = '';
    document.getElementById('error-url').innerHTML = '';
    document.getElementById('error-fecha-fin-fijo').innerHTML = '';
    document.getElementById('error-hora-fin-fijo').innerHTML = '';
    document.getElementById('error-hora-publicar').innerHTML = '';
    document.getElementById('error-fecha-publicar').innerHTML = '';
}

$('.articulo-submit[type="submit"]').click(function(event) {
    event.preventDefault();
    $('#action').val(this.value)
    var form_data = new FormData($('#form-articulo')[0]);
    $.ajax({
        url: 'validar-subir-articulo.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function(response) {
            resetErrors();
            response = JSON.parse(response);
            if (response['errores'] === false) {
                if($('#action').val() == 'borrador'){
                    $('.articulo-submit[type="submit"]').off('click');
                    $('.articulo-submit[type="submit"]').click();
                }else if ($('#action').val() == 'publicar'){
                    $('.articulo-submit[type="submit"]').off('click');
                    $('.articulo-submit[type="submit"]').click();
                }
                // $('.articulo-submit[type="submit"]').unbind("click");
            } else {
                if (response.hasOwnProperty('imagenes')){
                    printErrorsArticulos('imagenes', response);
                }
                if (response.hasOwnProperty('archivos')) {
                    printErrorsArticulos('archivos', response);
                }
                if (response.hasOwnProperty('url')) { 
                    printErrorsArticulos('url', response);
                }
                if (response.hasOwnProperty('fecha-fin-fijo')) { 
                    printErrorsArticulos('fecha-fin-fijo', response);
                }
                if (response.hasOwnProperty('hora-fin-fijo')) { 
                    printErrorsArticulos('hora-fin-fijo', response);
                }
                if (response.hasOwnProperty('fecha-publicar')) { 
                    printErrorsArticulos('fecha-publicar', response);
                }
                if (response.hasOwnProperty('hora-publicar')) { 
                    printErrorsArticulos('hora-publicar', response);
                }
            }
        },
        error: function(response) {
            $('#msg').html(response); // display error response from the PHP script
        }
    });
});

$(".subcategoria").change(function() {
    if(this.val() != ''){
        document.getElementById('error-subcategoria').innerHTML = '';
    }
});

$("#titulo").on('blur', function() {
    var error_titulo = document.getElementById('error-titulo');
    error_titulo.innerHTML = '';
    if (titulo.value == '') {
        error_titulo.innerHTML += 'Este campo es obligatorio';
    }
})

$("#cuerpo").on('blur', function() {
    var error_cuerpo = document.getElementById('error-cuerpo');
    error_cuerpo.innerHTML = '';
    if (cuerpo.value == '') {
        error_cuerpo.innerHTML += 'Este campo es obligatorio';
    }
})

$("#imagenes").change(function() {
    var error_imagen = document.getElementById('error-imagenes');
    error_imagen.innerHTML = '';
    jQuery.each(imagenes['files'], function() {
        if (this.name != '') {
            //JPG, JPEG, PNG & GIF
            var formatos = ["jpg", "jpeg", "png", "gif", "JPG", "JPEG", "PNG", "GIF"];
            var flag = false;
            for (i in formatos) {
                substring = '.' + formatos[i];
                var res = this.name.includes(substring);
                if (res == true) {
                    flag = true;
                }
            }
            if (flag == false) {
                error_imagen.innerHTML += this.name;
                error_imagen.innerHTML += '<br>';
                error_imagen.innerHTML += '<span class="tab">La imágen debe tener uno de los siguientes formatos: jpg, jpeg, png, gif</span>';
                error_imagen.innerHTML += '<br>';
            }
        }
    })
})

$("#archivos").change(function() {
    var error_archivo = document.getElementById('error-archivos');
    error_archivo.innerHTML = '';
    var cont = 0;
    jQuery.each(archivos['files'], function() {
        if (this.name != '') {
            //PDF, JPG, JPEG, PNG & GIF
            var formatos = ["pdf", "jpg", "jpeg", "png", "gif", "PDF", "JPG", "JPEG", "PNG", "GIF"];
            var flag = false;
            for (i in formatos) {
                substring = '.' + formatos[i];
                var res = this.name.includes(substring);
                if (res == true) {
                    flag = true;
                }
            }
            if (flag == false) {
                error_archivo.innerHTML += this.name;
                error_archivo.innerHTML += '<br>';
                error_archivo.innerHTML += '<span class="tab">El archivo debe tener uno de los siguientes formatos: pdf, jpg, jpeg, png, gif</span>';
                error_archivo.innerHTML += '<br>';
            }
        }
    })
})

$("#fijo").change(function() {
    var fecha_fin_fijo_div = document.getElementById('fecha-fin-fijo-div');
    var hora_fin_fijo_div = document.getElementById('hora-fin-fijo-div');
    if (fijo.checked == true) {
        fecha_fin_fijo_div.innerHTML = '<input type="text" class="datepicker fecha-fin-fijo" name="fecha-fin-fijo" id="fecha-fin-fijo">' + '<label class="black-text" for="fecha-fin-fijo"><i class="material-icons left">date_range</i>Fecha fin del artículo fijo</label>'
        var $input = $('.fecha-fin-fijo').pickadate({
            weekdaysLetter: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        });
        var picker = $input.pickadate('picker');
        hora_fin_fijo_div.innerHTML = '<input type="text" class="timepicker hora-fin-fijo" name="hora-fin-fijo" id="hora-fin-fijo">' + '<label class="black-text" for="hora-fin-fijo"><i class="material-icons left">access_time</i>Para cambiar, activa el botón "sí fijar articulo"</label>'
        var $input = $('.hora-fin-fijo').pickatime({
            default: 'now', // Set default time: 'now', '1:30AM', '16:30'
            fromnow: 0, // set default time to * milliseconds from now (using with default = 'now')
            twelvehour: true, // Use AM/PM or 24-hour format
            donetext: 'OK', // text for done-button
            cleartext: 'Limpiar', // text for clear-button
            canceltext: 'Cancelar', // Text for cancel-button
            autoclose: false, // automatic close timepicker
            ampmclickable: true, // make AM PM clickable
            aftershow: function() {}, //Function for after opening timepicker
            onOpen: function() {
                console.log('Opened up')
            }
        });
    } else {
        fecha_fin_fijo_div.innerHTML = '<input type="text" class="datepicker fecha-fin-fijo" name="fecha-fin-fijo" id="fecha-fin-fijo" disabled>' + '<label for="fecha-fin-fijo"><i class="material-icons left">date_range</i>Para cambiar, activa el botón "sí fijar articulo"</label>';
        hora_fin_fijo_div.innerHTML = '<input type="text" class="timepicker hora-fin-fijo" name="hora-fin-fijo" id="hora-fin-fijo" disabled>' + '<label for="hora-fin-fijo"><i class="material-icons left">access_time</i>Para cambiar, activa el botón "sí fijar articulo"</label>';
    }
    // var f = document.getElementById('fecha-fin-fijo');
    // var h = document.getElementById('hora-fin-fijo');

    // if (fijo.checked == true) {
    //     if (f.value == ''){
    //         document.getElementById('error-fecha-fin-fijo').innerHTML = 'Ingresa una fecha o da click en "no fijar artículo"';
    //     }
    //     if (h.value == ''){
    //        document.getElementById('error-hora-fin-fijo').innerHTML = 'Ingresa una hora o da click en "no fijar artículo"';
    //     }
    // }else{
        document.getElementById('error-fecha-fin-fijo').innerHTML = '';
        document.getElementById('error-hora-fin-fijo').innerHTML = '';
    // }
});


$("#publicar-despues").change(function() {
    var publicar_despues = document.getElementById('publicar-despues');
    var fecha_publicar_div = document.getElementById('fecha-publicar-div');
    var hora_publicar_div = document.getElementById('hora-publicar-div');
    if (publicar_despues.checked == true) {
        fecha_publicar_div.innerHTML = '<input type="text" class="datepicker fecha-publicar" name="fecha-publicar" id="fecha-publicar">' + '<label class="black-text" for="fecha-publicar"><i class="material-icons left">date_range</i>Fecha en que se publicará el artículo</label>'
        var $input = $('.fecha-publicar').pickadate({
            weekdaysLetter: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        });
        var picker = $input.pickadate('picker');
        hora_publicar_div.innerHTML = '<input type="text" class="timepicker hora-publicar" name="hora-publicar" id="hora-publicar">' + '<label class="black-text" for="hora-publicar"><i class="material-icons left">access_time</i>Hora en que se publicará el artículo</label>'
        var $input = $('.hora-publicar').pickatime({
            default: 'now', // Set default time: 'now', '1:30AM', '16:30'
            fromnow: 0, // set default time to * milliseconds from now (using with default = 'now')
            twelvehour: true, // Use AM/PM or 24-hour format
            donetext: 'OK', // text for done-button
            cleartext: 'Limpiar', // text for clear-button
            canceltext: 'Cancelar', // Text for cancel-button
            autoclose: false, // automatic close timepicker
            ampmclickable: true, // make AM PM clickable
            aftershow: function() {} //Function for after opening timepicker
        });
    } else {
        fecha_publicar_div.innerHTML = '<input type="text" class="datepicker fecha-publicar" name="fecha-publicar" id="fecha-publicar" disabled>' + '<label for="fecha-publicar"><i class="material-icons left">date_range</i>Para cambiar, activa el botón "publicar después"</label>';
        hora_publicar_div.innerHTML = '<input type="text" class="timepicker hora-publicar" name="hora-publicar" id="hora-publicar" disabled>' + '<label for="hora-publicar"><i class="material-icons left">access_time</i>Para cambiar, activa el botón "publicar después"</label>';
    }
    // var f = document.getElementById('fecha-publicar');
    // var h = document.getElementById('hora-publicar');

    // if (publicar_despues.checked == true) {
    //     if (f.value == ''){
    //         document.getElementById('error-fecha-publicar').innerHTML = 'Ingresa una fecha o da click en "publicar ahora"';
    //     }
    //     if (h.value == ''){
    //        document.getElementById('error-hora-publicar').innerHTML = 'Ingresa una hora o da click en "publicar ahora"';
    //     }
    // }else{
        document.getElementById('error-fecha-publicar').innerHTML = '';
        document.getElementById('error-hora-publicar').innerHTML = '';
    // }
});

$('input[type="checkbox"][class="checkboxsubcategoria"]').change(function() {
    if($(this).is(':checked')){
        $(this).parent().css('color','#0e88ee');
        document.getElementById('error-subcategoria').innerHTML = '';
    }else{

        var alguno_seleccionado = false;
        $('input[type="checkbox"][class="checkboxsubcategoria"]').each(function() {
            if ($(this).is(':checked')){
                alguno_seleccionado = true;
            }
        });
        if(alguno_seleccionado == false){
            document.getElementById('error-subcategoria').innerHTML = 'Selecciona al menos una subcategoría';
        }
    }
});

$(function() {
    $("#Nosotros, #Fenómenos, [id='Cultura de la Prevención'], [id='Programas Especiales'], #Trámites, #Servicios, #Documentos").each(function(){
        if ($(this).val() != ''){
            $(this).parent().css('color','#0e88ee');
        }
    });
    
});

$("#listararticulos").change(function() {
    var val = String($(this).val());
    val = JSON.parse(val);
    console.log(val.valor)
    $.post("listararticulo.php", {
        IdSubcategoria: val.subcategoria,
        columna: val.valor
    }).done(function(articulos) {
        var div = document.getElementById('articulos-admin');
        div.innerHTML = '';
        console.log(articulos);
        div.innerHTML = articulos;
    }).fail(function() {
        alert( "Imposible listar los artículos en este momento" );
      })
    ;
});




