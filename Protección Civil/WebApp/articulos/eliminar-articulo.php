<?php
    session_start();
    if(isset($_SESSION["privilegios"])){
        require_once("modelo-articulos.php");
        eliminarArticulo($_GET["id"]);
        $_SESSION["mensaje"] = 'Artículo eliminado correctamente.';
        header('location:ver-articulos.php');
    }else{
        include('../error.html');
    }
?>