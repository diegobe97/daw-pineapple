<?php
    session_start();
    require_once("modelo-articulos.php");
    if(isset($_SESSION["privilegios"]) && binarySearch($_SESSION["privilegios"], 21)){
        include("../_header.html");
        $publicacion = getPublicacion(conectar(), $_GET["idArticulo"]);
        
        include("_bitacora-articulo-main.html");
        include("../_user-menu.html");
        
        include("../_footer.html");
    }else{
        include("../error.html");
    }
?>