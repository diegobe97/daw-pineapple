$("#imagen").change(function() {
    var error_imagen = document.getElementById('error-imagen');
    error_imagen.innerHTML = '';
    var imagen = $('#imagen').val().replace(/C:\\fakepath\\/i, '')
    if (imagen != '') {
        //JPG, JPEG, PNG & GIF
        flag = false;
        var formatos = ["jpg", "jpeg", "png", "gif", "JPG", "JPEG", "PNG", "GIF"];
        for (i in formatos) {
            substring = '.' + formatos[i];
            var res = imagen.includes(substring);
            if (res == true) {
                flag = true;
            }
        }
        if (flag == false) {
            error_imagen.innerHTML += imagen;
            error_imagen.innerHTML += '<br>';
            error_imagen.innerHTML += '<span class="tab">La imágen debe tener uno de los siguientes formatos: jpg, jpeg, png, gif</span>';
            error_imagen.innerHTML += '<br>';
        }
    }
});

$('.index-submit[type="submit"]').click(function(event) {
    event.preventDefault();
    // var form_data = new FormData($('#form-que-es-pc')[0]);

    $.ajax({
        url: 'validar-subir-quees-pc.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function(response) {
            response = JSON.parse(response);
            var error_imagen = document.getElementById('error-imagen');
            error_imagen.innerHTML = '';
            if (response['errores'] === false) {
                    $('#publicar').off('click');
                    $("#publicar").click();
            } else {
                element = response['imagen'];
                error_imagen.innerHTML += element['nombre'] + '<br>' 
                jQuery.each(element['mensaje'], function() {
                    error_imagen.innerHTML += '<span class="tab">' + this + '</span>';
                    error_imagen.innerHTML += '<br>';
                }); 
            }
        },
        error: function(response) {
            alert('Imposible actualizar la sección en este momento.');
        }
    });
});

$('.banners-submit[type="submit"]').click(function(event) {
    event.preventDefault();
    var form_data = new FormData($('#form-banners')[0]);
    $.ajax({
        url: 'validar-subir-banner.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function(response) {
            response = JSON.parse(response);
            var error_imagenes = document.getElementById('error-imagenes');
            error_imagenes.innerHTML = '';
            if (response['errores'] === false) {
                $('.banners-submit[type="submit"]').off('click');
                $('.banners-submit[type="submit"]').click();
            } else {
                if (response.hasOwnProperty('imagenes')){
                    printErrors('imagenes', response);
                }
            }
        },
        error: function(response) {
            alert('Imposible subir el banner en estos momentos');
        }
    });
});

$('.atendemos-submit[type="submit"]').click(function(event) {
    event.preventDefault();
    var form_data = new FormData($('#form-atendemos')[0]);
    $.ajax({
        url: 'validar-subir-atendemos-tramitamos.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function(response) {
            response = JSON.parse(response);
            var error_imagenes = document.getElementById('error-imagenes');
            error_imagenes.innerHTML = '';
            if (response['errores'] === false) {
                $('.atendemos-submit[id="publicar"]').off('click');
                $('.atendemos-submit[id="publicar"]').click();
            } else {
                if (response.hasOwnProperty('imagenes')){
                    printErrors('imagenes', response);
                }
            }
        },
        error: function(response) {
            alert('Imposible subir el banner en estos momentos');
        }
    });
});

$('.tramitamos-submit[type="submit"]').click(function(event) {
    event.preventDefault();
    var form_data = new FormData($('#form-tramitamos')[0]);
    $.ajax({
        url: 'validar-subir-atendemos-tramitamos.php', // point to server-side PHP script 
        dataType: 'text', // what to expect back from the PHP script
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function(response) {
            response = JSON.parse(response);
            var error_imagenes = document.getElementById('error-imagenes');
            error_imagenes.innerHTML = '';
            if (response['errores'] === false) {
                $('.tramitamos-submit[id="publicar"]').off('click');
                $('.tramitamos-submit[id="publicar"]').click();
            } else {
                if (response.hasOwnProperty('imagenes')){
                    printErrors('imagenes', response);
                }
            }
        },
        error: function(response) {
            alert('Imposible subir el banner en estos momentos');
        }
    });
});

function printErrors(field, response){
    var msg = document.getElementById('error-'+field);
    msg.innerHTML = '';
    if (response[field].hasOwnProperty('mensaje')) {
        msg.innerHTML += '<span>' + response[field]['mensaje'] + '</span>';
    }else{
        jQuery.each(response[field], function(i, element) {
            if( element['upload']==false ) {
                if( element.hasOwnProperty('nombre')) { 
                    msg.innerHTML += element['nombre'] + '<br>' 
                };
                jQuery.each(element['mensaje'], function() {
                    msg.innerHTML += '<span class="tab">' + this + '</span>';
                    msg.innerHTML += '<br>'
                }); 
            }
        });
    }  
}