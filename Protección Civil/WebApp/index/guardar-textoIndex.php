<?php
    session_start();
    if(isset($_SESSION["privilegios"])){
	    header("location:index.php");
	    require_once("modelo-index.php");
	    editarTextoIndex(htmlspecialchars($_POST["textoIndex"]));
	    $_SESSION["mensaje"] = 'La sección "¿Qué es Protección Civil?" se actualizó correctamente.';
	}
?>
