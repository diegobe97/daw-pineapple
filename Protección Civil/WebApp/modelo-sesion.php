<?php
    require_once("modelo.php");

    function getIdRolByUserId($email){
        $db = conectar();
        //Specification of the SQL query
        $query='SELECT * FROM usuario WHERE NombreUsuario LIKE \''.$email.'\'';
         // Query execution; returns identifier of the result group
        $registros = $db->query($query);

        $usuario = mysqli_fetch_array($registros, MYSQLI_BOTH);
        
        $idUsuario = $usuario["IdUsuario"];
        
        //Specification of the SQL query
        $query='SELECT * FROM asignadoa WHERE IdUsuario = "'.$idUsuario.'"';
         // Query execution; returns identifier of the result group
        $registros = $db->query($query);
        
        $asignadoa = mysqli_fetch_array($registros, MYSQLI_BOTH);
        
        desconectar($db);

        return (int)$asignadoa["IdRol"];
    }

    function getPrivilegiosFor($IdRol){
        $db = conectar();

        //Specification of the SQL query
        $query='SELECT count(*) As total FROM cuentacon WHERE IdRol = "'.$IdRol.'"';
        // Query execution; returns identifier of the result group
        
        $cantidad = $db->query($query);

        $n = mysqli_fetch_array($cantidad, MYSQLI_BOTH);
        
        if($n == 0){
            return null;
        }
        
        //Specification of the SQL query
        $query='SELECT * FROM cuentacon WHERE IdRol = "'.$IdRol.'"';
         // Query execution; returns identifier of the result group
        $registros = $db->query($query);

        $arrayPrivilegios = array_fill(0,$n["total"],-1);

        $i = 0;

        while ($filaPrivilegios = mysqli_fetch_array($registros, MYSQLI_BOTH)) {
            $arrayPrivilegios[$i] = $filaPrivilegios["IdPrivilegio"];
            $i++;
        }

        // it releases the associated results
        mysqli_free_result($registros);

        desconectar($db);

        return $arrayPrivilegios;
    }

    function getIdUsuarioByMail($email){
        $db = conectar();
        
        $query='SELECT * FROM usuario WHERE NombreUsuario LIKE "'.$email.'"';
        // Query execution; returns identifier of the result group
        
        $user = $db->query($query);

        $usuario = mysqli_fetch_array($user, MYSQLI_BOTH);
        
        // it releases the associated results
        mysqli_free_result($user);

        desconectar($db);
        
        return $usuario["IdUsuario"];
    }
?>
