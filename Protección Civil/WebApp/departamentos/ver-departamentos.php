<?php
    session_start();
    if(isset($_SESSION["privilegios"])){
        require_once("modelo-departamentos.php");

        include('../_header.html');
        
        include('_departamentos-main.html');

        include('../_user-menu.html');
        include('../_footer.html');
        
        if (isset($_SESSION["mensaje"])) {
            $mensaje = $_SESSION["mensaje"];
            include('../_mensaje.html');
            unset($_SESSION["mensaje"]);
        }
    }else{
        include('../error.html');
    }
?>