<?php
    session_start();
    if(isset($_SESSION["privilegios"])){
        require_once("modelo-departamentos.php");
        include("../_header.html");

        include("_form-departamento.html");

        include('../_user-menu.html');
        include("../_footer.html");
    }else{
        include('../error.html');
    }
?>