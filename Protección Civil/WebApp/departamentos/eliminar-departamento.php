<?php
    session_start();
    require_once("modelo-departamentos.php");

    eliminarDepartamento($_GET["id"]);

    $_SESSION["mensaje"] = 'Departamento eliminado correctamente.';

    header("location:ver-departamentos.php");
?>
