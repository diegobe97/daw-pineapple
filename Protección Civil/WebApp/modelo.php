<?php
    function conectar() {

      $b = 1;
      if ($b == 1){
        $host = "localhost";
        $username = "root";
        $password = "root";
        $dbname = "CEPCQ";
        $port = 3306;
        $link = mysqli_init();
        $con = mysqli_real_connect($link, $host, $username, $password, $dbname, $port);

            //Checar conexión
            if (!$con){
              die("Conexión fallida: " . mysqli_connect_error());
            }

            $link->set_charset("utf8");

            return $link;

          } else {
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "CEPCQ";

            $con = mysqli_connect($servername, $username, $password, $dbname);

            // Check Connection
            if(!$con){
                die("Connection failed: ".mysqli_connect_error());
            }

            $con->set_charset('utf8');

            return $con;
        }
    }

    function desconectar($mysql) {
        mysqli_close($mysql);
    }

    function binarySearch($array, $value){
        $low = 0;
        $high = count($array) - 1;
        while($low <= $high){
            $mid = floor(($high + $low)/2);
            if($array[$mid] == $value){
                return true;
            }else if($array[$mid] > $value){
                $high = $mid - 1;
            }else{
                $low = $mid + 1;
            }
        }
        return false;
    }

    function trunc($str, $len) {
        if (strlen($str) > $len) {
            $str = substr($str, 0, $len) . "...";
        }
        return $str;
    }

    function getFechaConFormato($fechabd){
        $fecha  =   date('d F Y', strtotime($fechabd));

        $trans = array("January" => "Enero",
                        "February" => "Febrero",
                        "March" => "Marzo",
                        "April" => "Abril",
                        "May" => "Mayo",
                        "June" => "Junio",
                        "July" => "Julio",
                        "August" => "Agosto",
                        "September" => "Septiembre",
                        "October" => "Octubre",
                        "November" => "Noviembre",
                        "December" => "Diciembre" );

        $fecha = strtr($fecha, $trans);
        return $fecha;
    }
?>
